<?php
require("cupomida.php");
require('InstaPush.php');
class JSON extends Cupomida {
	private $SelectUsuario;
	private $JoinUsuario;
	private $GroupUsuario;
	private $SelectCupon;
	private $JoinCupon;
	private $FilterTime;
	private $FilterHour;
	private $FilterOverSell;
	private $FilterDeleted;
	private $FilterDetached;
	private $GroupRating;
	private $Order;
	private $Pagination;
	private $Usuario;
	private $Administrador;

	public function JSON() {
		parent::Cupomida();
		header('Content-Type: application/json; charset=UTF-8');
		$this->setVars();
		$this->querys();
		$this->router();
	}
	private function setVars(){
		if(isset($_SESSION["Usuario"])) $this->Usuario=$_SESSION["Usuario"];
		else $this->Usuario=1;
		if(isset($_SESSION["Administrador"])) $this->Administrador=intval($_SESSION["Administrador"]);
		else $this->Administrador=0;
	}
	private function querys(){
		$this->SelectUsuario="SELECT ".
			"`U`.`id_usuario`,".
			"`U`.`id_social`,".
			"`U`.`first_name`,".
			"`U`.`last_name`,".
			"`U`.`name`,".
			"`U`.`password`,".
			"`U`.`imagen`,".
			"`U`.`gender`,".
			"`U`.`locale`,".
			"`U`.`link`,".
			"`U`.`timezone`,".
			"`U`.`verified`,".
			"`U`.`email`,".
			"`U`.`id_ubicacion`,".
			"`U`.`administrador`,".
			"GROUP_CONCAT(`P`.`id_categoria`) AS `preferencias` ";
		$this->JoinUsuario="FROM `Usuarios` AS `U` ".
			"LEFT JOIN `Preferencias_Usuarios` AS `P` ".
				"ON `P`.`id_usuario`=`U`.`id_usuario` ";
		$this->GroupUsuario="GROUP BY `U`.`id_usuario` ";
		$this->SelectCupon="SQL_CALC_FOUND_ROWS `C`.`id_cupon`,".
			"`C`.`titulo`,`C`.`descripcion`,`C`.`dias_habiles`,".
			"`C`.`valido_desde`,`C`.`valido_hasta`,`C`.`restantes`,".
			"`C`.`precio`,`C`.`descuento`,`C`.`detalle`,".//`C`.`imagen`,
			"`C`.`cantidad`,`C`.`condiciones`,".
			"`CC`.`id_categoria`,`CA`.`nombre` AS `titulo_categoria`,".
			"`CA`.`descripcion` AS `descripcion_categoria`,".
			"`CL`.`id_cliente`,`CL`.`nombre` AS `nombre_cliente`,".
			"`CL`.`email`,`CL`.`telefono`,`CL`.`horario`,".
			"`CL`.`informacion`,".//`CL`.`imagen` AS `imagen_cliente`,
			"`U`.`id_ubicacion`,`U`.`direccion`,".
			"`U`.`latitud`,`U`.`longitud`,".
			"`CH`.`id_horario`,`H`.`nombre` AS `titulo_horario`,".
			"`H`.`hora_inicio`,`H`.`hora_fin`,".
			"COUNT(`L`.`id_like`) AS `likes_cupon`,".
			"COUNT(`R`.`id_rating`) AS `valoraciones_cupon`,".
			"AVG(`R`.`valoracion`) AS `valoracion_cupon`,".
			"COUNT(`RC`.`id_rating`) AS `valoraciones_cliente`,".
			"AVG(`RC`.`valoracion`) AS `valoracion_cliente` ";
		$this->JoinCupon="FROM `Cupones` AS `C` ".
			"INNER JOIN `Cupon_Categoria` AS `CC` ".
				"ON `CC`.`id_cupon`=`C`.`id_cupon` ".
			"LEFT JOIN `Categorias` AS `CA` ".
				"ON `CA`.`id_categoria`=`CC`.`id_categoria` ".
			"LEFT JOIN `Clientes` AS `CL` ".
				"ON `C`.`id_cliente`=`CL`.`id_cliente` ".
			"LEFT JOIN `Ubicacion_Cupon` AS `UC` ".
				"ON `UC`.`id_cupon`=`C`.`id_cupon` ".
			"LEFT JOIN `Ubicaciones` AS `U` ".
				"ON `U`.`id_ubicacion`=`UC`.`id_ubicacion` ".
			"LEFT JOIN `Cupon_Horario` AS `CH` ".
				"ON `CH`.`id_cupon`=`C`.`id_cupon` ".
			"LEFT JOIN `Horarios` AS `H` ".
				"ON `H`.`id_horario`=`CH`.`id_horario` ".
			"LEFT OUTER JOIN `Likes` AS `L` ".
				"ON `L`.`id_cupon`=`C`.`id_cupon` ".
			"LEFT OUTER JOIN `Rating_Cupon` AS `R` ".
				"ON `R`.`id_cupon`=`C`.`id_cupon` ".
			"LEFT OUTER JOIN `Rating_Cliente` AS `RC` ".
				"ON `RC`.`id_cliente`=`CL`.`id_cliente` ";
		$this->FilterTime="WHERE CONVERT_TZ(NOW(),'UTC','America/Santiago') BETWEEN ".
			"`C`.`valido_desde` AND `C`.`valido_hasta` ";
		$this->FilterHour="AND TIME(CONVERT_TZ(NOW(),'UTC','America/Santiago')) BETWEEN ".
			"`H`.`hora_inicio` AND `H`.`hora_fin` ";
		$this->FilterOverSell="AND `C`.`restantes`>0 ";
		$this->FilterDeleted="AND `C`.`estado`=1 ";
		$this->FilterDetached="AND `CL`.`estado`=1 ";
		if(isset($_REQUEST["Limite"])) $L=intval($_REQUEST["Limite"]);
		else $L=6;
		if(isset($_REQUEST["Pagina"])){
			$P=intval($_REQUEST["Pagina"])*$L;
		}else $P=0;
		$this->GroupRating="GROUP BY `C`.`id_cupon` ";
		$this->Order="ORDER BY `C`.`id_cupon` DESC ";
		$this->Pagination="LIMIT $P,$L";
	}
	private function FilterAdmin(){
		switch($this->Administrador){
			case 1:
				return "WHERE 1 ";
			break;
			case 2:
				$query="WHERE `CL`.`id_cliente` IN (";
				if($Clientes=$this->DB->query("SELECT ".
					"`id_cliente` ".
					"FROM `Clientes_Usuarios` ".
					"WHERE `id_usuario`=".$this->Usuario
				)){
					while($Cliente=mysqli_fetch_assoc($Clientes)){
						$query.=$Cliente["id_cliente"].",";
					}
					$Clientes->free();
					if($query=="WHERE `CL`.`id_cliente` IN ("){
						$query.="0";
					}
					return rtrim($query,",").") ";
				}else return "WHERE `CL`.`id_cliente` IN (0) ";
			break;
			default:
				return "WHERE `CL`.`id_cliente` IN (0) ";
			break;
		}
	}
	private function FilterDate(){
		return "AND DATE('".$_REQUEST["Fecha"]."') BETWEEN ".
			"DATE(`C`.`valido_desde`) ".
		"AND ".
			"DATE(`C`.`valido_hasta`)";
	}
	private function FilterRange(){
		$Query="";
		if(isset($_REQUEST["Rango"])){
			if($_REQUEST["Rango"]=="Semana"){
				$Query.="AND WEEKOFYEAR(`V`.`tiempo`)=WEEKOFYEAR(CONVERT_TZ(NOW(),'UTC','America/Santiago')) ";
			}
			if($_REQUEST["Rango"]=="Mes"){
				$Query.="AND MONTH(`V`.`tiempo`)=MONTH(CONVERT_TZ(NOW(),'UTC','America/Santiago')) ";
			}
			if($_REQUEST["Rango"]=="Año" || $_REQUEST["Rango"]=="Mes" || $_REQUEST["Rango"]=="Semana"){
				$Query.="AND YEAR(`V`.`tiempo`)=YEAR(CONVERT_TZ(NOW(),'UTC','America/Santiago')) ";
			}
		}
		return $Query;
	}
	private function router(){
		switch($_REQUEST["Accion"]){
			case "Iniciar":
				$this->Iniciar();
			break;
			case "Cerrar":
				$_SESSION=[];
				echo json_encode(array("Success"=>true));
			break;
			case "Usuario por Key":
				$this->getUsuarioKey();
			break;
			case "Set Imagen Usuario":
				$this->setImagenUsuario();
			break;
			case "Actualizar Datos Usuario":
				$this->setDatosUsuario();
			break;
			case "Actualizar Contra Usuario":
				$this->setContra();
			break;
			case "Actualizar Rol":
				$this->setRol();
			break;
			case "Actualizar Empresas":
				$this->setEmpresas();
			break;
			case "Actualizar Cliente":
				$this->setCliente();
			break;
			case "Usuarios":
				$this->getUsuarios();
			break;
			case "Clientes Usuarios":
				$this->getClientesUsuarios();
			break;
			case "Nuevo Cupon":
				$this->setNuevoCupon();
			break;
			case "Habilitar Cupon":
				$this->setCuponPagado();
			break;
			case "Eliminar Cupon":
				$this->rmCupon();
			break;
			case "Eliminar Cliente":
				$this->rmCliente();
			break;
			case "Nuevo Rating Cupon":
				$this->setNuevoRatingCupon();
			break;
			case "Rating Cupon":
				$this->getRatingCupon();
			break;
			case "Nuevo Rating Cliente":
				$this->setNuevoRatingCliente();
			break;
			case "Rating Cliente":
				$this->getRatingCliente();
			break;
			case "Nueva Posicion":
				$this->setNuevaPosicion();
			break;
			case "Cupones":
				$this->getCupones();
			break;
			case "Cupones Pendientes":
				$this->getCuponesPendientes();
			break;
			case "Cupones Disponibles":
				$this->getCuponesDisponibles();
			break;
			case "Cupones por Categoria":
				$this->getCuponCategoria();
			break;
			case "Cupones por Cliente":
				$this->getCuponCliente();
			break;
			case "Cupones por Ubicacion":
				$this->getCuponUbicacion();
			break;
			case "Cupon por ID":
				$this->getCuponID();
			break;
			case "Cupon Buscar":
				$this->getCuponBuscado();
			break;
			case "Cupon Destacado":
				$this->getCuponDestacado();
			break;
			case "Cupon Cercano":
				$this->getCuponCercano();
			break;
			case "Cupones Visitados":
				$this->getCuponesVisitados();
			break;
			case "Imagen Cupon":
				$this->getImageCupon();
			break;
			case "Categorias":
				$this->getCategorias();
			break;
			case "Categorias Disponibles":
				$this->getCategoriasDisponibles();
			break;
			case "Clientes":
				$this->getClientes();
			break;
			case "Imagen Cliente":
				$this->getImageCliente();
			break;
			case "Codigos":
				$this->getCodigos();
			break;
			case "Codigos Cliente":
				$this->getCodigosCliente();
			break;
			case "Validar Codigo":
				$this->setCodigoCobrado();
			break;
			case "Horarios Disponibles":
				$this->getHorariosDisponibles();
			break;
			case "Horarios":
				$this->getHorarios();
			break;
			case "Ubicaciones":
				$this->getUbicaciones();
			break;
			case "Ubicaciones Disponibles":
				$this->getUbicacionesDisponibles();
			break;
			case "Ubicacion por ID":
				$this->getUbicacionID();
			break;
			case "Visitas":
				$this->getVisitasCupones();
			break;
			case "Likes":
				$this->getLikes();
			break;
			case "Nuevo Like":
				$this->setLike();
			break;
			case "Eliminar Like":
				$this->rmLike();
			break;
			case "Visitas por ID":
				$this->getVisitasCuponID();
			break;
			case "Ventas":
				$this->getVentas();
			break;
			case "Denuncias":
				$this->getDenuncias();
			break;
			case "Denunciar":
				$this->setDenuncia();
			break;
			case "Actualizar Preferencias":
				$this->setPreferencias();
			break;
			case "Tiempo":
				$this->getTimeStamp();
			break;
			case "Session":
				$this->getSession();
			break;
			default:
				echo json_encode(array("Success"=>false));
			break;
		}
	}
	private function Iniciar(){
		if($this->Usuario>1){
			$Query=$this->SelectUsuario.
				$this->JoinUsuario.
				"WHERE `U`.`id_usuario`=".$this->Usuario.
				" LIMIT 1";
		}else{
			if(isset($_POST["id"])){
				$Query=$this->SelectUsuario.
					$this->JoinUsuario.
					"WHERE `U`.`id_social`=".$_POST["id"].
					" LIMIT 1";
			}elseif(isset($_POST["email"],$_POST["contra"])){
				$Query=$this->SelectUsuario.
					$this->JoinUsuario.
					"WHERE `U`.`email`='".$_POST["email"]."' ".
					" LIMIT 1";
			}else{
				echo json_encode(array("Success"=>false));
				die();
			}
		}
		if($Usuarios=$this->DB->query($Query)){
			$Usuario=mysqli_fetch_assoc($Usuarios);
			if($Usuario && gettype($Usuario)=="array" && $Usuario["id_usuario"]!=null){
				if($Usuario["password"]==""){
					$_SESSION["Usuario"]=$Usuario["id_usuario"];
					$_SESSION["Administrador"]=$Usuario["administrador"];
					unset($Usuario["password"]);
					echo json_encode(array("Success"=>true,"Result"=>$Usuario));
				}else{
					if($Usuario["password"]==$_POST["contra"]){
						$_SESSION["Usuario"]=$Usuario["id_usuario"];
						$_SESSION["Administrador"]=$Usuario["administrador"];
						unset($Usuario["password"]);
						echo json_encode(array("Success"=>true,"Result"=>$Usuario));
					}else{
						echo json_encode(array(
							"Success"=>false,
							"errorCode"=>4,
							"Error"=>"Contraseña errónea"
						));
					}
				}
			}else{
				$usuarioLocal=false;
				if(!isset($_POST["id"])) $_POST["id"]=0;
				if(!isset($_POST["first_name"])) $_POST["first_name"]="";
				if(!isset($_POST["last_name"])) $_POST["last_name"]="";
				if(!isset($_POST["name"])){
					if(isset($_POST["email"])){
						$usuarioLocal=true;
						$_POST["name"]=substr(uniqid(),0,5);
					}
				}
				if(!isset($_POST["gender"])) $_POST["gender"]="female";
				if(!isset($_POST["link"])) $_POST["link"]="";
				if(!isset($_POST["locale"])) $_POST["locale"]="";
				if(!isset($_POST["timezone"])) $_POST["timezone"]="";
				if(!isset($_POST["verified"])) $_POST["verified"]="false";
				if(!isset($_POST["email"])) $_POST["email"]="";
				if(!isset($_POST["contra"])) $_POST["contra"]="";
				if($Usuario=$this->DB->query("CALL `nuevo_usuario`(".
					"'".$_POST["id"]."','".$_POST["first_name"].
					"','".$_POST["last_name"]."','".$_POST["name"].
					"','".$_POST["gender"]."','".$_POST["locale"].
					"','".$_POST["link"]."','".$_POST["timezone"].
					"','".$_POST["verified"]."','".$_POST["email"].
					"','".$_POST["contra"]."');"
				)){
					$Usuario=mysqli_fetch_assoc($Usuario);
					$this->DB->next_result();
					$_SESSION["Usuario"]=$Usuario["id_usuario"];
					$_SESSION["Administrador"]=0;
					$Result=array(
						"id_usuario"=>$Usuario["id_usuario"],
						"id_social"=>$_POST["id"],
						"first_name"=>$_POST["first_name"],
						"last_name"=>$_POST["last_name"],
						"name"=>$_POST["name"],
						"gender"=>$_POST["gender"],
						"locale"=>$_POST["locale"],
						"link"=>$_POST["link"],
						"timezone"=>$_POST["timezone"],
						"verified"=>$_POST["verified"],
						"email"=>$_POST["email"],
						"id_ubicacion"=>1,
						"administrador"=>0
					);
					if($usuarioLocal){
						$query="CALL `Cupomida`.`nuevo_correo`(".
							$Usuario["id_usuario"].",0,".
							"'Verificación de cuenta.',".
							"'Este es un correo para verificar el correo ".
							'y la cuenta que acaba de crear en <a href="http://cupomida.cl/">Cupomida.cl</a><br/>'.
							'Por favor diríjase a <a href="http://cupomida.cl/#Validar/'.$_POST["name"].
							'">http://cupomida.cl/#Validar/'.$_POST["name"].'</a><br/>'.
							"Su usted no se ha registrado en el sitio puede que alguien lo haya ".
							"intentado en su nombre, puede omitir este correo de forma segura.','');";
						if($this->DB->query($query)){
							echo json_encode(array("Success"=>true,"Result"=>$Result));
						}else{
							echo json_encode(array("Success"=>false,"Error"=>$this->DB->error));
						}
					}else{
						echo json_encode(array("Success"=>true,"Result"=>$Result));
					}
				}else{
					echo json_encode(array(
						"Success"=>false,
						"errorCode"=>2,
						"Error"=>$this->DB->error
					));
				}
			}
			$Usuarios->free();
		}else{
			echo json_encode(array(
				"Success"=>false,
				"errorCode"=>2,
				"Error"=>$this->DB->error
			));
		}
	}
	private function getUsuarioKey(){
		if(isset($_POST["ID"])){
			if($Users=$this->DB->query($this->SelectUsuario.
				$this->JoinUsuario.
				"WHERE `U`.`name`='".$_POST["ID"]."' ".
				"LIMIT 1"
			)){
				$User=mysqli_fetch_assoc($Users);
				$Users->free();
				unset($User["password"]);
				echo json_encode(array(
					"Success"=>true,
					"Result"=>$User
				));
			}else{
				echo json_encode(array(
					"Success"=>false,
					"errorCode"=>2,
					"Error"=>$this->DB->error
				));
			}
		}else echo json_encode(array("Success"=>false,"errorCode"=>3));
	}
	private function setImagenUsuario(){
		if($this->Usuario>1){
			if(isset($_REQUEST["Imagen"])){
				$imagen=$_REQUEST["Imagen"];
				if($this->DB->query("CALL `nueva_imagen_usuario` (".
					"'".$this->Usuario."',".
					"'".$imagen."');"
				)) echo json_encode(array("Success"=>true));
				else{
					echo json_encode(array(
						"Success"=>false,
						"errorCode"=>2,
						"Error"=>$this->DB->error
					));
				}
			}else echo json_encode(array("Success"=>false,"errorCode"=>3));
		}else echo json_encode(array("Success"=>false,"errorCode"=>1));
	}
	private function setDatosUsuario(){
		if($this->Usuario>1 || isset($_REQUEST["key"])){
			if(isset(
				$_REQUEST["first_name"],
				$_REQUEST["last_name"],
				$_REQUEST["email"],
				$_REQUEST["gender"]
			)){
				$time_start=microtime(true);
				$first_name=$_REQUEST["first_name"];
				$last_name=$_REQUEST["last_name"];
				$email=$_REQUEST["email"];
				$gender=$_REQUEST["gender"];
				if(isset($_REQUEST["key"])){
					if($_REQUEST["key"]) $key=$_REQUEST["key"];
				}else $key=false;
				if(isset($_REQUEST["id_usuario"])){
					if($key) $this->Usuario=$_REQUEST["id_usuario"];
				}
				$Query="CALL `actualizar_datos_usuario` (".
					"'".$this->Usuario."',".
					"'".$first_name."',".
					"'".$last_name."',".
					"'".$email."',".
					"'".$gender."',".
					"'".$key."');";
				if($this->DB->query($Query)){
					echo json_encode(array(
						"Success"=>true,
						"Query"=>$Query,
						"Tiempo"=>(microtime(true)-$time_start)
					));
				}else{
					echo json_encode(array(
						"Success"=>false,
						"errorCode"=>2,
						"Error"=>$this->DB->error
					));
				}
			}else echo json_encode(array("Success"=>false,"errorCode"=>3));
		}else echo json_encode(array("Success"=>false,"errorCode"=>1));
	}
	private function setContra(){
		if($this->Usuario>1 || isset($_REQUEST["key"])){
			if(isset(
				$_REQUEST["pass"],
				$_REQUEST["passOld"]
			)){
				$time_start=microtime(true);
				if(isset($_REQUEST["key"])){
					if($_REQUEST["key"]) $key=$_REQUEST["key"];
				}else $key=false;
				if(!isset($_REQUEST["pass"])) $_REQUEST["pass"]="";
				if(!isset($_REQUEST["passOld"])) $_REQUEST["passOld"]="";
				if(isset($_REQUEST["id_usuario"])){
					if($key) $this->Usuario=$_REQUEST["id_usuario"];
				}
				$Query="CALL `actualizar_contra_usuario` (".
					"'".$this->Usuario."',".
					"'".$_REQUEST["pass"]."',".
					"'".$_REQUEST["passOld"]."',".
					"'".$key."');";
				if($Changeds=$this->DB->query($Query)){
					$Changed=mysqli_fetch_assoc($Changeds);
					//$Changeds->free();
					echo json_encode(array(
						"Success"=>true,
						"Query"=>$Query,
						"Password"=>$Changed,
						"Tiempo"=>(microtime(true)-$time_start)
					));
				}else{
					echo json_encode(array(
						"Success"=>false,
						"errorCode"=>2,
						"Error"=>$this->DB->error
					));
				}
			}else echo json_encode(array("Success"=>false,"errorCode"=>3));
		}else echo json_encode(array("Success"=>false,"errorCode"=>1));
		
	}
	private function setRol(){
		if($this->Administrador==1){
			if(isset($_REQUEST["Rol"],$_REQUEST["ID"])){
				if($this->DB->query("UPDATE `Usuarios` ".
						"SET `administrador`=".$_REQUEST["Rol"].
					" WHERE `id_usuario`=".$_REQUEST["ID"]
				)){
					echo json_encode(array(
						"Success"=>true
					));
				}else{
					echo json_encode(array(
						"Success"=>false,
						"errorCode"=>2,
						"Error"=>$this->DB->error
					));
				}
			}else echo json_encode(array("Success"=>false,"errorCode"=>3));
		}else echo json_encode(array("Success"=>false,"errorCode"=>4));
	}
	private function setEmpresas(){
		if($this->Administrador===1){
			if(isset($_REQUEST["Empresas"],$_REQUEST["ID"]) && is_array($_REQUEST["Empresas"])){
				if($this->DB->query("DELETE FROM `Clientes_Usuarios` ".
					"WHERE `id_usuario`=".$_REQUEST["ID"]
				)){
					foreach($_REQUEST["Empresas"] as $Empresa){
						if(!$this->DB->query("INSERT INTO `Clientes_Usuarios` ".
								"(`id_cliente_usuario`,`id_usuario`,`id_cliente`,`tiempo`) ".
							"VALUES ".
							"(NULL,".$_REQUEST["ID"].",".$Empresa.",CONVERT_TZ(NOW(),'UTC','America/Santiago'))"
						)){
							echo json_encode(array(
								"Success"=>false,
								"errorCode"=>2,
								"Error"=>$this->DB->error
							));
							exit();
						}
					}
					echo json_encode(array(
						"Success"=>true
					));
				}else{
					echo json_encode(array(
						"Success"=>false,
						"errorCode"=>2,
						"Error"=>$this->DB->error
					));
				}
			}else echo json_encode(array("Success"=>false,"errorCode"=>3));
		}else echo json_encode(array("Success"=>false,"errorCode"=>4));
	}
	private function getUsuarios(){
		if($this->Administrador==1){
			if($Usuarios=$this->DB->query("SELECT ".
					"`id_usuario`,`first_name`,`last_name`,`administrador` ".
				"FROM `Usuarios` ".
				"WHERE `id_usuario`!=1"
			)){
				while($Rows[]=mysqli_fetch_assoc($Usuarios));
				array_pop($Rows);
				$Usuarios->free();
				echo json_encode(array(
					"Success"=>true,
					"Result"=>$Rows
				));
			}else{
				echo json_encode(array(
					"Success"=>false,
					"errorCode"=>2,
					"Error"=>$this->DB->error
				));
			}
		}else echo json_encode(array("Success"=>false,"errorCode"=>4));
	}
	private function getClientesUsuarios(){
		if($this->Administrador==1){
			if($Clientes_Usuarios=$this->DB->query("SELECT ".
					"`id_usuario`,`id_cliente` ".
				"FROM `Clientes_Usuarios`"
			)){
				while($Rows[]=mysqli_fetch_assoc($Clientes_Usuarios));
				array_pop($Rows);
				$Clientes_Usuarios->free();
				echo json_encode(array(
					"Success"=>true,
					"Result"=>$Rows
				));
			}else{
				echo json_encode(array(
					"Success"=>false,
					"errorCode"=>2,
					"Error"=>$this->DB->error
				));
			}
		}else echo json_encode(array("Success"=>false,"errorCode"=>4));
	}
	private function setNuevoCupon(){
		if($this->Administrador>0){
			$time_start=microtime(true);
			$id_cupon				=$_POST["Id_Cupon"];
			$titulo					=$_POST["Titulo"];
			$descripcion			=$_POST["Descripcion"];
			$detalle				=$_POST["Detalle"];
			$valido_desde			=$_POST["Valido_Desde"];
			$valido_hasta			=$_POST["Valido_Hasta"];
			$cantidad				=$_POST["Cantidad"];
			$dias_habiles			=$_POST["Dias_Habiles"];
			$precio					=$_POST["Precio"];
			$descuento				=$_POST["Descuento"];
			$imagen					=$_POST["Imagen"];
			$condiciones			=$_POST["Condiciones"];
			if(is_array($condiciones)) $condiciones=serialize($condiciones);
			$id_cliente				=$_POST["ID_Cliente"];
			$nombre_cliente			=$_POST["Nombre_Cliente"];
			$imagen_cliente			=$_POST["Imagen_Cliente"];
			$email					=$_POST["Email"];
			$telefono				=$_POST["Telefono"];
			$horario				=$_POST["Atencion"];
			$informacion			=$_POST["Informacion"];
			$id_categoria			=$_POST["ID_Categoria"];
			$nombre_categoria		=$_POST["Nombre_Categoria"];
			$descripcion_categoria	=$_POST["Descripcion_Categoria"];
			$id_ubicacion			=$_POST["ID_Ubicacion"];
			$direccion_ubicacion	=$_POST["Direccion_Ubicacion"];
			$latitud				=$_POST["Latitud"];
			$longitud				=$_POST["Longitud"];
			$id_horario				=$_POST["ID_Horario"];
			$nombre_horario			=$_POST["Nombre_Horario"];
			$hora_inicio			=$_POST["Hora_Inicio"];
			$hora_fin				=$_POST["Hora_Fin"];

			if($CuponQuery=$this->DB->query("CALL `nuevo_cupon` (".
				"'".$id_cupon."',".
				"'".$titulo."',".
				"'".$descripcion."',".
				"'".$detalle."',".
				"'".$cantidad."',".
				"'".$dias_habiles."',".
				"'".$condiciones."',".
				"'".$valido_desde."',".
				"'".$valido_hasta."',".
				"'".$precio."',".
				"'".$descuento."',".
				"'".$imagen."',".
				"'".$id_cliente."',".
				"'".$nombre_cliente."',".
				"'".$imagen_cliente."',".
				"'".$email."',".
				"'".$telefono."',".
				"'".$horario."',".
				"'".$informacion."',".
				"'".$id_categoria."',".
				"'".$nombre_categoria."',".
				"'".$descripcion_categoria."',".
				"'".$id_ubicacion."',".
				"'".$direccion_ubicacion."',".
				"'".$latitud."',".
				"'".$longitud."',".
				"'".$id_horario."',".
				"'".$nombre_horario."',".
				"'".$hora_inicio."',".
				"'".$hora_fin."');"
			)){
				$Cupon=mysqli_fetch_assoc($CuponQuery);
				$this->DB->next_result();
				$CuponQuery->free();
				$query="INSERT INTO `Codigos` ".
					"(`id_codigo`,`id_cupon`,`id_usuario`,`serie`,`tiempo`) ".
				"VALUES ".
					"(NULL,?,0,?,CONVERT_TZ(NOW(),'UTC','America/Santiago'))";
				$stmt=$this->DB->prepare($query);
				$stmt->bind_param(
					"ds",
					$Cupon["Ultimo_Cupon"],
					$serie
				);
				$this->DB->query("START TRANSACTION");
				for($i=1;$i<=intval($cantidad);$i++){
					$serie=$this->getSerie();
					if(!$stmt->execute()){
						echo json_encode(array(
							"Success"=>false,
							"errorCode"=>2,
							"Error"=>$this->DB->error
						));
						exit();
					}
				}
				$stmt->close();
				$this->DB->query("COMMIT");
				echo json_encode(array(
					"Success"=>true,
					"Response"=>$Cupon,
					"Tiempo"=>(microtime(true)-$time_start)
				));
				if($id_cupon=="0") $estado="creó";
				else $estado="actualizó";
				$InstaPush=new InstaPush();
				$InstaPush->Push("CuponCreado",array(
					"ID"=>$id_cupon,
					"Cupon"=>$titulo,
					"Descripcion"=>$descripcion,
					"Usuario"=>$this->Usuario,
					"Local"=>$nombre_cliente,
					"Estado"=>$estado,
					"Tiempo"=>date("H:i:s d/m/Y")
				));
			}else{
				echo json_encode(array(
					"Success"=>false,
					"errorCode"=>2,
					"Error"=>$this->DB->error
				));
			}
		}else echo json_encode(array("Success"=>false,"errorCode"=>4));
	}
	private function setCuponPagado(){
		if($this->Administrador==1){
			if(isset($_REQUEST["ID"])){
				$id_cupon=$_REQUEST["ID"];
				if($Valorados=$this->DB->query("CALL `habilitar_cupon`(".
					"'".$id_cupon."');"
				)){
					echo json_encode(array("Success"=>true));
				}else{
					echo json_encode(array(
						"Success"=>false,
						"errorCode"=>2,
						"Error"=>$this->DB->error
					));
				}
			}else echo json_encode(array("Success"=>false,"errorCode"=>3));
		}else echo json_encode(array("Success"=>false,"errorCode"=>4));
	}
	private function rmCupon(){
		if($this->Administrador>0){
			if(isset($_REQUEST["ID"])){
				$id_cupon=$_REQUEST["ID"];
				if($Valorados=$this->DB->query("CALL `eliminar_cupon`(".
					"'".$id_cupon."');"
				)){
					echo json_encode(array("Success"=>true));
				}else{
					echo json_encode(array(
						"Success"=>false,
						"errorCode"=>2,
						"Error"=>$this->DB->error
					));
				}
			}else echo json_encode(array("Success"=>false,"errorCode"=>3));
		}else echo json_encode(array("Success"=>false,"errorCode"=>4));
	}
	private function rmCliente(){
		if($this->Administrador==1){
			if(isset($_REQUEST["ID"])){
				$id_cliente=$_REQUEST["ID"];
				if($Valorados=$this->DB->query("CALL `eliminar_cliente`(".
					"'".$id_cliente."');"
				)){
					echo json_encode(array("Success"=>true));
				}else{
					echo json_encode(array(
						"Success"=>false,
						"errorCode"=>2,
						"Error"=>$this->DB->error
					));
				}
			}else echo json_encode(array("Success"=>false,"errorCode"=>3));
		}else echo json_encode(array("Success"=>false,"errorCode"=>4));
	}
	private function setNuevoRatingCupon(){
		if($this->Usuario>1){
			if(isset($_REQUEST["ID"],$_REQUEST["Valoracion"])){
				$id_cupon=$_REQUEST["ID"];
				$valoracion=$_REQUEST["Valoracion"];
				if($Valorados=$this->DB->query("CALL `nueva_evaluacion_cupon`(".
					"'".$id_cupon."',".
					"'".$this->Usuario."',".
					"'".$valoracion."')"
				)){
					$Valorado=mysqli_fetch_assoc($Valorados);
					$Valorados->free();
					echo json_encode(array("Success"=>true,"Result"=>$Valorado));
				}else{
					echo json_encode(array(
						"Success"=>false,
						"errorCode"=>2,
						"Error"=>$this->DB->error
					));
				}
			}else echo json_encode(array("Success"=>false,"errorCode"=>3));
		}else echo json_encode(array("Success"=>false,"errorCode"=>1));
	}
	private function getRatingCupon(){
		if($Valoraciones=$this->DB->query("SELECT ".
				"AVG(`valoracion`) AS `valoracion`,".
				"`C`.`id_cupon`,`titulo` ".
			"FROM `Rating_Cupon` ".
			"LEFT JOIN `Cupones` AS `C` ".
				"ON `Rating_Cupon`.`id_cupon`=`C`.`id_cupon` ".
			"WHERE 1 ".
				$this->FilterDeleted.
			"GROUP BY `C`.`id_cupon` ".
			"ORDER BY `C`.`id_cupon` DESC"
		)){
			while($Rows[]=mysqli_fetch_assoc($Valoraciones));
			array_pop($Rows);
			$Valoraciones->free();
			echo json_encode(array(
				"Success"=>true,
				"Result"=>$Rows
			));
		}else{
			echo json_encode(array(
				"Success"=>false,
				"errorCode"=>2,
				"Error"=>$this->DB->error
			));
		}
	}
	private function setNuevoRatingCliente(){
		if($this->Usuario>1){
			if(isset($_REQUEST["ID"],$_REQUEST["Valoracion"])){
				$id_cliente=$_REQUEST["ID"];
				$valoracion=$_REQUEST["Valoracion"];
				if($Valorados=$this->DB->query("CALL `nueva_evaluacion_cliente`(".
					"'".$id_cliente."',".
					"'".$this->Usuario."',".
					"'".$valoracion."')"
				)){
					$Valorado=mysqli_fetch_assoc($Valorados);
					$Valorados->free();
					echo json_encode(array("Success"=>true,"Result"=>$Valorado));
				}else{
					echo json_encode(array(
						"Success"=>false,
						"errorCode"=>2,
						"Error"=>$this->DB->error
					));
				}
			}else echo json_encode(array("Success"=>false,"errorCode"=>3));
		}else echo json_encode(array("Success"=>false,"errorCode"=>1));
	}
	private function getRatingCliente(){
		if($Valoraciones=$this->DB->query("SELECT ".
				"AVG(`valoracion`) AS `valoracion`,".
				"`CL`.`id_cliente`,`CL`.`nombre` ".
			"FROM `Rating_Cliente` ".
			"LEFT JOIN `Clientes` AS `CL` ".
				"ON `CL`.`id_cliente`=`Rating_Cliente`.`id_cliente`".
			"GROUP BY `CL`.`id_cliente` ".
			"ORDER BY `CL`.`id_cliente` DESC"
		)){
			while($Rows[]=mysqli_fetch_assoc($Valoraciones));
			array_pop($Rows);
			$Valoraciones->free();
			echo json_encode(array(
				"Success"=>true,
				"Result"=>$Rows
			));
		}else{
			echo json_encode(array(
				"Success"=>false,
				"errorCode"=>2,
				"Error"=>$this->DB->error
			));
		}
	}
	private function setNuevaPosicion(){
		if($this->Usuario>1){
			$direccion=$_REQUEST["direccion"];
			$latitud=$_REQUEST["latitud"];
			$longitud=$_REQUEST["longitud"];
			if($this->DB->query("CALL `nuevo_usuario_ubicacion`".
				"('".$this->Usuario."','".$direccion."','".$latitud."','".$longitud."')")
			) echo json_encode(array("Success"=>true));
			else{
				echo json_encode(array(
					"Success"=>false,
					"errorCode"=>2,
					"Error"=>$this->DB->error
				));
			}
		}else echo json_encode(array("Success"=>false,"errorCode"=>1));
	}
	private function getCupones(){
		$time_start=microtime(true);
		if($Cupones=$this->DB->query("SELECT ".
			$this->SelectCupon.
			$this->JoinCupon.
			$this->FilterAdmin().
			$this->FilterDeleted.
			$this->FilterDetached.
			$this->GroupRating.
			$this->Order.
			$this->Pagination
		)){
			while($Rows[]=mysqli_fetch_assoc($Cupones));
			array_pop($Rows);
			$Cupones->free();
			$Totales=$this->DB->query("SELECT FOUND_ROWS() AS `Total`");
			$Total=mysqli_fetch_assoc($Totales);
			$Totales->free();
			echo json_encode(array(
				"Success"=>true,
				"Result"=>$Rows,
				"Total"=>$Total["Total"],
				"Tiempo"=>(microtime(true)-$time_start)
			));
		}else{
			echo json_encode(array(
				"Success"=>false,
				"errorCode"=>2,
				"Error"=>$this->DB->error
			));
		}
	}
	private function getCuponesPendientes(){
		if($this->Administrador==1){
			$time_start=microtime(true);
			if($Cupones=$this->DB->query("SELECT ".
				$this->SelectCupon.
				$this->JoinCupon.
				$this->FilterAdmin().
				"AND `C`.`estado`=2 ".
				$this->FilterDetached.
				$this->GroupRating.
				$this->Order
			)){
				while($Rows[]=mysqli_fetch_assoc($Cupones));
				array_pop($Rows);
				$Cupones->free();
				$Totales=$this->DB->query("SELECT FOUND_ROWS() AS `Total`");
				$Total=mysqli_fetch_assoc($Totales);
				$Totales->free();
				echo json_encode(array(
					"Success"=>true,
					"Result"=>$Rows,
					"Total"=>$Total["Total"],
					"Tiempo"=>(microtime(true)-$time_start)
				));
			}else{
				echo json_encode(array(
					"Success"=>false,
					"errorCode"=>2,
					"Error"=>$this->DB->error
				));
			}
		}else echo json_encode(array("Success"=>false,"errorCode"=>4));
	}
	private function getCuponesDisponibles(){
		$query="SELECT ".
			$this->SelectCupon.
			$this->JoinCupon.
			$this->FilterTime.
			$this->FilterOverSell.
			$this->FilterDeleted.
			$this->FilterDetached;
		if(!isset($_REQUEST["Todos"])){
			$query.=$this->FilterHour;
		}
		if(isset($_REQUEST["Fecha"])){
			$query.=$this->FilterDate();
		}
		$query.=$this->GroupRating.
			$this->Order.
			$this->Pagination;
		$time_start=microtime(true);
		if($Cupones=$this->DB->query($query)){
			while($Rows[]=mysqli_fetch_assoc($Cupones));
			array_pop($Rows);
			$Cupones->free();
			$Totales=$this->DB->query("SELECT FOUND_ROWS() AS `Total`");
			$Total=mysqli_fetch_assoc($Totales);
			$Totales->free();
			echo json_encode(array(
				"Success"=>true,
				"Result"=>$Rows,
				"Total"=>$Total["Total"],
				"Tiempo"=>(microtime(true)-$time_start)
			));
		}else{
			echo json_encode(array(
				"Success"=>false,
				"errorCode"=>2,
				"Error"=>$this->DB->error
			));
		}
	}
	private function getCuponCategoria(){
		if(isset($_REQUEST["ID"])){
			$C=$_REQUEST["ID"];
			$time_start=microtime(true);
			if($Cupones=$this->DB->query("SELECT ".
				$this->SelectCupon.
				$this->JoinCupon.
				$this->FilterTime.
				$this->FilterOverSell.
				$this->FilterDeleted.
				$this->FilterDetached.
					"AND `CC`.`id_categoria`=".$C." ".
				$this->GroupRating.
				$this->Order.
				$this->Pagination
			)){
				while($Rows[] = mysqli_fetch_assoc($Cupones));
				array_pop($Rows);
				$Cupones->free();
				$Totales=$this->DB->query("SELECT FOUND_ROWS() AS `Total`");
				$Total=mysqli_fetch_assoc($Totales);
				$Totales->free();
				echo json_encode(array(
					"Success"=>true,
					"Result"=>$Rows,
					"Total"=>$Total["Total"],
					"Tiempo"=>(microtime(true)-$time_start)
				));
			}else{
				echo json_encode(array(
					"Success"=>false,
					"errorCode"=>2,
					"Error"=>$this->DB->error
				));
			}
		}else echo json_encode(array("Success"=>false,"errorCode"=>3));
	}
	private function getCuponCliente(){
		if(isset($_REQUEST["ID"])){
			$C=$_REQUEST["ID"];
			$time_start=microtime(true);
			if($Cupones=$this->DB->query("SELECT ".
				$this->SelectCupon.
				$this->JoinCupon.
				$this->FilterTime.
				$this->FilterOverSell.
				$this->FilterDeleted.
				$this->FilterDetached.
					"AND `CL`.`id_cliente`=".$C." ".
				$this->GroupRating.
				$this->Order.
				$this->Pagination
			)){
				while($Rows[]=mysqli_fetch_assoc($Cupones));
				array_pop($Rows);
				$Cupones->free();
				$Totales=$this->DB->query("SELECT FOUND_ROWS() AS `Total`");
				$Total=mysqli_fetch_assoc($Totales);
				$Totales->free();
				echo json_encode(array(
					"Success"=>true,
					"Result"=>$Rows,
					"Total"=>$Total["Total"],
					"Tiempo"=>(microtime(true)-$time_start)
				));
			}else{
				echo json_encode(array(
					"Success"=>false,
					"errorCode"=>2,
					"Error"=>$this->DB->error
				));
			}
		}else echo json_encode(array("Success"=>false,"errorCode"=>3));
	}
	private function getCuponUbicacion(){
		if(isset($_REQUEST["ID"])){
			$id_ubicacion=$_REQUEST["ID"];
			$time_start=microtime(true);
			if($Cupones=$this->DB->query("SELECT ".
				$this->SelectCupon.
				$this->JoinCupon.
				$this->FilterTime.
				$this->FilterOverSell.
				$this->FilterDeleted.
				$this->FilterDetached.
					"AND `U`.`id_ubicacion`=".$id_ubicacion." ".
				$this->GroupRating.
				$this->Order.
				$this->Pagination
			)){
				while($Rows[]=mysqli_fetch_assoc($Cupones));
				array_pop($Rows);
				$Cupones->free();
				$Totales=$this->DB->query("SELECT FOUND_ROWS() AS `Total`");
				$Total=mysqli_fetch_assoc($Totales);
				$Totales->free();
				echo json_encode(array(
					"Success"=>true,
					"Result"=>$Rows,
					"Total"=>$Total["Total"]
				));
			}else{
				echo json_encode(array(
					"Success"=>false,
					"errorCode"=>2,
					"Error"=>$this->DB->error
				));
			}
		}else echo json_encode(array("Success"=>false,"errorCode"=>3));
	}
	private function getCuponCercano(){
		if(isset($_REQUEST["Latitud"],$_REQUEST["Longitud"])){
			$latitud=$_REQUEST["Latitud"];
			$longitud=$_REQUEST["Longitud"];
			$radio=5;
			$unidad=111.045;
			$circulo=$radio/$unidad;
			
			$time_start=microtime(true);
			$Query="SELECT ".
				$this->SelectCupon.
				",".$unidad.
					"* DEGREES(ACOS(COS(RADIANS(".$latitud.")) ".
					"* COS(RADIANS(`U`.`latitud`)) ".
					"* COS(RADIANS(".$longitud." - `U`.`longitud`)) ".
					"+ SIN(RADIANS(".$latitud.")) ".
					"* SIN(RADIANS(`U`.`latitud`))".
				")) AS `distancia` ".
				$this->JoinCupon.
				$this->FilterTime.
				$this->FilterOverSell.
				$this->FilterDeleted.
				$this->FilterDetached.
				"AND `U`.`latitud` BETWEEN ".
					$latitud."-".$circulo." ".
				"AND ".
					$latitud."+".$circulo." ".
				"AND `U`.`longitud` BETWEEN ".
					$longitud."-(".$radio."/(".$unidad."*COS(RADIANS(".$longitud.")))) ".
				"AND ".
					$longitud."+(".$radio."/(".$unidad."*COS(RADIANS(".$longitud.")))) ".
				$this->GroupRating.
				"HAVING distancia<=".$radio." ".
				$this->Order.
				$this->Pagination;
			echo json_encode(array(
				"Success"=>true,
				"Query"=>$Query
			));
			/*
			if($Cupones=$this->DB->query($Query)){
				while($Rows[]=mysqli_fetch_assoc($Cupones));
				array_pop($Rows);
				$Cupones->free();
				$Totales=$this->DB->query("SELECT FOUND_ROWS() AS `Total`");
				$Total=mysqli_fetch_assoc($Totales);
				$Totales->free();
				echo json_encode(array(
					"Success"=>true,
					"Result"=>$Rows,
					"Total"=>$Total["Total"]
				));
			}else{
				echo json_encode(array(
					"Success"=>false,
					"errorCode"=>2,
					"Error"=>$this->DB->error
				));
			}
			*/
		}else echo json_encode(array("Success"=>false,"errorCode"=>3));
	}
	private function getCuponBuscado(){
		if(isset($_REQUEST["Termino"])){
			$termino=$_REQUEST["Termino"];
			$time_start=microtime(true);
			if($Cupones=$this->DB->query("SELECT ".
				$this->SelectCupon.
				$this->JoinCupon.
				$this->FilterTime.
				$this->FilterOverSell.
				$this->FilterDeleted.
				$this->FilterDetached.
					"AND (`C`.`titulo` LIKE '%".$termino."%' ".
					"OR `C`.`descripcion` LIKE '%".$termino."%') ".
				$this->GroupRating.
				$this->Order.
				$this->Pagination
			)){
				while($Rows[]=mysqli_fetch_assoc($Cupones));
				array_pop($Rows);
				$Cupones->free();
				$Totales=$this->DB->query("SELECT FOUND_ROWS() AS `Total`");
				$Total=mysqli_fetch_assoc($Totales);
				$Totales->free();
				echo json_encode(array(
					"Success"=>true,
					"Result"=>$Rows,
					"Total"=>$Total["Total"],
					"Tiempo"=>(microtime(true)-$time_start)
				));
			}else{
				echo json_encode(array(
					"Success"=>false,
					"errorCode"=>2,
					"Error"=>$this->DB->error
				));
			}
		}else echo json_encode(array("Success"=>false,"errorCode"=>3));
	}
	private function getCuponID(){
		if(isset($_REQUEST["ID"])){
			$id_cupon=$_REQUEST["ID"];
			if($this->Administrador>0){
				$query="SELECT ".
				$this->SelectCupon.
				$this->JoinCupon.
					"WHERE `C`.`id_cupon`=".$id_cupon.
				" AND `C`.`estado` IN (1,2)";
			}else{
				$query="SELECT ".
				$this->SelectCupon.
				$this->JoinCupon.
				$this->FilterTime.
				$this->FilterOverSell.
				$this->FilterDeleted.
				$this->FilterDetached.
					"AND `C`.`id_cupon`=".$id_cupon;
			}
			$time_start=microtime(true);
			if($Cupones=$this->DB->query($query)){
				while($Rows[]=mysqli_fetch_assoc($Cupones));
				array_pop($Rows);
				$Cupones->free();
				echo json_encode(array(
					"Success"=>true,
					"Result"=>$Rows,
					"Tiempo"=>(microtime(true)-$time_start)
				));
				if($this->Administrador==0){
					$this->DB->query("CALL `nueva_visita` ($id_cupon,$this->Usuario,2);");
				}
			}else{
				echo json_encode(array(
					"Success"=>false,
					"errorCode"=>2,
					"Error"=>$this->DB->error
				));
			}
		}else echo json_encode(array("Success"=>false,"errorCode"=>3));
	}
	private function getCuponDestacado(){
		$time_start=microtime(true);
		if($Cupones=$this->DB->query("SELECT ".
			$this->SelectCupon.
			$this->JoinCupon.
			$this->FilterTime.
			$this->FilterOverSell.
			$this->FilterDeleted.
			$this->FilterDetached.
			$this->GroupRating.
			" ORDER BY RAND() ".
			$this->Pagination
		)){
			while($Rows[]=mysqli_fetch_assoc($Cupones));
			array_pop($Rows);
			$Cupones->free();
			$Totales=$this->DB->query("SELECT FOUND_ROWS() AS `Total`");
			$Total=mysqli_fetch_assoc($Totales);
			$Totales->free();
			echo json_encode(array(
				"Success"=>true,
				"Result"=>$Rows,
				"Total"=>$Total["Total"],
				"Tiempo"=>(microtime(true)-$time_start)
			));
		}else{
			echo json_encode(array(
				"Success"=>false,
				"errorCode"=>2,
				"Error"=>$this->DB->error
			));
		}
	}
	private function getCuponesVisitados(){
		if(isset($_REQUEST["Rango"])){
			$time_start=microtime(true);
			$Query="SELECT ".
				$this->SelectCupon.
				$this->JoinCupon.
				"INNER JOIN `Visitas` AS `V` ".
					"ON `V`.`id_cupon`=`C`.`id_cupon` ".
				"WHERE `V`.`tipo`=1 ";
			if(isset($_REQUEST["Hora"])){
				$Query.="AND HOUR(`V`.`tiempo`)=".intval($_REQUEST["Hora"])." ";
			}
			if(isset($_REQUEST["Dia"])){
				$Query.="AND WEEKDAY(`V`.`tiempo`)=".intval($_REQUEST["Dia"])." ";
			}
			if(isset($_REQUEST["Categoria"])){
				$categoria=$_REQUEST["Categoria"];
				$Query.="AND `CA`.`nombre`='".$categoria."' ";
			}
			if(!isset($_REQUEST["Eliminados"]) || $_REQUEST["Eliminados"]=="false"){
				$Query.=$this->FilterDetached.
					$this->FilterDeleted;
			}
			if(!isset($_REQUEST["Vencidos"]) || $_REQUEST["Vencidos"]=="false"){
				$Query.=str_replace("WHERE","AND",$this->FilterTime);
			}
			if(!isset($_REQUEST["Acabados"]) || $_REQUEST["Acabados"]=="false"){
				$Query.=$this->FilterOverSell;
			}
			$Query.=$this->FilterRange().
				$this->GroupRating.
				$this->Pagination;
			if($Cupones=$this->DB->query($Query)){
				while($Rows[]=mysqli_fetch_assoc($Cupones));
				array_pop($Rows);
				$Cupones->free();
				$Totales=$this->DB->query("SELECT FOUND_ROWS() AS `Total`");
				$Total=mysqli_fetch_assoc($Totales);
				$Totales->free();
				echo json_encode(array(
					"Success"=>true,
					"Result"=>$Rows,
					"Total"=>$Total["Total"],
					"Tiempo"=>(microtime(true)-$time_start)
				));
			}else{
				echo json_encode(array(
					"Success"=>false,
					"errorCode"=>2,
					"Error"=>$this->DB->error
				));
			}
		}else echo json_encode(array("Success"=>false,"errorCode"=>3));
	}
	private function getImageCupon(){
		if(isset($_REQUEST["ID"])){
			$id_cupon=$_REQUEST["ID"];
			$time_start=microtime(true);
			if($Imagenes=$this->DB->query("SELECT `imagen` ".
				"FROM `Cupones` ".
				"WHERE `id_cupon`=".$id_cupon
			)){
				while($Rows[]=mysqli_fetch_assoc($Imagenes));
				array_pop($Rows);
				$Imagenes->free();
				echo json_encode(array(
					"Success"=>true,
					"Result"=>$Rows,
					"Tiempo"=>(microtime(true)-$time_start)
				));
				if($this->Administrador==0){
					$this->DB->query("CALL `nueva_visita` ($id_cupon,$this->Usuario,1);");
				}
			}else{
				echo json_encode(array(
					"Success"=>false,
					"errorCode"=>2,
					"Error"=>$this->DB->error
				));
			}
		}else echo json_encode(array("Success"=>false,"errorCode"=>3));
	}
	private function getCategoriasDisponibles(){
		$time_start=microtime(true);
		if($Categorias=$this->DB->query("SELECT ".
			"DISTINCT(`CA`.`id_categoria`),`CA`.`nombre`,".
			"`CA`.`descripcion` ".
			"FROM `Categorias` AS `CA` ".
			"LEFT JOIN `Cupon_Categoria` AS `CC` ".
				"ON `CC`.`id_categoria`=`CA`.`id_categoria` ".
			"LEFT JOIN `Cupones` AS `C` ".
				"ON `C`.`id_cupon`=`CC`.`id_cupon` ".
			$this->FilterTime.
			$this->FilterOverSell.
			$this->FilterDeleted.
			"LIMIT 5"
		)){
			while($Rows[]=mysqli_fetch_assoc($Categorias));
			array_pop($Rows);
			$Categorias->free();
			echo json_encode(array(
				"Success"=>true,
				"Result"=>$Rows,
				"Tiempo"=>(microtime(true)-$time_start)
			));
		}else{
			echo json_encode(array(
				"Success"=>false,
				"errorCode"=>2,
				"Error"=>$this->DB->error
			));
		}
	}
	private function getCategorias(){
		if($Categorias=$this->DB->query("SELECT * FROM `Categorias`")){
			while($Rows[]=mysqli_fetch_assoc($Categorias));
			array_pop($Rows);
			$Categorias->free();
			echo json_encode(array(
				"Success"=>true,
				"Result"=>$Rows
			));
		}else{
			echo json_encode(array(
				"Success"=>false,
				"errorCode"=>2,
				"Error"=>$this->DB->error
			));
		}
	}
	private function getClientes(){
		if($this->Administrador>0){
			if($Clientes=$this->DB->query("SELECT `CL`.* ".
				"FROM `Clientes` AS `CL`".
				$this->FilterAdmin().
				$this->FilterDetached
			)){
				while($Rows[]=mysqli_fetch_assoc($Clientes));
				array_pop($Rows);
				$Clientes->free();
				echo json_encode(array(
					"Success"=>true,
					"Result"=>$Rows
				));
			}else{
				echo json_encode(array(
					"Success"=>false,
					"errorCode"=>2,
					"Error"=>$this->DB->error
				));
			}
		}else echo json_encode(array("Success"=>false,"errorCode"=>4));
	}
	private function getImageCliente(){
		if(isset($_REQUEST["ID"])){
			$C=$_REQUEST["ID"];
			if($Imagenes=$this->DB->query("SELECT `imagen` ".
				"FROM `Clientes` ".
				"WHERE `id_cliente`=".$C
			)){
				while($Rows[]=mysqli_fetch_assoc($Imagenes));
				array_pop($Rows);
				$Imagenes->free();
				echo json_encode(array(
					"Success"=>true,
					"Result"=>$Rows
				));
			}else{
				echo json_encode(array(
					"Success"=>false,
					"errorCode"=>2,
					"Error"=>$this->DB->error
				));
			}
		}else echo json_encode(array("Success"=>false,"errorCode"=>3));
	}
	private function getCodigos(){
		if($this->Usuario>1){
			if($Codigos=$this->DB->query("SELECT * FROM `Codigos` WHERE `id_usuario`=$this->Usuario")){
				while($Rows[]=mysqli_fetch_assoc($Codigos));
				array_pop($Rows);
				$Codigos->free();
				echo json_encode(array(
					"Success"=>true,
					"Result"=>$Rows
				));
			}else{
				echo json_encode(array(
					"Success"=>false,
					"errorCode"=>2,
					"Error"=>$this->DB->error
				));
			}
		}else echo json_encode(array("Success"=>false,"errorCode"=>1));
	}
	private function getCodigosCliente(){
		if($this->Usuario>0){
			$Query="SELECT ".
					"`CO`.`id_codigo`,".
					"`CO`.`serie`,".
					"`CO`.`recordado`,".
					"`CO`.`cobrado`,".
					"DATE(`CO`.`tiempo`) AS `emision`,".
					"`C`.`id_cupon`,".
					"`C`.`titulo`,".
					"`C`.`valido_desde`,".
					"`C`.`valido_hasta`,".
					"`U`.`id_usuario`,".
					"`U`.`first_name`,".
					"`CL`.`id_cliente` ".
				"FROM `Codigos` AS `CO` ".
				"LEFT JOIN `Cupones` AS `C` ".
					"ON `C`.`id_cupon`=`CO`.`id_cupon` ".
				"LEFT JOIN `Usuarios` AS `U` ".
					"ON `U`.`id_usuario`=`CO`.`id_usuario` ".
				"LEFT JOIN `Clientes` AS `CL` ".
					"ON `CL`.`id_cliente`=`C`.`id_cliente` ";
			if($this->Administrador>0){
				$Query.=$this->FilterAdmin();
			}else{
				$Query.="WHERE 1 ";
			}
			if(isset($_REQUEST["Perfil"]) || $this->Administrador==0){
				$Query.="AND `U`.`id_usuario`=".$this->Usuario." ";
			}
			if(!isset($_REQUEST["Eliminados"]) || $_REQUEST["Eliminados"]=="false"){
				$Query.=$this->FilterDeleted.
					$this->FilterDetached;
			}
			if(!isset($_REQUEST["Vencidos"]) || $_REQUEST["Vencidos"]=="false"){
				$Query.=str_replace("WHERE","AND",$this->FilterTime);
			}
			$Query.=$this->Order;
			if($Codigos=$this->DB->query($Query)){
				while($Rows[]=mysqli_fetch_assoc($Codigos));
				array_pop($Rows);
				$Codigos->free();
				echo json_encode(array(
					"Success"=>true,
					"Result"=>$Rows
				));
			}else{
				echo json_encode(array(
					"Success"=>false,
					"errorCode"=>2,
					"Error"=>$this->DB->error
				));
			}
		}else echo json_encode(array("Success"=>false,"errorCode"=>1));
	}
	private function setCodigoCobrado(){
		if($this->Administrador>0){
			if(isset($_REQUEST["ID"])){
				$id=$_REQUEST["ID"];
				if($Codigos=$this->DB->query("CALL `validar_codigo` (".
					$id.",".$this->Usuario.")"
				)){
					$Codigo=mysqli_fetch_assoc($Codigos);
					$this->DB->next_result();
					$Codigos->free();
					echo json_encode(array(
						"Success"=>true,
						"Result"=>$Codigo
					));
				}else{
					echo json_encode(array(
						"Success"=>false,
						"errorCode"=>2,
						"Error"=>$this->DB->error
					));
				}
			}else echo json_encode(array("Success"=>false,"errorCode"=>3));
		}else echo json_encode(array("Success"=>false,"errorCode"=>4));
	}
	private function getHorariosDisponibles(){
		if($Horarios=$this->DB->query("SELECT ".
			"DISTINCT(`H`.`id_horario`),`H`.`nombre`,".
			"`H`.`hora_inicio`,`H`.`hora_fin` ".
			"FROM `Horarios` AS `H` ".
			"LEFT JOIN `Cupon_Horario` AS `CH` ".
				"ON `CH`.`id_horario`=`H`.`id_horario` ".
			"LEFT JOIN `Cupones` AS `C` ".
				"ON `C`.`id_cupon`=`CH`.`id_cupon` ".
			$this->FilterTime.
			$this->FilterOverSell.
			$this->FilterDeleted
		)){
			while($Rows[]=mysqli_fetch_assoc($Horarios));
			array_pop($Rows);
			$Horarios->free();
			echo json_encode(array(
				"Success"=>true,
				"Result"=>$Rows
			));
		}else{
			echo json_encode(array(
				"Success"=>false,
				"errorCode"=>2,
				"Error"=>$this->DB->error
			));
		}
	}
	private function getHorarios(){
		if($Horarios=$this->DB->query("SELECT * ".
			"FROM `Horarios` "
		)){
			while($Rows[]=mysqli_fetch_assoc($Horarios));
			array_pop($Rows);
			$Horarios->free();
			echo json_encode(array(
				"Success"=>true,
				"Result"=>$Rows
			));
		}else{
			echo json_encode(array(
				"Success"=>false,
				"errorCode"=>2,
				"Error"=>$this->DB->error
			));
		}
	}
	private function getUbicaciones(){
		if($Ubicaciones=$this->DB->query("SELECT * ".
			"FROM `Ubicaciones` "
		)){
			while($Rows[]=mysqli_fetch_assoc($Ubicaciones));
			array_pop($Rows);
			$Ubicaciones->free();
			echo json_encode(array(
				"Success"=>true,
				"Result"=>$Rows
			));
		}else{
			echo json_encode(array(
				"Success"=>false,
				"errorCode"=>2,
				"Error"=>$this->DB->error
			));
		}
	}
	private function getUbicacionesDisponibles(){
		if($Ubicaciones=$this->DB->query("SELECT ".
			"DISTINCT(`U`.`id_ubicacion`),`U`.`direccion`,".
			"`U`.`latitud`,`U`.`longitud` ".
			"FROM `Ubicaciones` AS `U` ".
			"LEFT JOIN `Ubicacion_Cupon` AS `UC` ".
				"ON `UC`.`id_ubicacion`=`U`.`id_ubicacion` ".
			"LEFT JOIN `Cupones` AS `C` ".
				"ON `C`.`id_cupon`=`UC`.`id_cupon` ".
			$this->FilterTime.
			$this->FilterOverSell.
			$this->FilterDeleted
		)){
			while($Rows[]=mysqli_fetch_assoc($Ubicaciones));
			array_pop($Rows);
			$Ubicaciones->free();
			echo json_encode(array(
				"Success"=>true,
				"Result"=>$Rows
			));
		}else{
			echo json_encode(array(
				"Success"=>false,
				"errorCode"=>2,
				"Error"=>$this->DB->error
			));
		}
	}
	private function getUbicacionID(){
		if(isset($_REQUEST["ID"])){
			$ID=$_REQUEST["ID"];
			if($Ubicacion=$this->DB->query("SELECT * ".
				"FROM `Ubicaciones` ".
				"WHERE `id_ubicacion`=".$ID.
				" LIMIT 1"
			)){
				$Row=mysqli_fetch_assoc($Ubicacion);
				$Ubicacion->free();
				echo json_encode(array(
					"Success"=>true,
					"Result"=>$Row
				));
			}else{
				echo json_encode(array(
					"Success"=>false,
					"errorCode"=>2,
					"Error"=>$this->DB->error
				));
			}
		}else echo json_encode(array("Success"=>false,"errorCode"=>3));
	}
	private function getLikes(){
		$Query="SELECT `C`.`id_cupon`,COUNT(`V`.`id_cupon`) AS `numero`,".
			"`titulo` ".
			"FROM `Likes` AS `V` ".
			"LEFT JOIN `Cupones` AS `C` ".
				"ON `V`.`id_cupon`=`C`.`id_cupon` ".
			"LEFT JOIN `Clientes` AS `CL` ".
				"ON `CL`.`id_cliente`=`C`.`id_cliente` ".
			$this->FilterAdmin().
			$this->FilterRange();
		if(!isset($_REQUEST["Eliminados"]) || $_REQUEST["Eliminados"]=="false"){
			$Query.=$this->FilterDetached.
				$this->FilterDeleted;
		}
		if(!isset($_REQUEST["Vencidos"]) || $_REQUEST["Vencidos"]=="false"){
			$Query.=str_replace("WHERE","AND",$this->FilterTime);
		}
		if(!isset($_REQUEST["Acabados"]) || $_REQUEST["Acabados"]=="false"){
			$Query.=$this->FilterOverSell;
		}
		$Query.="AND `V`.`estado`=1 ".
			"GROUP BY `V`.`id_cupon` ".
			"ORDER BY `numero` DESC";
		if($Likes=$this->DB->query($Query)){
			while($Rows[]=mysqli_fetch_assoc($Likes));
			array_pop($Rows);
			$Likes->free();
			echo json_encode(array(
				"Success"=>true,
				"Result"=>$Rows
			));
		}else{
			echo json_encode(array(
				"Success"=>false,
				"Query"=>$Query,
				"errorCode"=>2,
				"Error"=>$this->DB->error
			));
		}
	}
	private function getLikesID(){
		if(isset($_REQUEST["ID"])){
			$ID=$_REQUEST["ID"];
			if($Likes=$this->DB->query("SELECT COUNT(*) AS `Numero` ".
				"FROM `Likes` AS `V`".
				"LEFT JOIN `Cupones` AS `C` ".
					"ON `V`.`id_cupon`=`C`.`id_cupon` ".
				"WHERE `V`.`id_cupon`=".$ID.
				" AND `V`.`estado`=1 ".
				$this->FilterDeleted
			)){
				$Row=mysqli_fetch_assoc($Likes);
				$Likes->free();
				echo json_encode(array(
					"Success"=>true,
					"Result"=>$Row
				));
			}else{
				echo json_encode(array(
					"Success"=>false,
					"errorCode"=>2,
					"Error"=>$this->DB->error
				));
			}
		}else echo json_encode(array("Success"=>false,"errorCode"=>3));
	}
	private function setLike(){
		if(isset($_REQUEST["ID"])){
			$id_cupon=$_REQUEST["ID"];
			if($this->DB->query("CALL `nuevo_like` (".$id_cupon.",".$this->Usuario.");"
			)) echo json_encode(array("Success"=>true));
			else{
				echo json_encode(array(
					"Success"=>false,
					"errorCode"=>2,
					"Error"=>$this->DB->error
				));
			}
		}else echo json_encode(array("Success"=>false,"errorCode"=>3));
	}
	private function rmLike(){
		if(isset($_REQUEST["ID"])){
			$id_cupon=$_REQUEST["ID"];
			if($this->Usuario>1){
				if($this->DB->query("CALL `eliminar_like` (".$id_cupon.",".$this->Usuario.");"
				)) echo json_encode(array("Success"=>true));
				else{
					echo json_encode(array(
						"Success"=>false,
						"errorCode"=>2,
						"Error"=>$this->DB->error
					));
				}
			}else echo json_encode(array("Success"=>true));
		}else echo json_encode(array("Success"=>false,"errorCode"=>3));
	}
	private function getVisitasCupones(){
		if($this->Administrador>0){
			if(isset($_REQUEST["Orden"],$_REQUEST["Grupo"],$_REQUEST["Rango"])){
				$time_start=microtime(true);
				$Query="SELECT ".
					"COUNT(*) AS `numero`,`V`.`tipo`,DATE(`V`.`tiempo`) AS `Dia`,".
					"HOUR(`V`.`tiempo`) AS `Hora`,`C`.`id_cupon`,`C`.`titulo`,".
					"WEEKDAY(`V`.`tiempo`) AS `dia_semana`,`CA`.`id_categoria`,".
					"`CA`.`nombre` AS `nombre_categoria` ".
					"FROM `Visitas` AS `V` ".
					"LEFT JOIN `Cupones` AS `C` ".
						"ON `V`.`id_cupon`=`C`.`id_cupon` ".
					"LEFT JOIN `Clientes` AS `CL` ".
						"ON `CL`.`id_cliente`=`C`.`id_cliente` ".
					"LEFT JOIN `Cupon_Categoria` AS `CC` ".
						"ON `CC`.`id_cupon`=`C`.`id_cupon` ".
					"LEFT JOIN `Categorias` AS `CA` ".
						"ON `CA`.`id_categoria`=`CC`.`id_categoria` ".
					$this->FilterAdmin().
					$this->FilterRange();
				if(!isset($_REQUEST["Eliminados"]) || $_REQUEST["Eliminados"]=="false"){
					$Query.=$this->FilterDetached.
						$this->FilterDeleted;
				}
				if(!isset($_REQUEST["Vencidos"]) || $_REQUEST["Vencidos"]=="false"){
					$Query.=str_replace("WHERE","AND",$this->FilterTime);
				}
				if(!isset($_REQUEST["Acabados"]) || $_REQUEST["Acabados"]=="false"){
					$Query.=$this->FilterOverSell;
				}
				$Query.="GROUP BY ".$_REQUEST["Grupo"]." ".
					"ORDER BY ".$_REQUEST["Orden"]." DESC";
				if($Visitas=$this->DB->query($Query)){
					while($Rows[]=mysqli_fetch_assoc($Visitas));
					array_pop($Rows);
					$Visitas->free();
					echo json_encode(array(
						"Success"=>true,
						"Result"=>$Rows,
						"Tiempo"=>microtime(true)-$time_start
					));
				}else{
					echo json_encode(array(
						"Success"=>false,
						"errorCode"=>2,
						"Query"=>$Query,
						"Error"=>$this->DB->error
					));
				}
			}else echo json_encode(array("Success"=>false,"errorCode"=>3));
		}else echo json_encode(array("Success"=>false,"errorCode"=>4));
	}
	private function getVisitasCuponID(){
		if($this->Administrador>0){
			if(isset($_REQUEST["ID"])){
				$ID=$_REQUEST["ID"];
				$query=" SELECT ".
					"SQL_CALC_FOUND_ROWS ".
					"COUNT(*) AS `Numero`,`tipo`,`id_cupon`, DATE(`tiempo`) AS `Dia` ".
					"FROM `Visitas` ".
					"WHERE `id_cupon`=".$ID.
					" GROUP BY `Dia`,`tipo`,`id_cupon` ".
					" ORDER BY `id_visita` ASC ";
				if(isset($_REQUEST["Semana"])){
					$query.="LIMIT ".(intval($_REQUEST["Semana"])*7).",7;";
				}else $query.="LIMIT 7;";
				if($Visitas=$this->DB->query($query)){
					while($Rows[]=mysqli_fetch_assoc($Visitas));
					array_pop($Rows);
					$Visitas->free();
					$Totales=$this->DB->query("SELECT FOUND_ROWS() AS `Total`");
					$Total=mysqli_fetch_assoc($Totales);
					$Totales->free();
					echo json_encode(array(
						"Success"=>true,
						"Result"=>$Rows,
						"Total"=>$Total["Total"]
					));
				}else{
					echo json_encode(array(
						"Success"=>false,
						"errorCode"=>2,
						"Error"=>$this->DB->error
					));
				}
			}else echo json_encode(array("Success"=>false,"errorCode"=>3));
		}else echo json_encode(array("Success"=>false,"errorCode"=>4));
	}
	private function getVentas(){
		if($this->Administrador>0){
			if(isset($_REQUEST["Rango"])){
				$time_start=microtime(true);
				$Query="SELECT ".
					"COUNT(`C`.`id_cupon`) AS `numero`,`C`.`id_cupon`, ".
					"`C`.`titulo`,`C`.`restantes`".
					"FROM `Cupones` AS `C` ".
					"LEFT JOIN `Codigos` AS `V` ".
						"ON `V`.`id_cupon`=`C`.`id_cupon` ".
					"LEFT JOIN `Clientes` AS `CL` ".
						"ON `CL`.`id_cliente`=`C`.`id_cliente` ".
					$this->FilterAdmin().
					$this->FilterRange();
				if(!isset($_REQUEST["Eliminados"]) || $_REQUEST["Eliminados"]=="false"){
					$Query.=$this->FilterDetached.
						$this->FilterDeleted;
				}
				if(!isset($_REQUEST["Vencidos"]) || $_REQUEST["Vencidos"]=="false"){
					$Query.=str_replace("WHERE","AND",$this->FilterTime);
				}
				if(!isset($_REQUEST["Acabados"]) || $_REQUEST["Acabados"]=="false"){
					$Query.=$this->FilterOverSell;
				}
				$Query.="AND `V`.`id_usuario`!=0 ".
					"GROUP BY `C`.`id_cupon` ".
					"ORDER BY `numero` DESC";
				if($Ventas=$this->DB->query($Query)){
					while($Rows[]=mysqli_fetch_assoc($Ventas));
					array_pop($Rows);
					$Ventas->free();
					echo json_encode(array(
						"Success"=>true,
						"Result"=>$Rows,
						"Tiempo"=>microtime(true)-$time_start
					));
				}else{
					echo json_encode(array(
						"Success"=>false,
						"errorCode"=>2,
						"Query"=>$Query,
						"Error"=>$this->DB->error
					));
				}
			}else echo json_encode(array("Success"=>false,"errorCode"=>3));
		}else echo json_encode(array("Success"=>false,"errorCode"=>4));
	}
	private function setDenuncia(){
		if($this->Usuario>1){
			if(isset($_REQUEST["ID"],$_REQUEST["Cupon"])){
				$tipo=$_REQUEST["ID"];
				$id_cupon=$_REQUEST["Cupon"];
				if($this->DB->query("CALL `nueva_denuncia` (".
					"'".$id_cupon."',".
					"'".$this->Usuario."',".
					"'".$tipo."');"
				)) echo json_encode(array("Success"=>true));
				else{
					echo json_encode(array(
						"Success"=>false,
						"errorCode"=>2,
						"Error"=>$this->DB->error
					));
				}
			}else{
				echo json_encode(array(
					"Success"=>false,
					"errorCode"=>3
				));
			}
		}else echo json_encode(array("Success"=>false,"errorCode"=>1));
	}
	private function getDenuncias(){
		if($this->Administrador>0){
			$time_start=microtime(true);
			if($Denuncias=$this->DB->query("SELECT ".
					"COUNT(*) AS `numero`,`tipo`,".
					"`C`.`id_cupon`,`C`.`titulo` ".
				"FROM `Denuncias` ".
				"LEFT JOIN `Cupones` AS `C` ".
					"ON `Denuncias`.`id_cupon`=`C`.`id_cupon` ".
				"LEFT JOIN `Clientes` AS `CL` ".
					"ON `CL`.`id_cliente`=`C`.`id_cliente` ".
				$this->FilterAdmin().
				$this->FilterDetached.
				$this->FilterDeleted.
				"GROUP BY `tipo`,`C`.`id_cupon` ".
				"ORDER BY `C`.`id_cupon` DESC"
			)){
				while($Rows[]=mysqli_fetch_assoc($Denuncias));
				array_pop($Rows);
				$Denuncias->free();
				echo json_encode(array(
					"Success"=>true,
					"Result"=>$Rows,
					"Tiempo"=>(microtime(true)-$time_start)
				));
			}else{
				echo json_encode(array(
					"Success"=>false,
					"errorCode"=>2,
					"Error"=>$this->DB->error
				));
			}
		}else echo json_encode(array("Success"=>false,"errorCode"=>4));
	}
	private function setPreferencias(){
		if($this->Usuario>1){
			if(isset($_REQUEST["Preferencias"]) && is_array($_REQUEST["Preferencias"])){
				if($this->DB->query("DELETE FROM `Preferencias_Usuarios` ".
					"WHERE `id_usuario`=".$this->Usuario
				)){
					foreach($_REQUEST["Preferencias"] as $Preferencia){
						if(!$this->DB->query("INSERT INTO `Preferencias_Usuarios` ".
								"(`id_preferencia_usuario`,`id_usuario`,`id_categoria`,`tiempo`) ".
							"VALUES ".
							"(NULL,".$this->Usuario.",".$Preferencia.",CONVERT_TZ(NOW(),'UTC','America/Santiago'));"
						)){
							echo json_encode(array(
								"Success"=>false,
								"errorCode"=>2,
								"Error"=>$this->DB->error
							));
							exit();
						}
					}
					echo json_encode(array(
						"Success"=>true
					));
				}else{
					echo json_encode(array(
						"Success"=>false,
						"errorCode"=>2,
						"Error"=>$this->DB->error
					));
				}
			}else echo json_encode(array("Success"=>false,"errorCode"=>3));
		}else echo json_encode(array("Success"=>false,"errorCode"=>1));
	}
	private function setCliente(){
		if($this->Administrador===1){
			if(isset($_REQUEST["ID"],$_REQUEST["Cupones"])){
				$ID=$_REQUEST["ID"];
				$Numero=intval($_REQUEST["Cupones"]);
				if($this->DB->query("UPDATE `Clientes` ".
						"SET `cupones`=".$Numero." ".
					"WHERE `id_cliente`=".$ID
				)){
					echo json_encode(array(
						"Success"=>true
					));
				}else{
					echo json_encode(array(
						"Success"=>false,
						"errorCode"=>2,
						"Error"=>$this->DB->error
					));
					exit();
				}
			}else echo json_encode(array("Success"=>false,"errorCode"=>3));
		}else echo json_encode(array("Success"=>false,"errorCode"=>4));
	}
	private function getTimeStamp(){
		echo json_encode(array(
			"Success"=>true,
			"Time"=>date("H:i"),
			"Date"=>date("Y-m-d")
		));
	}
	private function getSession(){
		echo json_encode(array(
			"Success"=>true,
			"Result"=>$_SESSION
		));
	}
	private function getSerie(){
		$characters='abcdefghijklmnopqrstuvwxyz0123456789';
		$Serie='';
		for($i=0;$i<10;$i++) {
			$Serie.=$characters[rand(0,strlen($characters)-1)];
		}
		return $Serie;
	}
}
$Json = new JSON();
