<?php
require('cupomida.php');
require('libs/PHPMailerAutoload.php');

class Alerta extends Cupomida {
	public function JSON() {
		parent::Cupomida();
	}
	public function Buscar(){
		$query="SELECT `serie`,`email`,`first_name`,`last_name`,`valido_hasta`
			FROM `Codigos`
			LEFT JOIN `Usuarios`
				ON `Usuarios`.`id_usuario`=`Codigos`.`id_usuario`
			LEFT JOIN `Cupones`
				ON `Cupones`.`id_cupon`=`Codigos`.`id_cupon`
			WHERE DATE(`Cupones`.`valido_hasta`)=DATE(NOW())
				AND `Usuario`.`Recordatorio`=1";
		if($Cupones=$this->DB->query($query)){
			while($Cupon=mysqli_fetch_assoc($Cupones)){
				if($Cupon["email"]!=""){
					$mail = new PHPMailer();
					$mail->AddAddress(
						$Info["Usuario"]["email"],
						$Info["Usuario"]["first_name"]." ".$Info["Usuario"]["last_name"]
					);
					$mail->Subject = 'Cupón desde Cupomida.';
					$mail->Body = 'Gracias por ser parte de Cupomida, '.
						'le adjuntamos el cupón imprimible en formato PDF, '.
						'puede mostrarlo tal cual en el local en la pantalla de su celular.';
					$mail->AddStringAttachment($doc, 'cupon.pdf', 'base64', 'application/pdf');
					if(!$mail->Send()) error_log($mail->ErrorInfo,0);
				}
			}
			$Cupones->free();
		}
}
$Cupon=new Alerta();
