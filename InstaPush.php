<?php
class InstaPush {
	public function Listar(){
		return $this->curl("apps/list","",true);
	}
	public function NuevaApp($Titulo){
		return $this->curl("apps/add",json_encode(array("title"=>$Titulo)),true);
	}
	public function Eventos(){
		return $this->curl("events/list");
	}
	public function Push($Event,$Trackers){
		return $this->curl("post",
			json_encode(array(
				"event"=>$Event,
				"trackers"=>$Trackers
			))
		);
	}
	public function NuevoEvento($Titulo,$Trackers,$Message){
		return $this->curl("events/add",json_encode(array(
			"title"=>$Titulo,
			"trackers"=>$Trackers,
			"message"=>$Message
		)));
	}
	private function curl($url,$data_string="",$token=false){
		$ch=curl_init("https://api.instapush.im/v1/".$url);
		$headers=array("Content-Type: application/json");
		if($token) array_push($headers,"X-INSTAPUSH-TOKEN: 54864fe8a4c48af556884a6b");
		else{
			array_push($headers,
				"X-INSTAPUSH-APPID: 548cb684a4c48a427a884a6b",
				"X-INSTAPUSH-APPSECRET: 0050437fc13718fb5bd815109e7fe9ff"
			);
		}
		curl_setopt($ch,CURLOPT_CUSTOMREQUEST,"POST");
		curl_setopt($ch,CURLOPT_HEADER,true);
		if($data_string!=""){
			array_push($headers,"Content-Length: ".strlen($data_string));
			curl_setopt($ch,CURLOPT_POSTFIELDS,$data_string);
		}
		curl_setopt($ch,CURLOPT_HTTPHEADER,$headers);
		curl_setopt($ch,CURLOPT_TIMEOUT,20);
		curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
		$resp=curl_exec($ch);
		curl_close($ch);
		return json_decode($resp);
	}
}
