<?php
	require("cupomida.php");
	class Cupon extends Cupomida {
		public function Cupon(){
			parent::Cupomida();
		}
		public function Buscar(){
			if(isset($_REQUEST["id_cupon"])){
				$id_cupon=(double) $_REQUEST["id_cupon"];
				$Query="SELECT ".
					"`C`.`id_cupon`,".
					"`C`.`titulo`,`C`.`descripcion`,`C`.`dias_habiles`,".
					"`C`.`valido_desde`,`C`.`valido_hasta`,`C`.`restantes`,".
					"`C`.`precio`,`C`.`descuento`,`C`.`detalle`,`C`.`imagen`,".
					"`C`.`cantidad`,`C`.`condiciones`,".
					"`CC`.`id_categoria`,`CA`.`nombre` AS `titulo_categoria`,".
					"`CA`.`descripcion` AS `descripcion_categoria`,".
					"`CL`.`id_cliente`,`CL`.`nombre` AS `nombre_cliente`,".
					"`CL`.`email`,`CL`.`telefono`,`CL`.`horario`,".
					"`CL`.`informacion`,`CL`.`imagen` AS `imagen_cliente`,".
					"`U`.`id_ubicacion`,`U`.`direccion`,".
					"`U`.`latitud`,`U`.`longitud`,".
					"`CH`.`id_horario`,`H`.`nombre` AS `titulo_horario`,".
					"`H`.`hora_inicio`,`H`.`hora_fin` ".
					"FROM `Cupones` AS `C` ".
					"INNER JOIN `Cupon_Categoria` AS `CC` ".
						"ON `CC`.`id_cupon`=`C`.`id_cupon` ".
					"LEFT JOIN `Categorias` AS `CA` ".
						"ON `CA`.`id_categoria`=`CC`.`id_categoria` ".
					"LEFT JOIN `Clientes` AS `CL` ".
						"ON `C`.`id_cliente`=`CL`.`id_cliente` ".
					"LEFT JOIN `Ubicacion_Cupon` AS `UC` ".
						"ON `UC`.`id_cupon`=`C`.`id_cupon` ".
					"LEFT JOIN `Ubicaciones` AS `U` ".
						"ON `U`.`id_ubicacion`=`UC`.`id_ubicacion` ".
					"LEFT JOIN `Cupon_Horario` AS `CH` ".
						"ON `CH`.`id_cupon`=`C`.`id_cupon` ".
					"LEFT JOIN `Horarios` AS `H` ".
						"ON `H`.`id_horario`=`CH`.`id_horario` ".
					"WHERE `C`.`id_cupon`=".$id_cupon;
				if($Cupon=$this->DB->query($Query)){
					$row=mysqli_fetch_assoc($Cupon);
					$Cupon->free();
					return $row;
				}else return $this->Vacio();
			}else return $this->Vacio();
		}
		private function Vacio(){
			return array(
				""=>""
			);
		}
	}
	$Cupones=new Cupon();
	$Cupon=$Cupones->Buscar();
?><!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>Cupomida.cl</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width">
		<base href="http://cupomida.cl/" target="_blank">
		<link rel="icon" href="favicon.png" type="image/png" sizes="32x32">

		<meta property="og:title" content="<?php echo $Cupon["titulo"]; ?>" />
		<meta property="og:site_name" content="Cupomida.cl" />
		<meta property="og:description" content="<?php echo $Cupon["descripcion"]; ?>" />
		<meta property="og:type" content="website" />
		<meta property="og:locale" content="es_LA" />
		<meta property="og:url" content="http://cupomida.cl/Cupon/<?php echo $Cupon["id_cupon"]; ?>" />
		<meta property="og:image" content="http://cupomida.cl/decoder.php?i=<?php echo $Cupon["id_cupon"]; ?>" />
		<meta name="description" content="<?php echo $Cupon["descripcion"]; ?>">

        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,600,800' rel='stylesheet' type='text/css'>
		<!--[if lte IE 8]>
		    <link rel="stylesheet" href="css/leaflet.ie.css" />
		<![endif]-->
		<link rel="stylesheet" href="css/main.css">

        <script src="js/modernizr-2.6.2-respond-1.1.0.min.js"></script>
    </head>
    <body>
        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->
        <div class="section section-main">
	    	<div class="container" id="content">
				<div class="row">
					<p>Está siendo redirigido a <a href="http://cupomida.cl/#Cupon/<?php echo $Cupon["id_cupon"]; ?>">Cupomida.</a></p>
				</div>
			</div>
		</div>
        <!-- Javascripts -->
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="js/jquery-1.9.1.min.js"><\/script>')</script>
    </body>
</html>
