<?php
require("cupomida.php");
require("InstaPush.php");

class Mail extends Cupomida {
	private $mail=false;
	public function checkVencimiento(){
		$query="SELECT ".
			"`C`.`id_cupon`,".
			"`C`.`titulo`,".
			"`C`.`descripcion`,".
			"`C`.`valido_hasta`,".
			"`CL`.`nombre`,".
			"`CL`.`email`,".
			"`CL`.`telefono`,".
			"`CL`.`informacion`,".
			"`U`.`id_usuario`,".
			"`U`.`email`,".
			"`U`.`first_name`,".
			"`U`.`last_name`,".
			"`CO`.`id_codigo`,".
			"`CO`.`serie`,".
			"`UB`.`direccion`,".
			"`CC`.`adjunto` ".
		"FROM `Cupones` AS `C` ".
		"LEFT JOIN `Codigos` AS `CO` ".
			"ON `C`.`id_cupon`=`CO`.`id_cupon` ".
		"LEFT JOIN `Usuarios` AS `U` ".
			"ON `U`.`id_usuario`=`CO`.`id_usuario` ".
		"LEFT JOIN `Clientes` AS `CL` ".
			"ON `CL`.`id_cliente`=`C`.`id_cliente` ".
		"LEFT JOIN `Ubicacion_Cupon` AS `UC` ".
			"ON `UC`.`id_cupon`=`C`.`id_cupon` ".
		"LEFT JOIN `Ubicaciones` AS `UB` ".
			"ON `UB`.`id_ubicacion`=`UC`.`id_ubicacion` ".
		"LEFT JOIN `Cola_Correos` AS `CC` ".
			"ON `CC`.`id_cupon`=`C`.`id_cupon` ".
			"AND `CC`.`id_usuario`=`U`.`id_usuario` ".
		"WHERE ".
			"`CO`.`recordado`=0 AND ".
			"`CO`.`id_usuario`>0 AND ".
			"DATE(ADDDATE(CONVERT_TZ(NOW(),'UTC','America/Santiago'),INTERVAL 3 DAY))".
			"= DATE(`C`.`valido_hasta`)";
		if($Cupones=$this->DB->query($query)){
			while($Cupon=mysqli_fetch_assoc($Cupones)){
				if($this->sendMail(array(
					"Name"=>$Cupon["first_name"]." ".$Cupon["last_name"],
					"Mail"=>$Cupon["email"],
					"Subject"=>"Recordatorio desde Cupomida.cl",
					"Body"=>"Queremos recordarle que su cupón '".$Cupon["titulo"]."' ".
							"se encuentra pronto a vencer (".$Cupon["valido_hasta"]."). <br>".
							"Le adjuntamos nuevamente su cupón para imprimirlo o mostrarlo ".
							"directamente en la pantalla de su celular.",
					"Attachment"=>$Cupon["adjunto"]
				))){
					$this->DB->query("UPDATE ".
						"`Codigos` SET `recordado`=1 ".
						"WHERE `id_codigo`=".$Cupon["id_codigo"].
						" LIMIT 1;"
					);
					$InstaPush=new InstaPush();
					$InstaPush->Push("CuponRecordado",array(
						"Usuario"=>$Cupon["first_name"]." ".$Cupon["last_name"],
						"Cupon"=>$Cupon["titulo"],
						"Vencimiento"=>$Cupon["valido_hasta"],
						"Tiempo"=>date("H:i:s d/m/Y")
					));
				}
			}
		}
	}
	public function checkImpresos(){
		$query="SELECT ".
				"`CC`.`id_correo`,".
				"`CC`.`id_cupon`,".
				"`CC`.`asunto`,".
				"`CC`.`mensaje`,".
				"`CC`.`adjunto`,".
				"`C`.`id_usuario`,".
				"`C`.`first_name`,".
				"`C`.`last_name`,".
				"`C`.`email`".
			"FROM `Cupomida`.`Cola_Correos` AS `CC` ".
			"LEFT JOIN `Cupomida`.`Usuarios` AS `C` ".
				"ON `C`.`id_usuario`=`CC`.`id_usuario`".
			"WHERE `CC`.`enviado`=0";
		if($Cupones=$this->DB->query($query)){
			while($Cupon=mysqli_fetch_assoc($Cupones)){
				if($this->sendMail(array(
					"Mail"=>$Cupon["email"],
					"Name"=>$Cupon["first_name"]." ".$Cupon["last_name"],
					"Subject"=>utf8_decode($Cupon["asunto"]),
					"Body"=>$Cupon["mensaje"],
					"Attachment"=>$Cupon["adjunto"]
				))){
					$this->DB->query("UPDATE `Cupomida`.`Cola_Correos` ".
					"SET `enviado`=1 WHERE `id_correo`=".$Cupon["id_correo"]
					);
				}
				sleep(2);
			}
			$Cupones->free();
		}else{
			echo $this->DB->error;
		}
	}
	private function sendMail($data){
		if($this->mail){
			$this->mail->ClearAddresses();
			$this->mail->ClearAttachments();
		}else{
			$this->ConnectSMTP();
		}
		$this->mail->From = 'cupomida.cl@gmail.com';
		$this->mail->FromName = 'Cupomida.cl';

		$this->mail->addAddress($data["Mail"],$data["Name"]);
		//$this->mail->addAddress('andresj551@gmail.com',"Andres Jalinton");

		$this->mail->Subject = $data["Subject"];
		$this->mail->Body    = $data["Body"];
		$this->mail->AltBody = 'Si no puede visualizar el contenido de este correo,'.
			' por favor re intente abrirlo desde un cliente de correo que '.
			' acepte HTML.';
		if(isset($data["Attachment"])){
			if($data["Attachment"]!=''){
				$this->mail->AddStringAttachment(
					base64_decode($data["Attachment"]),
					'cupon.pdf',
					'base64',
					'application/pdf'
				);
			}
		}
		if($this->mail->send()){
			return true;
		} else {
			error_log($this->mail->ErrorInfo,0);
			return false;
		}
	}
	private function ConnectSMTP(){
		require_once 'libs/phpMailer/PHPMailerAutoload.php';
		$this->mail = new PHPMailer;
		$this->mail->isSMTP();
		$this->mail->Host = 'email-smtp.us-east-1.amazonaws.com';
		$this->mail->SMTPAuth = true;
		$this->mail->Username = 'AKIAJG4KLC3AQXOTOV3Q';
		$this->mail->Password = 'AidGKD/szBmrEUKKwIGgWooMdqBi9RfkMAQh9hjyOQSP';
		$this->mail->SMTPSecure = 'tls';
		$this->mail->Port = 587;
	}
}
$mail=new Mail();
$mail->checkImpresos();
$mail->checkVencimiento();
