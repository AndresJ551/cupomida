var Cupomida={
	titulo:"Cupomida.cl",
	root:"http://cupomida.cl/",
	JSONroot:"http://cupomida.cl/JSON.php",
	Imageroot:"http://cupomida.cl/decoder.php?i=",
	Started:false,
	NoHistory:false,
	after:false,
	centro:{
		lat:-33.444338,
		lon:-70.653543
	},
	clients:false,
	categories:false,
	map:false,
	markers:{},
	addresses:[],
	hashes:[""],
	cupon:false,
	cupones:false,
	newCupon:false,
	user:false,
	messagesTimeOut:false,
	postImage:[],
	view:false,
	facebookID:261227094086465,
	regularExpressions:{
		Cupon:new RegExp(
			"_IdCupon|"+
			"_Titulo|"+
			"_Descripcion|"+
			"_Detalle|"+
			"_Categoria|"+
			"_ImagenCupon|"+
			"_Impresos|"+
			"_Termino|"+
			"_Precio|"+
			"_Descuento|"+
			"_btnDenunciar|"+
			"_Hora_Canjeo|"+
			"_Horario|"+
			"_Semana|"+
			"_btnImprimir|"+
			"_FBLikes|"+
			"_FBComments|"+
			"_TWShare|"+
			"_ValoracionesCupon|"+
			"_IdCliente|"+
			"_NombreCliente|"+
			"_NombreIntCliente|"+
			"_Condiciones|"+
			"_Informacion|"+
			"_ImagenCliente|"+
			"_ValoracionesCliente|"+
			"_Telefono|"+
			"_Email|"+
			"_Atencion|"+
			"_Comidas|"+
			"_Comuna|"+
			"_Distancia|"+
			"_Mapa",
			"g"
		),
		Fecha:/\d+-\d+-\d+\s\d+:\d+/,
		Email:/^.+@(\w+|\d+).\w+/,
		infoWindow:new RegExp(
			"_ID|"+
			"_Imagen|"+
			"_Titulo|"+
			"_Descripcion|"+
			"_Detalle|"+
			"_Precio|"+
			"_Evaluacion|"+
			"_Horario|"+
			"_Dias",
			"g"
		)
	},
	sizes:{
		query:6,
		header:{
			width:0,
			height:150
		},
		imageCupon:{
			width:540,
			height:360
		},
		imageCliente:{
			width:220,
			height:220
		},
		imagePerfil:{
			width:220,
			height:220
		},
		imageThumbMap:{
			width:40,
			height:40,
			radius:0
		}
	},
	imageFormat:{
		type:"image/jpeg",
		compression:0.8
	},
	select:{
		body:"body",
		menu:"#mainmenu > ul",
		menuAdmin:"#menuAdmin",
		menuItems:"#mainmenu > ul > li",
		menuLiks:"#mainmenu > ul > li > a",
		menuLogin:"#menu-login",
		menuTime:"#clock",
		modal:"#modal",
		content:"#content",
		categories:"#categories",
		iframeCupon:"#iframeCupon",
		googleLogin:".btn-google-login",
		selectVisitas:"#EstadisticaCupon",
		selectRangoVisitas:"#RangoFechaCupon",
		selectIntervalo:"#intervaloEstadisticas",
		checkboxEliminados:"#eliminadosEstadisticas",
		checkboxVencidos:"#vencidosEstadisticas",
		checkboxVendidos:"#vendidosEstadisticas",
		recomendacion:"#recomendation",
		selectFechaTimeLine:"#fechaTimeLine",
		semanaHabil:"#semanaHabil .btn",
		panelControl:"#panelControl",
		codigosEliminados:"#eliminadosCodigos",
		codigosVencidos:"#vencidosCodigos",
		codigosValidados:"#validadosCodigos"
	},
	time:{
		hour:false,
		minute:false,
		day:false,
		month:false,
		year:false
	},
	Twitter:{
		oauth_token:false,
		oauth_token_secret:false
	},
	Google:{
		ID:"475647180439.apps.googleusercontent.com",
		ApiKey:"AIzaSyCA2cPq85DlzyojGPgzdqW_iviTBKevsAc",
		Analytics:"UA-54778007-1",
		Scopes:"https://www.googleapis.com/auth/plus.me",
		init:function(){
			if(typeof(gapi)=="undefined"){
				$.getScript("https://apis.google.com/js/client:plusone.js",function(){
				}).fail(function(jqXHR, textStatus){
					Cupomida.serverResponse(jqXHR, textStatus);
				});
			}
		},
		signinCallback:function(authResult){
			if (authResult['status']['signed_in']) {
				console.log('Sign-in state: ', authResult);
			} else {
				console.log('Sign-in state: ', authResult['error']);
			}
		}
	},
	init:function(){
		Cupomida.timer();
		Cupomida.setClicks.Cupomida();
		Cupomida.initAnalytics();
		Cupomida.validarSesion(function(){
			Cupomida.router();
		});
	},
	initSite:function(callback){
		if(Cupomida.Started){
			if(typeof(callback)=="function") callback();
		}else{
			Cupomida.Started=true;
			Cupomida.initFacebook();
			$(".coming-soon-content").slideUp(1000,function(){
				$(Cupomida.select.body).prepend(Cupomida.tpl.menu);
				$(Cupomida.select.body).append(Cupomida.tpl.footer);
				Cupomida.resizeFooter();
				if(Cupomida.user){
					Cupomida.actualizarPosicion();
				}
				Cupomida.poblarCategorias();
				Cupomida.escucharFacebook();
				Cupomida.menuLogin();
				if(typeof(callback)=="function") callback();
			});
			$(".background-welcome, .welcome-login").fadeOut(1000);
		}
	},
	initMenu:function(){
		
	},
	initTime:function(callback){
		$.post(Cupomida.JSONroot,{
			Accion:"Tiempo"
		},function(data){
			var Time=data.Time.split(":");
			var Date=data.Date.split("-");
			Cupomida.time.hour=parseInt(Time[0],10);
			Cupomida.time.minute=parseInt(Time[1],10);
			Cupomida.time.day=parseInt(Date[2],10);
			Cupomida.time.month=parseInt(Date[1],10);
			Cupomida.time.year=parseInt(Date[0],10);
			$(Cupomida.select.menuTime).html(data.Time);
			if(typeof(callback)=="function") callback();
		},"json").fail(function(jqXHR,textStatus){
			Cupomida.serverResponse(jqXHR,textStatus);
			if(typeof(callback)=="function") callback();
		});
	},
	initAnalytics:function(){
		(function(i,s,o,g,r,a,m){
			i.GoogleAnalyticsObject=r;
			i[r]=i[r]||function(){
				(i[r].q=i[r].q||[]).push(arguments);
			},
			i[r].l=1*new Date();
			a=s.createElement(o);
			m=s.getElementsByTagName(o)[0];
			a.async=1;
			a.src=g;
			m.parentNode.insertBefore(a,m);
		})(window,document,"script","//www.google-analytics.com/analytics.js","ga");
		ga("create", Cupomida.Google.Analytics, "auto");
	},
	resizeFooter:function(){
		$(".col-footer").css("height","");
		var footerColHeight=Math.max(
				$(".col-footer:eq(0)").height(),
				$(".col-footer:eq(1)").height(),
				$(".col-footer:eq(3)").height(),
				$(".col-footer:eq(4)").height()
			)+"px";
		$(".col-footer").css("height",footerColHeight);
	},
	timer:function(callback){
		Cupomida.initTime(function(){
			setTimeout(function(){
				Cupomida.timer(callback);
				if(typeof(callback)=="function") callback();
			},60000);
		});
	},
	AnalyticsSend:function(page,title){
		ga("send", "pageview", {
			"page": page,
			"title": title
		});
	},
	validarSesion:function(callback){
		if(Cupomida.user){
			if(typeof(callback)=="function") callback(true);
		}else{
			$.post(Cupomida.JSONroot,{
				Accion:"Iniciar"
			},function(data){
				if(data.Success) Cupomida.user=data.Result;
				if(typeof(callback)=="function") callback(data.Success);
			},"json").fail(function(jqXHR, textStatus){
				Cupomida.serverResponse(jqXHR, textStatus);
			});
		}
	},
	router:function(){
		var Hash=window.location.hash;
		switch(Hash){
			case "#Login":
				Cupomida.initSite(function(){
					Cupomida.pageLogin(function(){
						Cupomida.setClicks.Registro();
					});
				});
			break;
			case "#TimeLine":
				Cupomida.initSite(function(){
					Cupomida.pageTimeLine();
				});
			break;
			case "#Registro":
				Cupomida.initSite(function(){
					Cupomida.pageRegistro(function(){
						Cupomida.setClicks.Registro();
					});
				});
			break;
			case "#TimeLine":
				Cupomida.initSite(function(){
					Cupomida.pageTimeLine();
				});
			break;
			case "#Mapa":
				Cupomida.initSite(function(){
					Cupomida.pageMapa();
				});
			break;
			case "#Perfil":
				if(Cupomida.user){
					Cupomida.initSite(function(){
						Cupomida.pagePerfil(function(){
							Cupomida.setClicks.Perfil();
						});
					});
				}else{
					Cupomida.setClicks.Welcome();
					Cupomida.preguntarLogin(function(callback){
						Cupomida.initSite(function(){
							if(typeof(callback)=="function") callback();
						});
					});
				}
			break;
			case "#Cupon/Nuevo":
				if(Cupomida.user){
					Cupomida.initSite(function(){
						Cupomida.pageNuevoCupon(function(){
						});
					});
				}else{
					Cupomida.setClicks.Welcome();
					Cupomida.preguntarLogin(function(callback){
						Cupomida.initSite(function(){
							if(typeof(callback)=="function") callback();
						});
					});
				}
			break;
			case "#Estadísticas":
				if(Cupomida.user){
					Cupomida.initSite(function(){
						Cupomida.pageEstadisticas();
					});
				}else{
					Cupomida.setClicks.Welcome();
					Cupomida.preguntarLogin(function(callback){
						Cupomida.initSite(function(){
							if(typeof(callback)=="function") callback();
						});
					});
				}
			break;
			case "#Panel_Control":
				if(Cupomida.user){
					if(Cupomida.user.administrador>0){
						Cupomida.initSite(function(){
							Cupomida.pagePanelControl();
						});
					}else{
						Cupomida.mensajeError(
							"Error",
							"No tienes permisos para entrar al panel de control."
						);
					}
				}else{
					Cupomida.setClicks.Welcome();
					Cupomida.preguntarLogin(function(callback){
						Cupomida.initSite(function(){
							if(typeof(callback)=="function") callback();
						});
					});
				}
			break;
			case "#Códigos":
				if(Cupomida.user){
					if(Cupomida.user.administrador>0){
						Cupomida.initSite(function(){
							Cupomida.pageCodigos({},function(){
							});
						});
					}else{
						Cupomida.mensajeError(
							"Error",
							"No tienes permisos para entrar al los códigos."
						);
					}
				}else{
					Cupomida.setClicks.Welcome();
					Cupomida.preguntarLogin(function(callback){
						Cupomida.initSite(function(){
							if(typeof(callback)=="function") callback();
						});
					});
				}
			break;
			default:
				if(/#Cupon\/d*/.test(Hash)){
					Cupomida.initSite(function(){
						Cupomida.pageCupon(Hash.replace("#Cupon/",""),function(){
							Cupomida.setClicks.Cupon();
						});
					});
				}else if(/#Cliente\/d*/.test(Hash)){
					Cupomida.initSite(function(){
						Cupomida.pageCliente(Hash.replace("#Cliente/",""),function(){
							Cupomida.setClicks.Galeria();
							Cupomida.primeraPagina();
						});
					});
				}else if(/#Categoria\/d*/.test(Hash)){
					Cupomida.initSite(function(){
						Cupomida.pageCategoria(Hash.replace("#Categoria/",""),function(){
							Cupomida.setClicks.Galeria();
							Cupomida.primeraPagina();
						});
					});
				}else if(/#Ubicación\/d*/.test(Hash)){
					Cupomida.initSite(function(){
						Cupomida.pageUbicacion(Hash.replace("#Ubicación/",""),function(){
							Cupomida.setClicks.Galeria();
							Cupomida.primeraPagina();
						});
					});
				}else if(/#Validar\/.*/.test(Hash)){
					Cupomida.initSite(function(){
						Cupomida.pageValidar(Hash.replace("#Validar/",""),function(){
							Cupomida.setClicks.Perfil();
						});
					});
				}else if(/#Buscar\/.*/.test(Hash)){
					Cupomida.initSite(function(){
						Cupomida.pageBuscar(Hash.replace("#Buscar/",""),function(){
						});
					});
				}else{
					if(Cupomida.user){
						Cupomida.initSite(function(){
							Cupomida.pageInicio(function(){
								Cupomida.setClicks.Galeria();
								Cupomida.primeraPagina();
							});
						});
					}else Cupomida.setClicks.Welcome();
				}
			break;
		}
	},
	initFacebook:function(callback){
		$.getScript('//connect.facebook.net/es_LA/sdk.js',function(){
			FB.init({
				appId:Cupomida.facebookID,
				version:'v2.0',
				status:true,
				xfbml:true
			});
			if(typeof(callback)=="function") callback();
			Cupomida.readyFacebook();
		}).fail(function(jqXHR, textStatus){
			Cupomida.serverResponse(jqXHR, textStatus);
		});
	},
	readyFacebook:function(callback){
		if(typeof(window.fbAsyncInit)=="function"){
			FB.getLoginStatus(function(response){
				if(response.status=="connected"){
					if(typeof(callback)=="function") callback();
				}
			});
		}else{
			window.fbAsyncInit=function(){
				FB.getLoginStatus(function(response){
					if(response.status=="connected"){
						if(typeof(callback)=="function") callback();
					}
				});
			};
		}
	},
	loginFacebook:function(callback){
		FB.login(function(response){
			if(response.authResponse) if(typeof(callback)=="function") callback();
			else Cupomida.mensajeError("Cancelado","Debe aceptar los permisos de Facebook.");
		},{'scope':'email,xmpp_login'});
	},
	registrarFacebook:function(callback){
		FB.api('/me',function(response) {
			response.Accion="Iniciar";
			$.post(Cupomida.JSONroot,response,function(data){
				if(data.Success){
					Cupomida.user=data.Result;
					if(typeof(callback)=="function") callback();
				}else{
					Cupomida.errorCode(data.errorCode,function(){
						Cupomida.mensajeError("Facebook Error","Ocurrió un error al conectar con Facebook.");
					});
				}
			},"json").fail(function(jqXHR, textStatus){
				Cupomida.serverResponse(jqXHR, textStatus);
			});
		});
	},
	permisosFacebook:function(){
		FB.api('/me/permissions',function(response){
			console.log(response);
		});
		/*
		FB.login(function(response) {
			console.log(response);
		},{
			scope:"publish_actions,user_likes,read_stream",
			auth_type:"rerequest"
		});
		*/
	},
	publicarFacebook:function(data,callback){
		/*
		FB.api('/me/feed','post',data,function(response){
			if(!response || response.error){
				console.log(response);
				Cupomida.mensajeError("No publicado","Ocurrió un error al crear la publicación en Facebook");
			}else{
				Cupomida.mensajeInfo("Publicado","Se publicó el cupón en su Facebook");
				if(typeof(callback)=="function") callback(response);
			}
		});
		*/
		data.method="feed";
		FB.ui(data,function(response) {
			if (response && response.post_id) {
				Cupomida.mensajeInfo("Publicado","Se publicó el cupón en su Facebook");
			}else{
				Cupomida.mensajeError("No publicado","Ocurrió un error al crear la publicación en Facebook");
			}
			if(typeof(callback)=="function") callback(response);
		});
	},
	escucharFacebook:function(){
		Cupomida.readyFacebook(function(){
			FB.Event.subscribe('edge.create', function(url,html_element) {
				var ID=url.replace("http://cupomida.cl/#Cupon/","");
				$.post(Cupomida.JSONroot,{Accion:"Nuevo Like",ID:ID},function(data){
					console.log(data);
				});
			});
			FB.Event.subscribe('edge.remove', function(url,html_element) {
				var ID=url.replace("http://cupomida.cl/#Cupon/","");
				$.post(Cupomida.JSONroot,{Accion:"Eliminar Like",ID:ID},function(data){
					console.log(data);
				});
			});
		});
	},
	initTwitter:function(callback){
		$.getScript("js/codebird.js",function(){
			if(typeof(callback)=="function") callback();
		}).fail(function(jqXHR, textStatus){
			Cupomida.serverResponse(jqXHR, textStatus);
		});
	},
	loginTwitter:function(callback){
		Cupomida.initTwitter(function(){
			var cb = new Codebird();
			cb.setConsumerKey(
				"WeOprRe7GBGgWFPtHYyVvYY29",
				"tHE1HucUYrVXt51vCEcm1lTCjYiqqOvC4xIYSKQKmCO2tHhlkJ"
			);
			cb.__call("oauth_requestToken",{
				oauth_callback: "oob"
			},function(reply){
				cb.setToken(reply.oauth_token, reply.oauth_token_secret);
				cb.__call("oauth_authorize",{},function(auth_url){
					window.open(auth_url);
					$(Cupomida.select.body).prepend(
						'<div class="popup-blocked blinking"></div>',
						'<div class="glyphicon glyphicon-arrow-up arrow seis floating"></div>'
					);
					Cupomida.modal.show({
						title:"Pin",
						body:'<p class="lead">Escriba el pin que aparece en el popup, '+
							'si no lo encuentra probablemente su navegador esté bloqueando '+
							'su inicio.</p>'+
							'<input type="text" id="pinTwitter" '+
								'class="form-control" '+
								'placeholder="Pin de acceso.">',
						buttons:[
							{
								id:"twitterPin",
								text:"Aceptar",
								class:"btn-blue"
							},{
								id:"twitterCan",
								text:"Cancelar",
								class:"btn-red"
							}
						]
					},function(id){
						switch(id){
							case "ready":break;
							case "twitterPin":
								cb.__call("oauth_accessToken",{
									oauth_verifier: $("#pinTwitter").val()
								},function(reply){
									if(reply.httpstatus==200 && reply.user_id){
										cb.setToken(reply.oauth_token,reply.oauth_token_secret);
										Cupomida.Twitter.oauth_token=reply.oauth_token;
										Cupomida.Twitter.oauth_token_secret=reply.oauth_token_secret;
										cb.__call("account_verifyCredentials",{
										},function(user){
											if(user.httpstatus==200){
												Cupomida.registrarTwitter(user,function(){
													if(typeof(callback)=="function") callback();
												});
											}else{
												Cupomida.mensajeError(
													"Error",
													"Ocurrió un error al iniciar la sesión con Twitter."
												);
											}
										});
									}else{
										Cupomida.mensajeError(
											"Error",
											"Ocurrió un error al iniciar la sesión con Twitter."
										);
									}
								});
							default:
								Cupomida.modal.hide();
								$(".popup-blocked, .arrow.seis").remove();
							break;
						}
					});
				});
			});
		});
	},
	registrarTwitter:function(response,callback){
		$.post(Cupomida.JSONroot,{
			Accion:"Iniciar",
			id:response.id,
			first_name:response.name,
			last_name:"",
			name:response.screen_name,
			email:"",
			timezone:response.time_zone,
			verified:response.verified
		},function(data){
			if(data.Success){
				Cupomida.user=data.Result;
				if(typeof(callback)=="function") callback();
			}else{
				Cupomida.errorCode(data.errorCode,function(){
					Cupomida.mensajeError("Twitter Error","Ocurrió un error al conectar con Twitter.");
				});
			}
		},"json").fail(function(jqXHR, textStatus){
			Cupomida.serverResponse(jqXHR, textStatus);
		});
	},
	loginNativo:function(email,contra,callback){
		Cupomida.whirlpool(function(){
			if(email!=='' && contra!==''){
				if(Cupomida.regularExpressions.Email.test(email)){
					$.post(Cupomida.JSONroot,{
						Accion:"Iniciar",
						email:email,
						contra:Whirlpool(contra)
					},function(data){
						if(data.Success){
							if(data.Result.verified=="false"){
								Cupomida.mensajeInfo(
									"Valide su usuario:",
									"Aún no ha validado su usuario, "+
									"por favor ingrese a su correo "+
									"y acceda mediante el enlace."
								);
							}else{
								Cupomida.user=data.Result;
								if(typeof(callback)=="function") callback();
							}
						}else{
							Cupomida.errorCode(data.errorCode,function(){
								Cupomida.mensajeError(
									"Login Error",
									"Ocurrió un error al ingresar.<br>"+
									"Compruebe que su contraseña sea correcta."
								);
							});
						}
					},"json").fail(function(jqXHR, textStatus){
						Cupomida.serverResponse(jqXHR, textStatus);
					});
				}else{
					Cupomida.mensajeError("Error","El correo ingresado no es válido.");
				}
			}else{
				Cupomida.mensajeError("Error","Debe ingresar su correo y contraseña.");
			}
		});
	},
	registrarUsuario:function(callback){
		var Correo=$("#formCorreo").val().trim(),
			Contra=$("#formContra").val().trim(),
			ContraVal=$("#formContraConf").val().trim();
		if(!Cupomida.regularExpressions.Email.test(Correo)){
			Cupomida.mensajeError("Error","Debe ingresar un correo válido");
			return false;
		}
		if(Contra.length>6){
			if(Contra==ContraVal){
				Cupomida.whirlpool(function(){
					$.post(Cupomida.JSONroot,{
						Accion:"Iniciar",
						email:Correo,
						contra:Whirlpool(Contra)
					},function(data){
						if(data.Success){
							if(typeof(callback)=="function") callback(data);
						}else{
							Cupomida.errorCode(data.errorCode,function(){
								Cupomida.mensajeError(
									"Error",
									"Ocurrió un error al registrar su usuario."
								);
							});
						}
					},"json").fail(function(jqXHR, textStatus){
						Cupomida.serverResponse(jqXHR, textStatus);
					});
				});
			}else{
				Cupomida.mensajeError(
					"Error",
					"Las contraseñas deben coincidir."
				);
				return false;
			}
		}else{
			Cupomida.mensajeError(
				"Error",
				"Debe ingresar una contraseña de al menos 6 caracteres."
			);
			return false;
		}
	},
	setClicks:{
		Cupomida:function(){
			$(window).unbind("popstate").bind("popstate",function(e){
				if(e.originalEvent.state !== null){
					if(e.originalEvent.state.number < history.length-1){
						//Atrás
					}else{
						//Adelante
					}
				}
				Cupomida.NoHistory=true;
				Cupomida.router();
			});
		},
		Welcome:function(){
			//$(".video-wrapper").fitVids();
			$('.show-tooltip').tooltip();
			$(".welcome-logo").off("click").on("click",function(){
				Cupomida.initSite(function(){
					Cupomida.pageInicio(function(){
						Cupomida.setClicks.Galeria();
					});
				});
			});
			$(".welcome-login").off("click").on("click",function(){
				Cupomida.initSite(function(){
					Cupomida.pageLogin(function(){
						Cupomida.setClicks.Registro();
					});
				});
			});
			$(".welcome-search").off("click").on("click",function(){
				var term=$("#txtWelcomeBuscar").val().trim();
				if(term!==""){
					Cupomida.initSite(function(){
						Cupomida.pageBuscar(term,function(){
							Cupomida.setClicks.Galeria();
						});
					});
				}else{
					Cupomida.mensajeError("Error","Debe escribir al menos una palabra.");
				}
			});
			$("#txtWelcomeBuscar").off("keyup").on("keyup",function(e){
				if(e.keyCode===13){
					e.preventDefault();
					var term=$("#txtWelcomeBuscar").val().trim();
					if(term!==""){
						Cupomida.initSite(function(){
							Cupomida.pageBuscar(term,function(){
								Cupomida.setClicks.Galeria();
							});
						});
					}else{
						Cupomida.mensajeError("Error","Debe escribir al menos una palabra.");
					}
					return false;
				}
			});
		},
		Menu:function(){
			var menuItems = $(Cupomida.select.menuItems),
				current = -1;
			$(Cupomida.select.menuLiks).off("click").on("click",function(e){
				var $item=$(e.currentTarget).parent("li.has-submenu"),
					idx=$item.index();
				if($item.length !== 0){
					if(current !== -1){
						menuItems.eq(current).removeClass("mainmenu-open");
					}
					if(current === idx){
						$item.removeClass("mainmenu-open");
						current = -1;
					}else{
						$item.addClass("mainmenu-open");
						current = idx;
						$(Cupomida.select.body).off("click").on("click",function(e){
							menuItems.eq(current).removeClass("mainmenu-open");
							current = -1;
						});
					}
					return false;
				}
			});
			$(".page-inicio").off("click").on("click",function(){
				Cupomida.pageInicio(function(){
					Cupomida.setClicks.Galeria();
				});
			});
			$(".page-cerrar").off("click").on("click",function(){
				Cupomida.reset.todo(function(){
					Cupomida.menuLogin();
					Cupomida.pageInicio();
				});
			});
			$(".page-login").off("click").on("click",function(){
				Cupomida.pageLogin(function(){
					Cupomida.setClicks.Registro();
					if(Cupomida.pageTutorial.iniciado) Cupomida.pageTutorial.dos();
				});
			});
			$(".page-perfil").off("click").on("click",function(e){
				Cupomida.pagePerfil(function(){
					Cupomida.setClicks.Perfil();
				});
			});
			$(".page-timeline").off("click").on("click",function(){
				Cupomida.pageTimeLine(function(){
					Cupomida.setClicks.TimeLine();
				});
			});
			$(".page-mapa").off("click").on("click",function(){
				Cupomida.pageMapa();
			});
			$(".page-nuevo").off("click").on("click",function(){
				Cupomida.pageNuevoCupon();
			});
			$(".page-estadisticas").off("click").on("click",function(){
				Cupomida.pageEstadisticas();
			});
			$(".page-panel").off("click").on("click",function(){
				Cupomida.pagePanelControl();
			});
			$(".ver-categoria").off("click").on("click",function(e){
				Cupomida.pageCategoria($(e.currentTarget).data("id"),function(){
					Cupomida.setClicks.Galeria();
					Cupomida.primeraPagina();
				});
			});
			$(".page-codigos").off("click").on("click",function(){
				Cupomida.pageCodigos({});
			});
			$(".btn-buscar").off("click").on("click",function(e){
				Cupomida.pageBuscar($("#txt-buscar").val());
				e.preventDefault();
				return false;
			});
		},
		Cupon:function(){
			$(".rating-star-cupon").off("mouseover").on("mouseover",function(e){
				var este_index=$(e.currentTarget).index();
				$(".rating-star-cupon").each(function(i,e){
					if(este_index>=i){
						$(e).addClass("glyphicon-star");
						$(e).removeClass("glyphicon-star-empty");
					}else{
						$(e).removeClass("glyphicon-star");
						$(e).addClass("glyphicon-star-empty");
					}
				});
			}).off("mouseleave").on("mouseleave",function(){
				Cupomida.reset.stars();
			}).off("click").on("click",function(e){
				if(Cupomida.user){
					Cupomida.nuevoRating({
						Accion:"Nuevo Rating Cupon",
						ID:Cupomida.cupon.id_cupon,
						Valoracion:$(e.currentTarget).index()
					},function(Estado){
						Cupomida.mensajeInfo(
							"Guardado",//"+Estado+"
							"Gracias por su tiempo, "+
							"su evaluación ha sido guardada "+
							"para otras personas que utilizan el servicio."
						);
					});
				}else Cupomida.preguntarLogin();
			});
			$(".rating-star-cliente").off("mouseover").on("mouseover",function(e){
				var este_index=$(e.currentTarget).index();
				$(".rating-star-cliente").each(function(i,e){
					if(este_index>=i){
						$(e).addClass("glyphicon-star");
						$(e).removeClass("glyphicon-star-empty");
					}else{
						$(e).removeClass("glyphicon-star");
						$(e).addClass("glyphicon-star-empty");
					}
				});
			}).off("mouseleave").on("mouseleave",function(){
				Cupomida.reset.stars();
			}).off("click").on("click",function(e){
				if(Cupomida.user){
					Cupomida.nuevoRating({
						Accion:"Nuevo Rating Cliente",
						ID:Cupomida.cupon.id_cliente,
						Valoracion:$(e.currentTarget).index()
					},function(Estado){
						Cupomida.mensajeInfo(
							"Guardado",//"+Estado+"
							"Gracias por su tiempo, "+
							"su evaluación ha sido guardada "+
							"para otras personas que utilizan el servicio."
						);
					});
				}else Cupomida.preguntarLogin();
			});
			$(".shop-item-selections .btnImprimir").off("click").on("click",function(){
				if(Cupomida.user){
					var content=Cupomida.tpl.iframeCupon.replace("_ID",Cupomida.cupon.id_cupon);
					Cupomida.modal.show({
						title:"Cupón",
						body:content,
						buttons:[
							{
								id:"btnImprimirModal",
								text:"Imprimir",
								class:"btn-blue"
							},{
								id:"btnCerrarModal",
								text:"Cerrar",
								class:"btn-red"
							}
						]
					},function(id){
						switch(id){
							case "ready":
								Cupomida.publicarFacebook({
									/*
									message:Cupomida.cupon.titulo,
									link:{
										picture:Cupomida.Imageroot+
											Cupomida.cupon.id_cupon,
										name:Cupomida.cupon.titulo,
										caption:Cupomida.wordWrap(Cupomida.cupon.detalle,10),
										description:Cupomida.cupon.descripcion
									},
									tags:Cupomida.facebookID
									*/
									name:Cupomida.cupon.titulo,
									link:Cupomida.root+"#Cupon/"+Cupomida.cupon.id_cupon,
									picture:Cupomida.Imageroot+Cupomida.cupon.id_cupon,
									caption:Cupomida.wordWrap(Cupomida.cupon.detalle,10),
									description:Cupomida.cupon.detalle,
									message:Cupomida.cupon.titulo
								},function(response){
									console.log('Post: ',response);
								});
							break;
							case "btnImprimirModal":
								var PDF=$(Cupomida.select.iframeCupon)[0];
								PDF.focus();
								PDF.contentWindow.print();
							break;
							default:Cupomida.modal.hide();
						}
					});
				}else Cupomida.preguntarLogin();
			});
			$(".denuncia").off("click").on("click",function(){
				if(Cupomida.user){
					Cupomida.modal.show({
						title:"Denuncia",
						body:'<p class="lead">Gracias por tomarse el tiempo de denunciar, '+
							'revisaremos el caso tan pronto como podamos.</p>',
						buttons:[
							{
								id:"btnAbusoModal",
								text:"Cláusula abusiva",
								class:"btn-warning"
							},{
								id:"btnInapropiadoModal",
								text:"Contenido inapropiado",
								class:"btn-warning"
							},{
								id:"btnCancelarModal",
								text:"Cancelar",
								class:"btn-red"
							}
						]
					},function(id){
						switch(id){
							case "ready":
							break;
							case "btnAbusoModal":
								Cupomida.guardarDenuncia("Abuso");
								Cupomida.modal.hide();
							break;
							case "btnInapropiadoModal":
								Cupomida.guardarDenuncia("Inapropiado");
								Cupomida.modal.hide();
							break;
							default:
								Cupomida.modal.hide();
							break;
						}
					});
				}else Cupomida.preguntarLogin();
			});
			Cupomida.setClicks.ClienteLink.ver();
		},
		Registro:function(){
			$(".btn-facebook-login").off("click").on("click",function(){
				Cupomida.loginFacebook(function(){
					Cupomida.registrarFacebook(function(){
						Cupomida.menuLogin();
						if(Cupomida.after) Cupomida.pageAfter();
						else{
							Cupomida.pageInicio(function(){
								Cupomida.setClicks.Galeria();
							});
						}
					});
				});
			});
			$(".btn-twitter-login").off("click").on("click",function(){
				Cupomida.loginTwitter(function(){
					Cupomida.menuLogin();
					if(Cupomida.after) Cupomida.pageAfter();
					else{
						Cupomida.pageInicio(function(){
							Cupomida.setClicks.Galeria();
						});
					}
				});
			});
			$("#formRegistrar").off("click").on("click",function(e){
				e.preventDefault();
				Cupomida.registrarUsuario(function(data){
					if(data.Result.verified=="false"){
						Cupomida.modal.show({
							title:"Listo",
							body:'<p class="lead">Su cuenta ha sido creada, '+
								'para iniciar sesión y utilizar el sitio '+
								'revise su correo y valide con el link que contiene.</p>',
							buttons:[
								{
									id:"registroOK",
									text:"Aceptar",
									class:"btn-blue"
								}
							]
						},function(id){
							switch(id){
								case "registroOK":
									Cupomida.modal.hide();
									Cupomida.pageInicio();
								break;
							}
						});
					}else{
						Cupomida.user=data.Result;
						Cupomida.menuLogin();
						if(Cupomida.after) Cupomida.pageAfter();
						else{
							Cupomida.pageInicio(function(){
								Cupomida.setClicks.Galeria();
							});
						}
					}
				});
				return false;
			});
			$(".page-register").off("click").on("click",function(){
				Cupomida.pageRegistro(function(){
					Cupomida.setClicks.Registro();
				});
			});
			$("#login-entrar").off("click").on("click",function(e){
				e.preventDefault();
				Cupomida.loginNativo(
					$("#login-username").val().trim(),
					$("#login-password").val().trim()
				,function(){
					Cupomida.menuLogin();
					if(Cupomida.after) Cupomida.pageAfter();
					else{
						Cupomida.pageInicio(function(){
							Cupomida.setClicks.Galeria();
						});
					}
				});
				return false;
			});
			$("#login-username").off("keyup").on("keyup",function(e){
				if(e.keyCode===13){
					e.preventDefault();
					Cupomida.loginNativo(
						$(e.currentTarget).val().trim(),
						$("#login-password").val().trim()
					,function(){
						Cupomida.menuLogin();
						if(Cupomida.after) Cupomida.pageAfter();
						else{
							Cupomida.pageInicio(function(){
								Cupomida.setClicks.Galeria();
							});
						}
					});
					return false;
				}
			});
			$("#login-password").off("keyup").on("keyup",function(e){
				if(e.keyCode===13){
					e.preventDefault();
					Cupomida.loginNativo(
						$("#login-username").val().trim(),
						$(e.currentTarget).val().trim()
					,function(){
						Cupomida.menuLogin();
						if(Cupomida.after) Cupomida.pageAfter();
						else{
							Cupomida.pageInicio(function(){
								Cupomida.setClicks.Galeria();
							});
						}
					});
					return false;
				}
			});
		},
		Perfil:function(){
			$("#nuevaFileImagenPerfil").off("change").on("change",function(e){
				Cupomida.fileToBase64(e,
					$("#nuevaImagenPerfil")[0],
					Cupomida.sizes.imagePerfil.width,
					Cupomida.sizes.imagePerfil.height,
				function(data){
					$.post(Cupomida.JSONroot,{
						Accion:"Set Imagen Usuario",
						Imagen:data
					},function(data){
						if(data.Success){
							Cupomida.mensajeInfo(
								"Guardado",
								"La nueva imagen fue guardada correctamente."
							);
						}else{
							Cupomida.errorCode(data.errorCode,function(){
								Cupomida.mensajeError(
									"Error",
									"Ocurrió un error al guardar su imagen."
								);
							});
						}
					},"json").fail(function(jqXHR, textStatus){
						Cupomida.serverResponse(jqXHR, textStatus);
					});
				});
			});
			$("#btnDatosPerfil").off("click").on("click",function(){
				Cupomida.actualizarDatosUsuario();
			});
			$("#btnContraPerfil").off("click").on("click",function(){
				Cupomida.actualizarContraUsuario();
			});
			$(".btnCancelarPerfil").off("click").on("click",function(){
				Cupomida.pageInicio(function(){
					Cupomida.setClicks.Galeria();
				});
			});
			$("input[name=preferencias]").off("change").on("change",function(){
				var Preferencias=[];
				$("input[name=preferencias]:checked").each(function(i,e){
					Preferencias.push($(e).val());
				});
				Cupomida.actualizarPreferencias(Preferencias,function(Success){
					if(Success){
						$("input[name=preferencias]:checked").addClass("box-success").delay(500).queue(function(){
							$(this).removeClass("box-success");
							$(this).dequeue();
						});
					}else{
						$("input[name=preferencias]:checked").addClass("box-error").delay(500).queue(function(){
							$(this).removeClass("box-error");
							$(this).dequeue();
						});
					}
				});
			});
		},
		Galeria:function(){
			Cupomida.setClicks.CuponLink.ver();
			Cupomida.setClicks.CuponLink.editar();
			Cupomida.setClicks.CuponLink.borrar(function(Success,id_cupon){
				if(Success) $("#Cupon-"+id_cupon).remove();
			});
		},
		NuevoCupon:function(map){
			$(window).unbind("beforeunload").bind("beforeunload",function(e){
				return "Está a punto de dejar esta página, los cambios se perderán. ¿Está seguro?";
			});
			$("#nuevaFileImagenCupon").on("change",function(e){
				Cupomida.fileToBase64(e,
					$("#nuevaImagenCupon")[0],
					Cupomida.sizes.imageCupon.width,
					Cupomida.sizes.imageCupon.height,
				function(data){
					Cupomida.newCupon.image=data;
					Cupomida.setMarkIcon(Cupomida.markers.center,data);
				});
			});
			$("#nuevaFileImagenCliente").on("change",function(e){
				Cupomida.fileToBase64(e,
					$("#nuevaImagenCliente")[0],
					Cupomida.sizes.imageCliente.width,
					Cupomida.sizes.imageCliente.height,
				function(data){
					Cupomida.newCupon.imageCliente=data;
				});
			});
			$(".shop-item-selections .btnGuardar").off("click").on("click",function(){
				Cupomida.guardarCupon(function(){
					Cupomida.pageInicio(function(){
						Cupomida.setClicks.Galeria();
						Cupomida.mensajeInfo("Nuevo Cupon","Cupón guardado correctamente.");
					});
				});
			});
			$("#btnNuevaCondicion").off("click").on("click",function(){
				$("#tab1 ul").prepend(
					Cupomida.tpl.cuponCondiciones.replace(
						"_Condicion",
						Cupomida.tpl.cuponNuevaCondicion.replace("_Condicion","")
					)
				);
			});
			$(Cupomida.select.semanaHabil).off("click").on("click",function(e){
				$(e.currentTarget).toggleClass("btn-red");
			});
			Cupomida.cargarNuevoExtensiones(function(){
				var ValueDate=Cupomida.time.year+"-"+Cupomida.time.month+"-"+Cupomida.time.day,
					ValueTime=Cupomida.time.hour+":"+Cupomida.time.minute,
					ValueDateTime=ValueDate+" "+ValueTime,
					format={
					lang:"es",
					i18n:{
						es:{
							months:[
								"Enero","Febrero","Marzo","Abril",
								"Mayo","Junio","Julio","Agosto",
								"Septiembre","Octubre","Noviembre",
								"Diciembre"
							],
							dayOfWeek:[
								"Dom", "Lun", "Mar", "Mie",
								"Jue", "Vie", "Sab"
							]
						}
					},
					defaultTime:ValueTime,
					defaultDate:ValueDate,
					dayOfWeekStart:1,
					step:30,
					value:ValueDateTime,
					format:"Y-m-d H:i",
					lazyInit:true
				};
				$("#nuevoInicio").datetimepicker(format);
				$("#nuevoTermino").datetimepicker(format);
				format.datepicker=false;
				format.format="H:i";
				format.defaultDate=false;
				$("#nuevaHoraInicio").datetimepicker(format);
				$("#nuevaHoraFin").datetimepicker(format);
				$.post(Cupomida.JSONroot,{Accion:"Clientes"},function(data){
					if(data.Success){
						Cupomida.clients=data.Result;
						var Clientes=[];
						for(var d in data.Result){
							Clientes.push({
								label:data.Result[d].nombre,
								value:data.Result[d].id_cliente
							});
						}
						$("#nuevoNombreCliente").autocomplete({
							source:Clientes,
							autoFocus: true,
							focus:function(e,ui){
								$(this).val(ui.item.label);
								return false;
							},
							select:function(e,ui){
								for(var c in Cupomida.clients){
									var C=Cupomida.clients[c];
									if(ui.item.value==C.id_cliente){
										$("#nuevoIdCliente").val(C.id_cliente);
										Cupomida.markers.center.setTitle(C.nombre);
										$("#nuevoNombreIntCliente").html(C.nombre);
										$("#nuevoTelefono").val(C.telefono);
										$("#nuevoCorreo").val(C.email);
										$("#nuevaAtencion").val(C.horario);
										Cupomida.idImagenACanvas(
											"Imagen Cliente",
											C.id_cliente,
											$("#nuevaImagenCliente")[0]
										);
									}
								}
								$(this).val(ui.item.label);
								return false;
							}
						});
					}else Cupomida.mensajeError("Error","No se pudo cargar la lista de clientes.");
				},"json").fail(function(jqXHR, textStatus){
					Cupomida.serverResponse(jqXHR, textStatus);
				});
				Cupomida.obtenerCategorias(function(){
					var Categorias=[];
					for(var c in Cupomida.categories){
						Categorias.push({
							label:Cupomida.categories[c].nombre,
							value:Cupomida.categories[c].id_categoria
						});
					}
					$("#nuevaCategoria").autocomplete({
						source:Categorias,
						autoFocus: true,
						focus:function(e,ui){
							$(this).val(ui.item.label);
							return false;
						},
						select:function(e,ui){
							for(var c in Cupomida.categories){
								var C=Cupomida.categories[c];
								if(ui.item.value==C.id_categoria){
									$("#nuevoIdCategoria").val(C.id_categoria);
									$("#nuevaDescripcionCategoria").val(C.descripcion);
								}
							}
							$(this).val(ui.item.label);
							return false;
						}
					});
				});
				$.post(Cupomida.JSONroot,{Accion:"Horarios"},function(data){
					if(data.Success){
						Cupomida.Schedules=data.Result;
						var Horarios=[];
						for(var r in data.Result){
							Horarios.push({
								label:data.Result[r].nombre,
								value:data.Result[r].id_horario
							});
						}
						$("#nuevoHorario").autocomplete({
							source:Horarios,
							autoFocus: true,
							focus:function(e,ui){
								$(this).val(ui.item.label);
								return false;
							},
							select:function(e,ui){
								for(var s in Cupomida.Schedules){
									var C=Cupomida.Schedules[s];
									if(ui.item.value==C.id_horario){
										$("#nuevoIdHorario").val(C.id_horario);
										$("#nuevaHoraInicio").val(C.hora_inicio);
										$("#nuevaHoraFin").val(C.hora_fin);
									}
								}
								$(this).val(ui.item.label);
								return false;
							}
						});
					}else Cupomida.mensajeError("Error","No se pudo cargar la lista de horarios.");
				},"json").fail(function(jqXHR, textStatus){
					Cupomida.serverResponse(jqXHR, textStatus);
				});
			});
			google.maps.event.addListener(map,"click",function(e){
				Cupomida.markers.center.setPosition(e.latLng);
				$("#nuevaLatitud").val(e.latLng.k);
				$("#nuevaLongitud").val(e.latLng.B);
				$("#nuevaIdUbicacion").val(0);
				Cupomida.obtenerNombrePosicion(e.latLng.k,e.latLng.B,function(){
					$("#nuevaComuna").autocomplete({
						source:Cupomida.addresses,
						autoFocus: true
					});
				});
			});
		},
		Estadisticas:{
			Pagina:function(){
				$(Cupomida.select.checkboxEliminados).off("change").on("change",function(e){
					Cupomida.poblarEstadisticas.Actualizar();
				});
				$(Cupomida.select.checkboxVencidos).off("change").on("change",function(e){
					Cupomida.poblarEstadisticas.Actualizar();
				});
				$(Cupomida.select.checkboxVendidos).off("change").on("change",function(e){
					Cupomida.poblarEstadisticas.Actualizar();
				});
				$(Cupomida.select.selectIntervalo).off("change").on("change",function(e){
					Cupomida.poblarEstadisticas.Actualizar();
				});
			},
			Visitas:function(){
				$(Cupomida.select.selectVisitas).off("change").on("change",function(e){
					Cupomida.poblarEstadisticas.VisitaCupon($(e.currentTarget).val(),0);
				});
				$(Cupomida.select.selectRangoVisitas).off("change").on("change",function(e){
					var ID=$(Cupomida.select.selectVisitas).val(),
						Rango=$(e.currentTarget).val();
					Cupomida.poblarEstadisticas.VisitaCupon(ID,Rango);
				});
			},
			Impresos:function(){
				Cupomida.setClicks.CuponLink.ver();
			},
			Vistos:function(){
				Cupomida.setClicks.CuponLink.ver();
			},
			Likes:function(){
				Cupomida.setClicks.CuponLink.ver();
			},
			Hora:function(datos,chart){
				google.visualization.events.addListener(chart,"select",function(e){
					var selectedItem=chart.getSelection()[0];
					if(selectedItem){
						Cupomida.obtenerCuponesHora(
							datos.getValue(selectedItem.row,0),
							function(){
								
							}
						);
					}
				});
			},
			Dia:function(datos,chart){
				google.visualization.events.addListener(chart,"select",function(e){
					var selectedItem = chart.getSelection()[0];
					if(selectedItem){
						Cupomida.obtenerCuponesDia(
							selectedItem.row,
							function(){
								
							}
						);
					}
				});
			},
			Categoria:function(datos,chart){
				google.visualization.events.addListener(chart,"select",function(e){
					var selectedItem = chart.getSelection()[0];
					if(selectedItem){
						Cupomida.obtenerCuponesCategoria(
							datos.getValue(selectedItem.row,0),
							function(){
								
							}
						);
					}
				});
			},
			FiltrarImpresos:function(){
				$("#FiltroImpresosEstadistica").off("keyup").on("keyup",function(e){
					$("#CuponesImpresos table tr").find("td:nth-child(2)").each(function(i,t){
						if(i>0){
							if($(t).html().toLowerCase().indexOf($(e.currentTarget).val().toLowerCase())!==-1){
								$(t).parent().show();
							}else{
								$(t).parent().hide();
							}
						}
					});
				});
			},
			FiltrarVistos:function(){
				$("#FiltroVistosDetalleEstadistica").off("keyup").on("keyup",function(e){
					$("#CuponesVistosDetalle table tr").find("td:nth-child(2)").each(function(i,t){
						if(i>0){
							if($(t).html().toLowerCase().indexOf($(e.currentTarget).val().toLowerCase())!==-1){
								$(t).parent().show();
							}else{
								$(t).parent().hide();
							}
						}
					});
				});
				$("#FiltroVistosEstadistica").off("keyup").on("keyup",function(e){
					$("#CuponesVistosTabla table tr").find("td:nth-child(2)").each(function(i,t){
						if(i>0){
							if($(t).html().toLowerCase().indexOf($(e.currentTarget).val().toLowerCase())!==-1){
								$(t).parent().show();
							}else{
								$(t).parent().hide();
							}
						}
					});
				});
			},
			FiltrarLikes:function(){
				$("#FiltroLikesEstadistica").off("keyup").on("keyup",function(e){
					$("#CuponesLikes table tr").find("td:nth-child(2)").each(function(i,t){
						if(i>0){
							if($(t).html().toLowerCase().indexOf($(e.currentTarget).val().toLowerCase())!==-1){
								$(t).parent().show();
							}else{
								$(t).parent().hide();
							}
						}
					});
				});
			}
		},
		PanelControl:{
			Usuarios:function(){
				$(".rol").off("change").on("change",function(e){
					var parent=$(e.currentTarget).parent().parent(),
						ID=parseInt(parent.attr("id").replace("Usuario-",""),10),
						Rol=$(e.currentTarget).val();
					Cupomida.actualizarRol(ID,Rol,function(Success){
						if(Success){
							$(e.currentTarget).addClass("box-success").delay(500).queue(function(){
								$(e.currentTarget).removeClass("box-success");
								$(e.currentTarget).dequeue();
							});
							if(Rol==2){
								var Clientes='<select multiple="multiple">';
								for(var c in Cupomida.clients){
									Clientes+='<option value="'+Cupomida.clients[c].id_cliente+'">'+
											Cupomida.clients[c].nombre+
										'</option>';
								}
								Clientes+='</select>';
								parent.find(".empresa").html(Clientes);
							}else parent.find(".empresa").html("");
						}else{
							$(e.currentTarget).addClass("box-error").delay(500).queue(function(){
								$(e.currentTarget).removeClass("box-error");
								$(e.currentTarget).dequeue();
							});
						}
					});
				});
				$(".empresa select").off("change").on("change",function(e){
					var Empresas=$(e.currentTarget).val(),
						ID=parseInt($(e.currentTarget).parent().parent().attr("id").replace("Usuario-",""),10);
					Cupomida.actualizarEmpresas(Empresas,ID,function(Success){
						if(Success){
							$(e.currentTarget).addClass("box-success").delay(500).queue(function(){
								$(e.currentTarget).removeClass("box-success");
								$(e.currentTarget).dequeue();
							});
						}else{
							$(e.currentTarget).addClass("box-error").delay(500).queue(function(){
								$(e.currentTarget).removeClass("box-error");
								$(e.currentTarget).dequeue();
							});
						}
					});
				});
			},
			Cupones:function(){
				Cupomida.setClicks.CuponLink.ver();
				Cupomida.setClicks.CuponLink.editar();
				Cupomida.setClicks.CuponLink.borrar(function(Success,id_cupon){
					if(Success) $("#Cupon-"+id_cupon).remove();
				});
			},
			Clientes:function(){
				$("#ClientesPanel .numero-cupones").off("change").on("change",function(e){
					var ID=$(e.currentTarget).parent().parent().attr("id").replace("Cliente-",""),
						Numero=$(e.currentTarget).val();
					if(/\d{1,3}/.test(Numero)){
						Cupomida.actualizarCliente({
							ID:ID,
							Cupones:Numero
						},function(success){
							if(success){
								$(e.currentTarget).addClass("box-success").delay(500).queue(function(){
									$(e.currentTarget).removeClass("box-success");
									$(e.currentTarget).dequeue();
								});
							}else{
								$(e.currentTarget).addClass("box-error").delay(500).queue(function(){
									$(e.currentTarget).removeClass("box-error");
									$(e.currentTarget).dequeue();
								});
							}
						});
					}else{
						Cupomida.mensajeError("Error","El contenido debe ser un número válido menor a 999.");
						$(e.currentTarget).addClass("box-error").delay(500).queue(function(){
							$(e.currentTarget).removeClass("box-error");
							$(e.currentTarget).dequeue();
						}).val(0);
					}
				});
				Cupomida.setClicks.ClienteLink.ver();
				Cupomida.setClicks.ClienteLink.borrar();
			},
			FiltrarUsuarios:function(){
				$("#FiltroUsuariosPanel").off("keyup").on("keyup",function(e){
					$("#UsuariosPanel table tr").find("td:first").each(function(i,t){
						if(i>0){
							if($(t).html().toLowerCase().indexOf($(e.currentTarget).val().toLowerCase())!==-1){
								$(t).parent().show();
							}else{
								$(t).parent().hide();
							}
						}
					});
				});
			},
			FiltrarCupones:function(){
				$("#FiltroCuponesPanel").off("keyup").on("keyup",function(e){
					$("#CuponesPanel table tr").find("td:nth-child(2)").each(function(i,t){
						if(i>0){
							if($(t).html().toLowerCase().indexOf($(e.currentTarget).val().toLowerCase())!==-1){
								$(t).parent().show();
							}else{
								$(t).parent().hide();
							}
						}
					});
				});
			},
			FiltrarDenuncias:function(){
				$("#FiltroDenunciasPanel").off("keyup").on("keyup",function(e){
					$("#DenunciasPanel table tr").find("td:nth-child(2)").each(function(i,t){
						if(i>0){
							if($(t).html().toLowerCase().indexOf($(e.currentTarget).val().toLowerCase())!==-1){
								$(t).parent().show();
							}else{
								$(t).parent().hide();
							}
						}
					});
				});
			},
			FiltrarClientes:function(){
				$("#FiltroClientesPanel").off("keyup").on("keyup",function(e){
					$("#ClientesPanel table tr").find("td:first").each(function(i,t){
						if(i>0){
							if($(t).html().toLowerCase().indexOf($(e.currentTarget).val().toLowerCase())!==-1){
								$(t).parent().show();
							}else{
								$(t).parent().hide();
							}
						}
					});
				});
			},
			FiltrarPendientes:function(){
				$("#FiltroPendientesPanel").off("keyup").on("keyup",function(e){
					$("#PendientesPanel table tr").find("td:first").each(function(i,t){
						if(i>0){
							if($(t).html().toLowerCase().indexOf($(e.currentTarget).val().toLowerCase())!==-1){
								$(t).parent().show();
							}else{
								$(t).parent().hide();
							}
						}
					});
				});
			},
		},
		TimeLine:function(){
			Cupomida.setClicks.CuponLink.ver();
			$(Cupomida.select.selectFechaTimeLine).off("change").on("change",function(e){
				Cupomida.poblarTimeLine($(e.currentTarget).val(),function(){
					Cupomida.setClicks.TimeLine();
				});
			});
		},
		Mapa:{
			Mark:function(map,marker,infoWindow){
				google.maps.event.addListener(marker,"click",function(e){
					infoWindow.open(map,marker);
					Cupomida.setImages();
				});
				google.maps.event.addListener(infoWindow,"domready",function(){
					Cupomida.setDiasHabiles(infoWindow.semana);
					Cupomida.setClicks.CuponLink.ver();
				});
			},
			Zoom:function(map,markers){
				google.maps.event.addListener(map,"zoom_changed",function(){
					var zoom=map.getZoom();
					for(var i=0;i<markers.length;i++){
						markers[i].setVisible(zoom>12);
					}
				});
			}
		},
		Codigos:function(){
			$(Cupomida.select.codigosEliminados).off("change").on("change",function(e){
				Cupomida.poblarCodigos({
					Eliminados:$(e.currentTarget).is(":checked"),
					Vencidos:$(Cupomida.select.codigosValidados).is(":checked")
				});
			});
			$(Cupomida.select.codigosVencidos).off("change").on("change",function(e){
				Cupomida.poblarCodigos({
					Eliminados:$(Cupomida.select.codigosEliminados).is(":checked"),
					Vencidos:$(e.currentTarget).is(":checked")
				});
			});
			$(Cupomida.select.codigosValidados).off("change").on("change",function(e){
				$("#Codigos table tr").find("td:first").each(function(i,t){
					if(i>0){
						if($(t).html().toLowerCase().indexOf($(e.currentTarget).val().toLowerCase())!==-1){
							$(t).parent().show();
						}else{
							$(t).parent().hide();
						}
					}
				});
			});
			$("#Filtro_Codigos").off("keyup").on("keyup",function(e){
				$("#Codigos table tr").find("td:first").each(function(i,t){
					if(i>0){
						if($(t).html().toLowerCase().indexOf($(e.currentTarget).val().toLowerCase())!==-1){
							$(t).parent().show();
						}else{
							$(t).parent().hide();
						}
					}
				});
			});
			$("#Filtro_Series").off("keyup").on("keyup",function(e){
				$("#Codigos table tr").find("td:nth-child(2)").each(function(i,t){
					if(i>0){
						if($(t).html().toLowerCase().indexOf($(e.currentTarget).val().toLowerCase())!==-1){
							$(t).parent().show();
						}else{
							$(t).parent().hide();
						}
					}
				});
			});
			$(".validar-codigo").off("click").on("click",function(e){
				var id=$(e.currentTarget).data("id");
				Cupomida.validarCodigo({ID:id},function(data){
					if(data){
						if(data.Impreso==="1"){
							if(data.Permiso==="1"){
								$(e.currentTarget).parent().html("Usado");
							}else{
								Cupomida.mensajeError(
									"Error",
									"No se puede validar el código porque no tiene los permisos suficientes."
								);
							}
						}else{
							Cupomida.mensajeError(
								"Error",
								"No se puede validar el código porque aún no ha sido impreso."
							);
						}
					}else{
						Cupomida.mensajeError(
							"Error",
							"No se pudo validar el código."
						);
					}
				});
			});
			$("#Descargar_Codigos").off("click").on("click",function(e){
				Cupomida.exportToCSV.apply(this,[$("#Codigos table")]);
			});
			Cupomida.setClicks.CuponLink.ver();
		},
		CuponLink:{
			ver:function(){
				$(".ver-cupon").off("click").on("click",function(e){
					Cupomida.pageCupon($(e.currentTarget).data("id"),function(){
						Cupomida.setClicks.Cupon();
					});
				});
			},
			editar:function(){
				$(".editar-cupon").off("click").on("click",function(e){
					Cupomida.pageEditarCupon($(e.currentTarget).data("id"));
				});
			},
			habilitar:function(callback){
				$(".habilitar-cupon").off("click").on("click",function(e){
					var id_cupon=$(e.currentTarget).data("id");
					Cupomida.habilitarCupon(id_cupon,function(habilitado){
						if(typeof(callback)=="function") callback(habilitado,id_cupon);
					});
				});
			},
			borrar:function(callback){
				$(".borrar-cupon").off("click").on("click",function(e){
					var id_cupon=$(e.currentTarget).data("id");
					Cupomida.borrarCupon(id_cupon,function(borrado){
						if(typeof(callback)=="function") callback(borrado,id_cupon);
					});
				});
			}
		},
		ClienteLink:{
			ver:function(){
				$(".ver-cliente").off("click").on("click",function(e){
					Cupomida.pageCliente($(e.currentTarget).data("id"),function(){
						Cupomida.setClicks.Galeria();
						Cupomida.primeraPagina();
					});
				});
			},
			borrar:function(){
				$(".borrar-cliente").off("click").on("click",function(e){
					var ID=$(e.currentTarget).data("id");
					Cupomida.borrarCliente(ID,function(success){
						if(success) $("#Cliente-"+ID).remove();
					});
				});
			}
		}
	},
	setImages:function(callback){
		$(".loading").each(function(i,e){
			var img=$(e),
				Accion="";
			if(img.hasClass("center-block")) Accion="Imagen Cliente";
			else Accion="Imagen Cupon";
			Cupomida.postImage.push($.post(Cupomida.JSONroot,{
				Accion:Accion,
				ID:img.data("id")
			},function(data){
				if(data.Success){
					img.attr("src",data.Result[0].imagen);
					img.removeClass("loading");
					if(typeof(callback)=="function"){
						callback(Accion,data.Result[0].imagen);
					}
				}else Cupomida.serverResponse(null,"");
			},"json").fail(function(jqXHR, textStatus){
				Cupomida.serverResponse(jqXHR,textStatus);
			}));
		});
	},
	setDiasHabiles:function(dias_habiles){
		dias_habiles=dias_habiles.split(",");
		for(var d in dias_habiles){
			if(dias_habiles[d]!=="N"){
				$($(Cupomida.select.semanaHabil)[d]).addClass("btn-red");
			}
		}
	},
	setMarkIcon:function(mark,img){
		mark.setIcon(
			Cupomida.resizeBase64Image(
				img,
				Cupomida.sizes.imageThumbMap
			)
		);
	},
	idImagenACanvas:function(Accion,ID,canvas,callback){
		$.post(Cupomida.JSONroot,{
			Accion:Accion,
			ID:ID
		},function(data){
			if(data.Success){
				var ctx=canvas.getContext("2d");
				if(Accion=="Imagen Cliente"){
					Cupomida.newCupon.imageCliente=data.Result[0].imagen;
					canvas.width=Cupomida.sizes.imageCliente.width;
					canvas.height=Cupomida.sizes.imageCliente.height;
				}else if(Accion=="Imagen Cupon"){
					Cupomida.newCupon.imageCupon=data.Result[0].imagen;
					canvas.width=Cupomida.sizes.imageCupon.width;
					canvas.height=Cupomida.sizes.imageCupon.height;
				}
				var img=new Image();
				img.src=data.Result[0].imagen;
				ctx.drawImage(img,0,0,canvas.width,canvas.height);
				if(typeof(callback)=="function") callback(data.Result[0].imagen);
			}else Cupomida.serverResponse(null,"");
		},"json").fail(function(jqXHR, textStatus){
			Cupomida.serverResponse(jqXHR,textStatus);
		});
	},
	nuevoCupon:function(postData,callback){
		$.post(Cupomida.JSONroot,postData,
		function(data){
			if(data.Success){
				if(typeof(callback)=="function"){
					callback(data.Response);
				}
			}else{
				Cupomida.errorCode(data.errorCode,function(){
					Cupomida.mensajeError("Error","Ocurrió un error al guardar su cupon.");
				});
			}
		},"json").fail(function(jqXHR, textStatus){
			Cupomida.serverResponse(jqXHR, textStatus);
		});
	},
	nuevoRating:function(postData,callback){
		$.post(Cupomida.JSONroot,postData,
		function(data){
			if(data.Success){
				if(typeof(callback)=="function") callback();
			}else{
				Cupomida.errorCode(data.errorCode,function(){
					Cupomida.mensajeError("Error","Ocurrió un error al guardar su valoración.");
				});
			}
		},"json").fail(function(jqXHR, textStatus){
			Cupomida.serverResponse(jqXHR, textStatus);
		});
	},
	poblarCupones:function(postData,callback){
		Cupomida.view=postData;
		$.post(Cupomida.JSONroot,postData,function(data){
			if(data.Success){
				if(data.Result.length > 0){
					var c=data.Result,rep,content="",distancia="",
						res=Cupomida.obtenerResolucion();
					Cupomida.cupones=c;
					for(var r in c){
						distancia="";
						if(typeof(Cupomida.user.latitude)!=="undefined"){
							distancia=" - A ";
								distancia+=Cupomida.calcularDistancia(
									Cupomida.user.latitude,
									Cupomida.user.longitude,
									c[r].latitud,
									c[r].longitud
								).toFixed(2);
							distancia+=" Kms.";
						}
						rep={
							_ID:c[r].id_cupon,
							_Titulo:c[r].titulo,
							_Descripcion:Cupomida.wordWrap(c[r].descripcion,10),
							_Precio:Cupomida.formatoNumero(c[r].precio),
							_Distancia:distancia,
							_Descuento:c[r].descuento,
							_Imagen:'img/loader.gif" class="loading" data-id="'+c[r].id_cupon
						};
						if(parseInt(Cupomida.user.administrador,10)==1){
							rep._Admin='<div class="btn-group">'+
									'<a '+
										'href="javascript:void(0);" '+
										'class="btn btn-red borrar-cupon" '+
										'data-id="'+c[r].id_cupon+'">Borrar</a>'+
									'<a '+
										'href="javascript:void(0);" '+
										'class="btn btn-blue editar-cupon" '+
										'data-id="'+c[r].id_cupon+'">Editar</a>'+
								'</div>';
						}else rep._Admin="";
						content+=Cupomida.tpl.cupon.replace(
							/_ID|_Admin|_Titulo|_Descripcion|_Precio|_Distancia|_Descuento|_Imagen/g,
							function(m){return rep[m];}
						);
						r=parseInt(r,10);
						if(res.width>=991){
							if((r+1)%3===0){
								content+='<div class="clearfix visible-xs-block"></div>';
							}
						}else if(res.width<992 && res.width>769){
							if((r+1)%2===0){
								content+='<div class="clearfix visible-xs-block"></div>';
							}
						}
					}
					rep={_Cupones:content};
					content=Cupomida.tpl.cupones.replace(/_Cupones/g,function(m){return rep[m];});
					$(Cupomida.select.content).html(content);
					Cupomida.paginarCupones(data.Total);
					Cupomida.setImages();
					if(typeof(callback)=="function") callback();
				}else Cupomida.emptyResult();
			}else{
				Cupomida.errorCode(data.errorCode,function(){
					Cupomida.mensajeError("Error","No se pudieron cargar los cupones de esta sección.");
				});
			}
		},"json").fail(function(jqXHR, textStatus){
			Cupomida.serverResponse(jqXHR, textStatus);
		});
	},
	poblarCategorias:function(callback){
		$.post(Cupomida.JSONroot,{
			Accion:"Categorias Disponibles"
		},function(data){
			if(data.Success){
				var c=data.Result,content="",rep;
				for(var r in c){
					rep={
						_ID:c[r].id_categoria,
						_Titulo:c[r].nombre,
						_Descripcion:c[r].descripcion
					};
					content+=Cupomida.tpl.categoria.replace(
						/_ID|_Titulo|_Descripcion/g,
						function(m){return rep[m];}
					);
				}
				$(Cupomida.select.categories).html(content);
				Cupomida.poblarRecomendacion(function(){
					Cupomida.setClicks.Menu();
				});
				if(typeof(callback)=="function") callback();
			}else{
				Cupomida.errorCode(data.errorCode,function(){
					Cupomida.mensajeError("Error","No se pudieron cargar las categorías.");
				});
				Cupomida.setClicks.Menu();
			}
		},"json").fail(function(jqXHR, textStatus){
			Cupomida.serverResponse(jqXHR, textStatus);
		});
	},
	poblarRecomendacion:function(callback){
		$.post(Cupomida.JSONroot,{
			Accion:"Cupon Destacado",
			Limit:1
		},function(data){
			if(data.Success){
				if(data.Result.length > 0){
					var rep={
						_ID:data.Result[0].id_cupon,
						_Ancho:"100%",
						_Alto:"100px",
						_Titulo:data.Result[0].titulo,
						_Class:""
					};
					var Imagen=Cupomida.tpl.imagenEspera.replace(
						/_ID|_Ancho|_Alto|_Titulo|_Class/g,
						function(m){return rep[m];}
					);
					rep={
						_ID:data.Result[0].id_cupon,
						_Titulo:data.Result[0].titulo,
						_Imagen:Imagen
					};
					var content=Cupomida.tpl.menuRecomendacion.replace(
						/_ID|_Titulo|_Imagen/g,
						function(m){return rep[m];}
					);
					$(Cupomida.select.recomendacion).html(content);
					Cupomida.setImages();
					if(typeof(callback)=="function") callback();
				}else{
					Cupomida.mensajeError("Sin cupones","Lo sentimos, en este momento no existen cupones.");
				}
			}else{
				Cupomida.errorCode(data.errorCode,function(){
					Cupomida.mensajeError("Error","No se pudo cargar el cupón recomendado.");
				});
			}
		},"json").fail(function(jqXHR, textStatus){
			Cupomida.serverResponse(jqXHR, textStatus);
		});
	},
	poblarPanelControl:{
		Denuncias:function(callback){
			var Content="",
				rep={
					_ID:"DenunciasPanel",
					_Titulo:"Denuncias:"
				};
			Content=Cupomida.tpl.contentPanel.replace(
				/_ID|_Titulo/g,
				function(m){return rep[m];}
			);
			$(Cupomida.select.panelControl).append(Content);
			var Denuncias='<table class="table hover-red">';
			Denuncias+='<thead><tr>'+
					'<td>Cantidad</td>'+
					'<td>Cupon</td>'+
					'<td>Tipo de denuncia</td>'+
					'<td>Enlace</td>'+
				'</tr></thead><tbody>';
			$.post(Cupomida.JSONroot,{
				Accion:"Denuncias"
			},function(data){
				if(data.Success){
					if(data.Result.length>0){
						var c=data.Result;
						for(var r in c){
							rep={
								_Numero:c[r].numero,
								_Cupon:c[r].titulo,
								_Tipo:c[r].tipo,
								_Ver:'<a href="javascript:void(0);" '+
									'class="ver-cupon" '+
									'data-id="'+c[r].id_cupon+'">Ver Cupón</a>'
							};
							Denuncias+=Cupomida.tpl.denunciaPanel.replace(
								/_Numero|_Cupon|_Tipo|_Ver/g,
								function(m){return rep[m];}
							);
						}
					}else{
						Denuncias+='<tr>'+
								'<td colspan="4">'+
									'<p>No hay denuncias de sus cupones.</p>'+
								'</td>'+
							'</tr>';
					}
					Denuncias+='</tbody></table>';
					$("#DenunciasPanel").html(Denuncias);
					Cupomida.setClicks.CuponLink.ver();
					Cupomida.setClicks.PanelControl.FiltrarDenuncias();
					if(typeof(callback)=="function") callback();
				}else{
					Cupomida.errorCode(data.errorCode,function(){
						Cupomida.mensajeError("Error","No se pudieron cargar las denuncias.");
					});
				}
			},"json").fail(function(jqXHR, textStatus){
				Cupomida.serverResponse(jqXHR, textStatus);
			});
		},
		Cupones:function(callback){
			var Content="",
				rep={
					_ID:"CuponesPanel",
					_Titulo:"Últimos cupones:"
				};
			Content=Cupomida.tpl.contentPanel.replace(
				/_ID|_Titulo/g,
				function(m){return rep[m];}
			);
			$(Cupomida.select.panelControl).append(Content);
			var Cupones='<table class="table hover-red">';
			Cupones+='<thead><tr>'+
					'<td>Imagen</td>'+
					'<td>Titulo</td>'+
					'<td>Descripción</td>'+
					'<td>Categoria</td>'+
					'<td>Enlace</td>'+
					'<td>Editar</td>'+
					'<td>Borrar</td>'+
				'</tr></thead><tbody>';
			$.post(Cupomida.JSONroot,{
				Accion:"Cupones"
			},function(data){
				if(data.Success){
					if(data.Result.length>0){
						var c=data.Result,Imagen="";
						for(var r in c){
							rep={
								_ID:c[r].id_cupon,
								_Ancho:"100px",
								_Alto:"100px",
								_Titulo:c[r].titulo,
								_Class:""
							};
							Imagen=Cupomida.tpl.imagenEspera.replace(
								/_ID|_Ancho|_Alto|_Titulo|_Class/g,
								function(m){return rep[m];}
							);
							rep={
								_ID:c[r].id_cupon,
								_Titulo:c[r].titulo,
								_Descripcion:Cupomida.wordWrap(c[r].descripcion,20),
								_Categoria:c[r].titulo_categoria,
								_Imagen:Imagen,
								_Ver:'<a href="javascript:void(0);" '+
									'class="ver-cupon" '+
									'data-id="'+c[r].id_cupon+'">Ver</a>',
								_Editar:'<a href="javascript:void(0);" '+
									'class="editar-cupon" '+
									'data-id="'+c[r].id_cupon+'">Editar</a>',
								_Borrar:'<a href="javascript:void(0);" '+
									'class="borrar-cupon" '+
									'data-id="'+c[r].id_cupon+'">Borrar</a>'
							};
							Cupones+=Cupomida.tpl.cuponPanel.replace(
								/_ID|_Titulo|_Descripcion|_Categoria|_Imagen|_Ver|_Editar|_Borrar/g,
								function(m){return rep[m];}
							);
						}
					}else{
						Cupones+='<tr>'+
								'<td colspan="7">'+
									'<p>No ha subido cupones aún.</p>'+
								'</td>'+
							'</tr>';
					}
					Cupones+='</tbody></table>';
					$("#CuponesPanel").html(Cupones);
					Cupomida.setClicks.PanelControl.Cupones();
					Cupomida.setClicks.PanelControl.FiltrarCupones();
					if(typeof(callback)=="function") callback();
				}else{
					Cupomida.errorCode(data.errorCode,function(){
						Cupomida.mensajeError("Error","No se pudieron cargar los últimos cupones.");
					});
				}
			},"json").fail(function(jqXHR, textStatus){
				Cupomida.serverResponse(jqXHR, textStatus);
			});
		},
		Usuarios:function(callback){
			var Content="",
				rep={
				_ID:"UsuariosPanel",
				_Titulo:"Usuarios:"
			};
			Content=Cupomida.tpl.contentPanel.replace(
				/_ID|_Titulo/g,
				function(m){return rep[m];}
			);
			$(Cupomida.select.panelControl).append(Content);
			var Usuarios='<table class="table hover-red">';
			Usuarios+='<thead><tr>'+
					'<td>Usuario</td>'+
					'<td>Administrador</td>'+
					'<td>Empresa</td>'+
				'</tr></thead><tbody>';
			$.post(Cupomida.JSONroot,{
				Accion:"Usuarios"
			},function(data){
				if(data.Success){
					var c=data.Result;
					for(var r in c){
						rep={
							_ID:c[r].id_usuario,
							_Nombre:c[r].first_name,
							_Apellido:c[r].last_name
						};
						Usuarios+=Cupomida.tpl.usuarioPanel.replace(
							/_ID|_Nombre|_Apellido/g,
							function(m){return rep[m];}
						);
					}
					Usuarios+='</tbody></table>';
					$("#UsuariosPanel").html(Usuarios);
					Cupomida.setClicks.PanelControl.FiltrarUsuarios();
					Cupomida.poblarPanelControl.Empresas(function(){
						for(var r in c){
							$("#Usuario-"+c[r].id_usuario+" select.rol").val(c[r].administrador);
							if(c[r].administrador!=2){
								$("#Usuario-"+c[r].id_usuario+" .empresa select").hide();
							}
						}
						Cupomida.setClicks.PanelControl.Usuarios();
						if(typeof(callback)=="function") callback();
					});
				}else{
					Cupomida.errorCode(data.errorCode,function(){
						Cupomida.mensajeError("Error","No se pudieron cargar los usuarios.");
					});
				}
			},"json").fail(function(jqXHR, textStatus){
				Cupomida.serverResponse(jqXHR, textStatus);
			});
		},
		Empresas:function(callback){
			var Clientes='<select multiple="multiple">';
			if(Cupomida.clients){
				for(var c in Cupomida.clients){
					Clientes+='<option value="'+Cupomida.clients[c].id_cliente+'">'+
							Cupomida.clients[c].nombre+
						'</option>';
				}
				Clientes+='</select>';
				$("#UsuariosPanel .empresa").html(Clientes);
				Cupomida.poblarPanelControl.EmpresasSeleccionadas(function(){
					if(typeof(callback)=="function") callback();
				});
			}else{
				$.post(Cupomida.JSONroot,{
					Accion:"Clientes"
				},function(data){
					if(data.Success){
						Cupomida.clients=data.Result;
						var c=data.Result;
						for(var r in c){
							Clientes+='<option value="'+c[r].id_cliente+'">'+
									c[r].nombre+
								'</option>';
						}
						Clientes+='</select>';
						$("#UsuariosPanel .empresa").html(Clientes);
						Cupomida.poblarPanelControl.EmpresasSeleccionadas(function(){
							if(typeof(callback)=="function") callback();
						});
					}else{
						Cupomida.errorCode(data.errorCode,function(){
							Cupomida.mensajeError("Error","No se pudieron cargar los clientes.");
						});
					}
				},"json").fail(function(jqXHR, textStatus){
					Cupomida.serverResponse(jqXHR, textStatus);
				});
			}
		},
		EmpresasSeleccionadas:function(callback){
			$.post(Cupomida.JSONroot,{
				Accion:"Clientes Usuarios"
			},function(data){
				if(data.Success){
					var c=data.Result;
					for(var r in c){
						var select=$("#Usuario-"+c[r].id_usuario+" .empresa select"),
							valores=$(select).val();
						if(valores===null) select.val(c[r].id_cliente);
						else{
							valores.push(c[r].id_cliente);
							select.val(valores);
						}
					}
					if(typeof(callback)=="function") callback();
				}else{
					Cupomida.errorCode(data.errorCode,function(){
						Cupomida.mensajeError("Error","No se pudieron cargar los clientes.");
					});
				}
			},"json").fail(function(jqXHR, textStatus){
				Cupomida.serverResponse(jqXHR, textStatus);
			});
		},
		Clientes:function(callback){
			var Content="",
				rep={
				_ID:"ClientesPanel",
				_Titulo:"Clientes:"
			};
			Content=Cupomida.tpl.contentPanel.replace(
				/_ID|_Titulo/g,
				function(m){return rep[m];}
			);
			$(Cupomida.select.panelControl).append(Content);
			var Clientes='<table class="table hover-red">';
			Clientes+='<thead><tr>'+
					'<td>Cliente</td>'+
					'<td>Cupones</td>'+
					'<td>Ver Cupones</td>'+
					'<td>Desvincular</td>'+
				'</tr></thead><tbody>';
			for(var c in Cupomida.clients){
				rep={
					_ID:Cupomida.clients[c].id_cliente,
					_Cliente:Cupomida.clients[c].nombre,
					_Cupones:'<input '+
						'class="numero-cupones" '+
						'type="number" '+
						'value="'+Cupomida.clients[c].cupones+'" />',
					_Ver:'<a href="javascript:void(0);" '+
						'class="ver-cliente" '+
						'data-id="'+Cupomida.clients[c].id_cliente+'">Ver Cupones</a>',
					_Eliminar:'<a href="javascript:void(0);" '+
						'class="borrar-cliente" '+
						'data-id="'+Cupomida.clients[c].id_cliente+'">Desvincular</a>'
				};
				Clientes+=Cupomida.tpl.clientePanel.replace(
					/_ID|_Cliente|_Cupones|_Ver|_Eliminar/g,
					function(m){return rep[m];}
				);
			}
			Clientes+='</tbody></table>';
			$("#ClientesPanel").html(Clientes);
			Cupomida.setClicks.PanelControl.FiltrarClientes();
			Cupomida.setClicks.PanelControl.Clientes();
			if(typeof(callback)=="function") callback();
		},
		Pendientes:function(callback){
			var Content="",
				rep={
				_ID:"PendientesPanel",
				_Titulo:"Cupones Pendientes de Pago:"
			};
			Content=Cupomida.tpl.contentPanel.replace(
				/_ID|_Titulo/g,
				function(m){return rep[m];}
			);
			$(Cupomida.select.panelControl).append(Content);
			var Pendientes='<table class="table hover-red">';
			Pendientes+='<thead><tr>'+
					'<td>Cliente</td>'+
					'<td>Ver Cupón</td>'+
					'<td>Habilitar</td>'+
					'<td>Eliminar</td>'+
				'</tr></thead><tbody>';
			$.post(Cupomida.JSONroot,{
				Accion:"Cupones Pendientes"
			},function(data){
				if(data.Success){
					if(data.Result.length > 0){
						for(var p in data.Result){
							rep={
								_ID:data.Result[p].id_cupon,
								_Cliente:data.Result[p].nombre_cliente,
								_Ver:'<a href="javascript:void(0);" '+
									'class="ver-cupon" '+
									'data-id="'+data.Result[p].id_cupon+'">Ver Cupón</a>',
								_Habilitar:'<a href="javascript:void(0);" '+
									'class="habilitar-cupon" '+
									'data-id="'+data.Result[p].id_cupon+'">Habilitar</a>',
								_Eliminar:'<a href="javascript:void(0);" '+
									'class="borrar-cupon" '+
									'data-id="'+data.Result[p].id_cupon+'">Eliminar</a>'
							};
							Pendientes+=Cupomida.tpl.pendientePanel.replace(
								/_ID|_Cliente|_Ver|_Habilitar|_Eliminar/g,
								function(m){return rep[m];}
							);
						}
					}else{
						Pendientes+='<tr><td colspan="5">'+
							'<p>No hay cupones pendientes.</p>'+
						'</td></tr>';
					}
					Pendientes+='</tbody></table>';
					$("#PendientesPanel").html(Pendientes);
					Cupomida.setClicks.PanelControl.FiltrarPendientes();
					Cupomida.setClicks.CuponLink.ver();
					Cupomida.setClicks.CuponLink.habilitar(function(Success,id_cupon){
						if(Success) $("#Pendiente-"+id_cupon).remove();
					});
					Cupomida.setClicks.CuponLink.borrar(function(Success,id_cupon){
						if(Success) $("#Pendiente-"+id_cupon).remove();
					});
					if(typeof(callback)=="function") callback();
				}else{
					Cupomida.errorCode(data.errorCode,function(){
						Cupomida.mensajeError("Error","No se pudieron cargar los cupones de esta sección.");
					});
				}
			},"json").fail(function(jqXHR, textStatus){
				Cupomida.serverResponse(jqXHR, textStatus);
			});
		}
	},
	poblarTimeLine:function(Fecha,callback){
		Fecha=Fecha || undefined;
		$.post(Cupomida.JSONroot,{
			Accion:"Cupones Disponibles",
			Todos:true,
			Fecha:Fecha,
			Limite:30
		},function(data){
			var rep,content="";
			if(data.Success){
				if(data.Result.length > 0){
					var c=data.Result.sort(function(a,b){
						var an = /\d*/.exec(a.hora_inicio),
							bn = /\d*/.exec(b.hora_inicio);
						if(an > bn) return 1;
						if(an < bn) return -1;
						return 0;
					});
					var ultima_posicion=0,
						hora_primera=0,
						ultima_hora=0,
						posicion=0,
						alto=1,
						ancho=1,
						max_ancho=1,
						tamano_fila=69,
						tamano_columna=138,
						tiempo_inicio,
						horas="",
						hora_inicio,
						minuto_inicio,
						tiempo_fin,
						hora_fin,
						minuto_fin,
						hora_total,
						proporcion_minutos,
						distancia="";
					for(var r in c){
						tiempo_inicio=/(\d*):(\d*)/.exec(c[r].hora_inicio);
						hora_inicio=parseInt(tiempo_inicio[1],10);
						minuto_inicio=parseInt(tiempo_inicio[2],10);
						tiempo_fin=/(\d*):(\d*)/.exec(c[r].hora_fin);
						hora_fin=parseInt(tiempo_fin[1],10);
						minuto_fin=parseInt(tiempo_fin[2],10);
						hora_total=hora_fin-hora_inicio;
						proporcion_minutos=Math.round(tamano_fila/60);
						alto=(tamano_fila*hora_total)+
							(proporcion_minutos*minuto_fin)-
							(proporcion_minutos*minuto_inicio);
						if(hora_primera===0){
							horas="";
							hora_primera=hora_inicio;
							for(var i=hora_inicio;i<=23;i++){
								horas+=Cupomida.tpl.timeLineHora.replace("_Hora",i+":00");
							}
							$(".tableTimeLine tbody").html(horas);
							$("#CuponesTimeLine").css({
								height:((25-hora_inicio)*tamano_fila)+"px"
							});
						}
						posicion=tamano_fila+(
							tamano_fila*
							(hora_inicio-hora_primera)+
							(minuto_inicio*proporcion_minutos)
						);
						if(ultima_posicion>100){
							if(ultima_posicion!=posicion+alto){
								if(ultima_posicion<posicion){
									hora_primera=ultima_hora;
									posicion=tamano_fila+(
										tamano_fila*
										(hora_inicio-hora_primera)+
										(minuto_inicio*proporcion_minutos)
									);
									content+='<div class="clearfix"></div>';
									ancho=1;
								}else ancho++;
							}else ancho++;
						}
						if(ancho>max_ancho) max_ancho=ancho;
						distancia="";
						if(typeof(Cupomida.user.latitude)!=="undefined"){
							distancia="A ";
								distancia+=Cupomida.calcularDistancia(
									Cupomida.user.latitude,
									Cupomida.user.longitude,
									c[r].latitud,
									c[r].longitud
								).toFixed(2);
							distancia+="Km de Ud.";
						}
						rep={
							_ID:c[r].id_cupon,
							_Titulo:c[r].titulo,
							_Imagen:'img/loader.gif" class="loading" data-id="'+c[r].id_cupon,
							_Posicion:posicion,
							_Alto:alto,
							_Hora:/\d*:\d*/.exec(c[r].hora_inicio)+" - "+/\d*:\d*/.exec(c[r].hora_fin),
							_Distancia:distancia
						};
						content+=Cupomida.tpl.cuponTimeLine.replace(
							/_ID|_Titulo|_Imagen|_Alto|_Posicion|_Hora|_Distancia/g,
							function(m){return rep[m];}
						);
						ultima_hora=hora_fin;
						ultima_posicion=posicion+alto;
					}
					$("#CuponesTimeLine").html(content).css({
						width:(max_ancho*tamano_columna)+"px"
					});
					Cupomida.setImages();
					if(typeof(callback)=="function") callback();
				}else{
					rep={
						_ID:0,
						_Titulo:"No existen cupones para esta fecha, ",
						_Imagen:'img/loader.gif" class="loading" data-id="0',
						_Posicion:"109",
						_Alto:"300",
						_Hora:"intente con otra fecha.",
						_Distancia:""
					};
					content=Cupomida.tpl.cuponTimeLine.replace(
						/_ID|_Titulo|_Imagen|_Alto|_Posicion|_Hora|_Distancia/g,
						function(m){return rep[m];}
					);
					$("#CuponesTimeLine").html(content);
				}
			}else{
				Cupomida.errorCode(data.errorCode,function(){
					Cupomida.mensajeError("Error","No se pudieron cargar los cupones de esta sección.");
				});
			}
		},"json").fail(function(jqXHR, textStatus){
			Cupomida.serverResponse(jqXHR, textStatus);
		});
	},
	poblarEstadisticas:{
		Limites:function(){
			return {
				Rango:$(Cupomida.select.selectIntervalo).val(),
				Eliminados:$(Cupomida.select.checkboxEliminados).is(":checked"),
				Vencidos:$(Cupomida.select.checkboxVencidos).is(":checked"),
				Vendidos:$(Cupomida.select.checkboxVendidos).is(":checked")
			};
		},
		Actualizar:function(callback){
			var Limites=Cupomida.poblarEstadisticas.Limites();
			Cupomida.poblarEstadisticas.Vistos(Limites);
			Cupomida.poblarEstadisticas.Impresos(Limites);
			Cupomida.poblarEstadisticas.Likes(Limites);
		},
		Visitas:function(callback){
			$.post(Cupomida.JSONroot,{
				Accion:"Cupones",
				Limite:100
			},function(data){
				if(data.Success){
					var Cupones="";
					for(var r in data.Result){
						Cupones+='<option '+
								'value="'+data.Result[r].id_cupon+'">'+
								data.Result[r].titulo+
							'</option>';
					}
					$(Cupomida.select.selectVisitas).append(Cupones);
					if(typeof(callback)=="function") callback();
				}else{
					Cupomida.errorCode(data.errorCode,function(){
						Cupomida.mensajeError("Error","Ocurrió un error al cargar las estadísticas.");
					});
				}
			},"json").fail(function(jqXHR, textStatus){
				Cupomida.serverResponse(jqXHR, textStatus);
			});
		},
		VisitaCupon:function(ID,Semana,callback){
			Semana=Semana || 0;
			$.post(Cupomida.JSONroot,{
				Accion:"Visitas por ID",
				ID:ID,
				Semana:Semana
			},function(data){
				if(data.Success){
					var Puntos={};
					Cupomida.poblarEstadisticas.RangosFechas(data.Result.Total);
					Puntos.cols=[
						{
							"id":"",
							"label":"Fecha",
							"pattern":"",
							"type":"date"
						},
						{
							"id":"",
							"label":"Numero",
							"pattern":"",
							"type":"number"
						}
					];
					Puntos.rows=[];
					var dia;
					for(var r in data.Result){
						dia=data.Result[r].Dia.split("-");
						if(data.Result[r].id_cupon==ID){
							Puntos.rows.push({
								"c":[
									{"v":new Date(dia[0],dia[1],dia[2]),"f":null},
									{"v":data.Result[r].Numero,"f":null}
								]
							});
						}
					}
					Cupomida.initGrafico(function(){
						Cupomida.generarGrafico({
							Div:"CuponesVistos",
							Type:"Line",
							Titulo:"Estadísticas",
							TituloVertical:"Cantidad de visitas",
							TituloHorizontal:"Fecha",
							Puntos:Puntos
						});
						Cupomida.setClicks.Estadisticas.Visitas();
						if(typeof(callback)=="function") callback();
					});
				}else{
					Cupomida.errorCode(data.errorCode,function(){
						Cupomida.mensajeError("Error","Ocurrió un error al cargar las estadísticas.");
					});
				}
			},"json").fail(function(jqXHR, textStatus){
				Cupomida.serverResponse(jqXHR, textStatus);
			});
		},
		RangosFechas:function(Total){
			$(Cupomida.select.selectRangoVisitas).html('<option value="0">Seleccione una Fecha</option>');
			var Opciones="",
				Rangos=Math.ceil((Total/7));
			for(var i=0;i<Rangos;i++){
				Opciones+='<option '+
								'value="'+i+'">'+
								'Semana '+(i+1)+
							'</option>';
			}
			$(Cupomida.select.selectRangoVisitas).append(Opciones);
		},
		Impresos:function(postData,callback){
			var Impresos='<table class="table hover-red">'+
					'<thead><tr>'+
					'<td>Cantidad de impresiones</td>'+
					'<td>Cupón</td>'+
					'<td>Restantes para impresión</td>'+
					'<td>Ver Cupón</td>'+
				'</tr></thead><tbody>';
			postData.Accion="Ventas";
			$.post(Cupomida.JSONroot,postData,function(data){
				if(data.Success){
					if(data.Result.length>0){
						var c=data.Result,rep;
						for(var r in c){
							rep={
								_Numero:c[r].numero,
								_Cupon:c[r].titulo,
								_Restantes:c[r].restantes,
								_Ver:'<a href="javascript:void(0);" '+
									'class="ver-cupon" '+
									'data-id="'+c[r].id_cupon+'">Ver Cupón</a>'
							};
							Impresos+=Cupomida.tpl.impresosEstadisticas.replace(
								/_Numero|_Cupon|_Restantes|_Ver/g,
								function(m){return rep[m];}
							);
						}
					}else{
						Impresos+='<tr><td colspan="4"><p>No hay cupones impresos en este intervalo de fecha.</p></td></tr>';
					}
					Impresos+='</tbody></table>';
					$("#CuponesImpresos").html(Impresos);
					Cupomida.setClicks.Estadisticas.Impresos();
					Cupomida.setClicks.Estadisticas.FiltrarImpresos();
					if(typeof(callback)=="function") callback();
				}else{
					Cupomida.errorCode(data.errorCode,function(){
						Cupomida.mensajeError("Error","No se pudieron cargar los cupones impresos.");
					});
				}
			},"json").fail(function(jqXHR, textStatus){
				Cupomida.serverResponse(jqXHR, textStatus);
			});
		},
		Vistos:function(postData,callback){
			var hasVistos=false,hasVistosDetalle=false,
				Hora=[
					0,0,0,0,0,
					0,0,0,0,0,
					0,0,0,0,0,
					0,0,0,0,0,
					0,0,0,0
				],
				Dia=[0,0,0,0,0,0,0],
				Categoria={};
			var Vistos='<table class="table hover-red">'+
					'<thead><tr>'+
						'<td>Cantidad de visualizaciones</td>'+
						'<td>Cupón</td>'+
						'<td>Ver Cupón</td>'+
					'</tr></thead><tbody>',
				VistosDetalle='<table class="table hover-red">'+
					'<thead><tr>'+
						'<td>Cantidad</td>'+
						'<td>Cupón</td>'+
						'<td>Ver Cupón</td>'+
					'</tr></thead><tbody>';
			postData.Accion="Visitas";
			postData.Orden="numero";
			postData.Grupo="`tipo`,`id_cupon`";
			$.post(Cupomida.JSONroot,postData,function(data){
				if(data.Success){
					var c=data.Result,rep;
					for(var r in c){
						rep={
							_Numero:c[r].numero,
							_Cupon:c[r].titulo,
							_Ver:'<a href="javascript:void(0);" '+
								'class="ver-cupon" '+
								'data-id="'+c[r].id_cupon+'">Ver Cupón</a>'
						};
						if(c[r].tipo=="2"){
							VistosDetalle+=Cupomida.tpl.vistosEstadisticas.replace(
								/_Numero|_Cupon|_Ver/g,
								function(m){return rep[m];}
							);
							hasVistosDetalle=true;
						}else{
							Vistos+=Cupomida.tpl.vistosEstadisticas.replace(
								/_Numero|_Cupon|_Ver/g,
								function(m){return rep[m];}
							);
							Hora[c[r].Hora]++;
							Dia[c[r].dia_semana]++;
							if(typeof(Categoria[c[r].nombre_categoria])=="undefined"){
								Categoria[c[r].nombre_categoria]=1;
							}else Categoria[c[r].nombre_categoria]++;
							hasVistos=true;
						}
					}
					if(!hasVistos){
						Vistos+='<tr><td colspan="3">'+
							'<p>No hay cupones vistos en este intervalo de fecha.</p>'+
						'</td></tr>';
					}else{
						Cupomida.poblarEstadisticas.Hora(Hora);
						Cupomida.poblarEstadisticas.Dia(Dia);
						Cupomida.poblarEstadisticas.Categoria(Categoria);
					}
					if(!hasVistosDetalle){
						VistosDetalle+='<tr><td colspan="3">'+
							'<p>No hay cupones vistos detalladamente en este intervalo de fecha.</p>'+
						'</td></tr>';
					}
					Vistos+='</tbody></table>';
					VistosDetalle+='</tbody></table>';
					$("#CuponesVistosTabla").html(Vistos);
					$("#CuponesVistosDetalle").html(VistosDetalle);
					Cupomida.setClicks.Estadisticas.Vistos();
					Cupomida.setClicks.Estadisticas.FiltrarVistos();
					if(typeof(callback)=="function") callback();
				}else{
					Cupomida.errorCode(data.errorCode,function(){
						Cupomida.mensajeError("Error","No se pudieron cargar los cupones vistos.");
					});
				}
			},"json").fail(function(jqXHR, textStatus){
				Cupomida.serverResponse(jqXHR, textStatus);
			});
		},
		Likes:function(postData,callback){
			var Likes='<table class="table hover-red">'+
				'<thead><tr>'+
					'<td>Cantidad de likes</td>'+
					'<td>Cupón</td>'+
					'<td>Ver Cupón</td>'+
				'</tr></thead><tbody>';
			postData.Accion="Likes";
			$.post(Cupomida.JSONroot,postData,function(data){
				if(data.Success){
					if(data.Result.length>0){
						var c=data.Result,rep;
						for(var r in c){
							rep={
								_Numero:c[r].numero,
								_Cupon:c[r].titulo,
								_Ver:'<a href="javascript:void(0);" '+
									'class="ver-cupon" '+
									'data-id="'+c[r].id_cupon+'">Ver Cupón</a>'
							};
							Likes+=Cupomida.tpl.likesEstadisticas.replace(
								/_Numero|_Cupon|_Ver/g,
								function(m){return rep[m];}
							);
						}
					}else{
						Likes+='<tr><td colspan="3"><p>No hay likes en este intervalo de fecha.</p></td></tr>';
					}
					Likes+='</tbody></table>';
					$("#CuponesLikes").html(Likes);
					Cupomida.setClicks.Estadisticas.Likes();
					Cupomida.setClicks.Estadisticas.FiltrarLikes();
					if(typeof(callback)=="function") callback();
				}else{
					Cupomida.errorCode(data.errorCode,function(){
						Cupomida.mensajeError("Error","No se pudieron cargar los likes a cupones.");
					});
				}
			},"json").fail(function(jqXHR, textStatus){
				Cupomida.serverResponse(jqXHR, textStatus);
			});
		},
		Hora:function(Horas){
			var Puntos={};
			Puntos.cols=[
				{
					"id":"hora",
					"label":"Hora",
					"pattern":"",
					"type":"number"
				},
				{
					"id":"cantiadad_visitas",
					"label":"Cantidad de visitas",
					"pattern":"",
					"type":"number"
				}
			];
			Puntos.rows=[];
			for(var h in Horas){
				Puntos.rows.push({
					"c":[
						{"v":h,"f":null},
						{"v":Horas[h],"f":null}
					]
				});
			}
			Cupomida.generarGrafico({
				Div:"CuponesVistosHora",
				Type:"Line",
				Titulo:"Estadísticas de visitas por hora.",
				TituloVertical:"Cantidad de visitas",
				TituloHorizontal:"Hora del día",
				Puntos:Puntos
			},function(data,chart){
				Cupomida.setClicks.Estadisticas.Hora(data,chart);
			});
		},
		Dia:function(Dias){
			var Puntos={},Nombre="";
			Puntos.cols=[
				{
					"id":"dia_semana",
					"label":"Dia de la semana",
					"pattern":"",
					"type":"string"
				},
				{
					"id":"cantidad_visitas",
					"label":"Cantidad de visitas",
					"pattern":"",
					"type":"number"
				}
			];
			Puntos.rows=[];
			for(var d in Dias){
				Nombre=Cupomida.numeroDia(d);
				Puntos.rows.push({
					"c":[
						{"v":Nombre,"f":null},
						{"v":Dias[d],"f":null}
					]
				});
			}
			Cupomida.generarGrafico({
				Div:"CuponesVistosDia",
				Type:"Line",
				Titulo:"Estadísticas de visitas por día de la semana.",
				TituloVertical:"Cantidad de visitas",
				TituloHorizontal:"Día de la semana",
				Puntos:Puntos
			},function(data,chart){
				Cupomida.setClicks.Estadisticas.Dia(data,chart);
			});
		},
		Categoria:function(Categorias){
			var Puntos={};
			Puntos.cols=[
				{
					"id":"categoria",
					"label":"Categoría",
					"pattern":"",
					"type":"string"
				},
				{
					"id":"cantidad_visitas",
					"label":"Cantidad de visitas",
					"pattern":"",
					"type":"number"
				}
			];
			Puntos.rows=[];
			for(var c in Categorias){
				Puntos.rows.push({
					"c":[
						{"v":c,"f":null},
						{"v":Categorias[c],"f":null}
					]
				});
			}
			Cupomida.generarGrafico({
				Div:"CuponesVistosCategoria",
				Type:"Column",
				Titulo:"Estadísticas de visitas por categoría.",
				TituloVertical:"Cantidad de visitas",
				TituloHorizontal:"Categoría",
				Puntos:Puntos
			},function(data,chart){
				Cupomida.setClicks.Estadisticas.Categoria(data,chart);
			});
		}
	},
	poblarPerfil:{
		Imagen:function(){
			var canvas=$("#nuevaImagenPerfil")[0],
				ctx=canvas.getContext("2d"),
				img=new Image();
			canvas.width=Cupomida.sizes.imagePerfil.width;
			canvas.height=Cupomida.sizes.imagePerfil.height;
			if(Cupomida.user.imagen===""){
				Cupomida.readyFacebook(function(){
					/*
					$.get("https://graph.facebook.com/v2.1/me/picture",{
						width:canvas.width,
						height:canvas.height
					},function(data){
						img.src=data;
						ctx.drawImage(img,0,0,canvas.width,canvas.height);
					}).fail(function(jqXHR, textStatus){
						Cupomida.serverResponse(jqXHR, textStatus);
					});
					*/
					FB.api('/me/picture?width='+canvas.width+'&height='+canvas.height,function(response){
						img.src=response.data.url;
						ctx.drawImage(img,0,0,canvas.width,canvas.height);
					});
				});
			}else{
				img.src=Cupomida.user.imagen;
				ctx.drawImage(img,0,0,canvas.width,canvas.height);
			}
		},
		Genero:function(){
			if(Cupomida.user.gender===""){
				$("input[name='perfilSexo']")[0].attr("checked","checked");
			}else{
				$("input[name='perfilSexo']").each(function(i,e){
					if($(e).val()==Cupomida.user.gender){
						$(e).attr("checked","checked");
					}
				});
			}
		},
		Categorias:function(callback){
			Cupomida.obtenerCategorias(function(){
				var Categorias="",
					Preferencias=[];
				if(Cupomida.user.preferencias){
					Preferencias=Cupomida.user.preferencias.split(",");
				}
				for(var c in Cupomida.categories){
					Categorias+='<input '+
						'type="checkbox" '+
						'name="preferencias" ';
					if(Preferencias.indexOf(Cupomida.categories[c].id_categoria)!==-1){
						Categorias+='checked="checked" ';
					}
					Categorias+='value="'+Cupomida.categories[c].id_categoria+'">';
					Categorias+=Cupomida.categories[c].nombre+'</br>';
				}
				$("#perfilCategoria").append(Categorias);
				if(typeof(callback)=="function") callback();
			});
		},
		Datos:function(callback){
			var rep={
				_Nombre:Cupomida.user.first_name,
				_Apellido:Cupomida.user.last_name,
				_Correo:Cupomida.user.email
			};
			var content=Cupomida.tpl.perfil.replace(
				/_Nombre|_Apellido|_Correo/g,
				function(m){return rep[m];
			});
			$(Cupomida.select.content).html(content);
			if(typeof(callback)=="function") callback();
		},
		Cupones:function(postData,callback){
			var Codigos='<table class="table hover-red">'+
				'<thead><tr>'+
					'<td>Cupón</td>'+
					'<td>Serie</td>'+
					'<td>Validez</td>'+
					'<td>Emisión</td>'+
					'<td>Cobrado</td>'+
					'<td>Ver Cupón</td>'+
				'</tr></thead><tbody>';
			postData.Accion="Codigos Cliente";
			postData.Perfil=true;
			$.post(Cupomida.JSONroot,postData,function(data){
				if(data.Success){
					if(data.Result.length>0){
						var c=data.Result,
							rep,
							impreso="",
							cobrado="",
							valido="";
						for(var r in c){
							if(c[r].cobrado==="0") cobrado="Sin usar";
							else cobrado="Usado";
							valido="Válido hace "+
								Cupomida.calcularDias(c[r].valido_desde)+
								" días, hasta "+
								Cupomida.calcularDias(c[r].valido_hasta)+
								" días más.";
							rep={
								_ID:c[r].id_codigo,
								_Cupon:c[r].titulo,
								_Serie:c[r].serie,
								_Validez:valido,
								_Fecha:c[r].emision,
								_Cobrado:cobrado,
								_Ver:'<a href="javascript:void(0);" '+
									'class="ver-cupon" '+
									'data-id="'+c[r].id_cupon+'">Ver Cupón</a>'
							};
							Codigos+=Cupomida.tpl.codigosUsuario.replace(
								/_ID|_Cupon|_Serie|_Validez|_Fecha|_Cobrado|_Ver/g,
								function(m){return rep[m];}
							);
						}
					}else{
						Codigos+='<tr><td colspan="7"><p>No hay códigos disponibles.</p></td></tr>';
					}
					Codigos+='</tbody></table>';
					$("#MisCupones").html(Codigos);
					Cupomida.setClicks.CuponLink.ver();
					if(typeof(callback)=="function") callback();
				}else{
					Cupomida.errorCode(data.errorCode,function(){
						Cupomida.mensajeError("Error","No se pudieron cargar los cupones impresos.");
					});
				}
			},"json").fail(function(jqXHR, textStatus){
				Cupomida.serverResponse(jqXHR, textStatus);
			});
		}
	},
	poblarMapa:{
		Mapa:function(callback){
			var center=false;
			$(Cupomida.select.content).html(Cupomida.tpl.mapa);
			if(Cupomida.user){
				if(Cupomida.user.latitude){
					center=new google.maps.LatLng(
						Cupomida.user.latitude,
						Cupomida.user.longitude
					);
				}
			}
			if(!center){
				center=new google.maps.LatLng(
					Cupomida.centro.lat,
					Cupomida.centro.lon
				);
			}
			Cupomida.showMap({
				divId:"pageMapa",
				Titulo:"Mapa de cupones",
				mapProp:{
					center:center,
					zoom:12,
					mapTypeId:google.maps.MapTypeId.ROADMAP
				}
			},function(map){
				Cupomida.poblarMapa.Cupones(map,function(){
					if(typeof(callback)=="function") callback();
				});
			});
		},
		Cupones:function(map,callback){
			var mark=false,
				markers=[],
				positions=[],
				infoWindow=false,
				content="",
				imagen="",
				rep={};
			$.post(Cupomida.JSONroot,{
				Accion:"Cupones Disponibles",
				Limite:50,
				Todos:true
			},function(data){
				if(data.Success){
					if(data.Result.length > 0){
						for(var c in data.Result){
							if(positions.indexOf(data.Result[c].id_ubicacion)===-1){
								positions.push(data.Result[c].id_ubicacion);
								mark=new google.maps.Marker({
									position:new google.maps.LatLng(
										data.Result[c].latitud,
										data.Result[c].longitud
									),
									map:map,
									title:data.Result[c].titulo
								});
								markers.push(mark);
								Cupomida.poblarMapa.Icons(data.Result[c].id_cupon,mark);
								rep={
									_Ancho:"100%",
									_Alto:"auto",
									_ID:data.Result[c].id_cupon,
									_Titulo:data.Result[c].titulo,
									_Class:""
								};
								imagen=Cupomida.tpl.imagenEspera.replace(
									/_ID|_Titulo|_Alto|_Ancho|_Class/g,
									function(m){return rep[m];}
								);
								rep={
									_ID:data.Result[c].id_cupon,
									_Imagen:imagen,
									_Titulo:data.Result[c].titulo,
									_Descripcion:data.Result[c].descripcion,
									_Detalle:data.Result[c].detalle,
									_Precio:Cupomida.formatoNumero(data.Result[c].precio),
									_Evaluacion:data.Result[c].valoraciones_cupon+"/5",
									_Horario:"Disponible desde las "+
										data.Result[c].hora_inicio+
										" hasta las "+
										data.Result[c].hora_fin,
									_Dias:Cupomida.tpl.cuponSemana
								};
								content=Cupomida.tpl.infoMapa.replace(
									Cupomida.regularExpressions.infoWindow,
									function(m){return rep[m];}
								);
								infoWindow=new google.maps.InfoWindow({
									semana:data.Result[c].dias_habiles,
									content:content
								});
								Cupomida.setClicks.Mapa.Mark(map,mark,infoWindow);
							}
						}
						Cupomida.setClicks.Mapa.Zoom(map,markers);
					}else{
						Cupomida.errorCode(data.errorCode,function(){
							Cupomida.mensajeError("Vacío","En este momento no existen cupones.");
						});
					}
				}else{
					Cupomida.errorCode(data.errorCode,function(){
						Cupomida.mensajeError("Error","No se pudieron cargar los cupones de esta sección.");
					});
				}
			},"json").fail(function(jqXHR, textStatus){
				Cupomida.serverResponse(jqXHR, textStatus);
			});
		},
		Icons:function(id,mark){
			$.post(Cupomida.JSONroot,{
				Accion:"Imagen Cupon",
				ID:id
			},function(data){
				if(data.Success){
					Cupomida.setMarkIcon(mark,data.Result[0].imagen);
				}else Cupomida.serverResponse(null,"");
			},"json").fail(function(jqXHR, textStatus){
				Cupomida.serverResponse(jqXHR,textStatus);
			});
		}
	},
	poblarCodigos:function(postData,callback){
		var Codigos='<table class="table hover-red">'+
			'<thead><tr>'+
				'<td>Cupón</td>'+
				'<td>Serie</td>'+
				'<td>Validez</td>'+
				'<td>Impreso</td>'+
				'<td>Emisión</td>'+
				'<td>Cobrado</td>'+
				'<td>Ver Cupón</td>'+
			'</tr></thead><tbody>';
		postData.Accion="Codigos Cliente";
		$.post(Cupomida.JSONroot,postData,function(data){
			if(data.Success){
				if(data.Result.length>0){
					var c=data.Result,
						rep,
						impreso="",
						cobrado="",
						valido="";
					for(var r in c){
						if(c[r].id_usuario===null) impreso="Sin imprimir";
						else impreso="Impreso por: "+c[r].first_name;
						if(c[r].cobrado==="0"){
							cobrado='<a href="javascript:void(0);" '+
								'class="validar-codigo" '+
								'data-id="'+c[r].id_codigo+'">Validar</a>';
						}else cobrado="Usado";
						valido="Válido hace "+
							Cupomida.calcularDias(c[r].valido_desde)+
							" días, hasta "+
							Cupomida.calcularDias(c[r].valido_hasta)+
							" días más.";
						rep={
							_ID:c[r].id_codigo,
							_Cupon:c[r].titulo,
							_Serie:c[r].serie,
							_Validez:valido,
							_Impreso:impreso,
							_Fecha:c[r].emision,
							_Cobrado:cobrado,
							_Ver:'<a href="javascript:void(0);" '+
								'class="ver-cupon" '+
								'data-id="'+c[r].id_cupon+'">Ver Cupón</a>'
						};
						Codigos+=Cupomida.tpl.codigoCodigos.replace(
							/_ID|_Cupon|_Serie|_Validez|_Impreso|_Fecha|_Cobrado|_Ver/g,
							function(m){return rep[m];}
						);
					}
				}else{
					Codigos+='<tr><td colspan="7"><p>No hay códigos disponibles.</p></td></tr>';
				}
				Codigos+='</tbody></table>';
				$("#Codigos").html(Codigos);
				Cupomida.setClicks.Codigos();
				if(typeof(callback)=="function") callback();
			}else{
				Cupomida.errorCode(data.errorCode,function(){
					Cupomida.mensajeError("Error","No se pudieron cargar los cupones impresos.");
				});
			}
		},"json").fail(function(jqXHR, textStatus){
			Cupomida.serverResponse(jqXHR, textStatus);
		});
	},
	paginarCupones:function(Total){
		var Pages=0,
			Content="";
		if(Total % Cupomida.sizes.query) Pages=(Math.ceil(Total/Cupomida.sizes.query));
		else Pages=(Total/Cupomida.sizes.query);
		for(var p=1; p<=Pages; p++) Content+=Cupomida.tpl.btnPaginacion.replace("_Numero",p);
		$("#paginacion .btn-group").html(Content);
		$("#paginacion").removeClass("hidden");
		$(".btn-paginacion").off("click").on("click",function(e){
			$(".btn-paginacion").removeClass("active");
			$(e.currentTarget).addClass("active");
			Cupomida.view.Pagina=parseInt($(e.currentTarget).html(),10)-1;
			Cupomida.poblarCupones(Cupomida.view,function(){
				Cupomida.setClicks.Galeria();
			});
		});
	},
	primeraPagina:function(){
		$(".btn-paginacion")
			.removeClass("active")
			.first()
				.addClass("active");
	},
	pageAfter:function(callback){
		switch(Cupomida.after){
			case "#Estadísticas":
				Cupomida.pageEstadisticas(function(){
					if(typeof(callback)=="function") callback();
				});
			break;
			case "#Perfil":			
				Cupomida.pagePerfil(function(){
					Cupomida.setClicks.Perfil();
					if(typeof(callback)=="function") callback();
				});
			break;
			case "#Panel_Control":
				Cupomida.pagePanelControl(function(){
					if(typeof(callback)=="function") callback();
				});
			break;
			case "#Cupon/Nuevo":
				Cupomida.pageNuevoCupon(function(){
					if(typeof(callback)=="function") callback();
				});
			break;
			case "#Códigos":
				Cupomida.pageCodigos(function(){
					if(typeof(callback)=="function") callback();
				});
			break;
			default:
				Cupomida.pageInicio(function(){
					Cupomida.setClicks.Galeria();
					if(typeof(callback)=="function") callback();
				});
			break;
		}
		Cupomida.after=false;
	},
	pageInicio:function(callback){
		Cupomida.reset.changePage();
		Cupomida.history.push(Cupomida.titulo,"#");
		Cupomida.AnalyticsSend("/","Inicio");
		Cupomida.poblarCupones({
			Accion:"Cupones Disponibles",
			Todos:true
		},function(){
			Cupomida.primeraPagina();
			if(typeof(callback)=="function") callback();
		});
	},
	pageLogin:function(callback){
		Cupomida.reset.changePage();
		Cupomida.history.push(Cupomida.titulo+" - Login","#Login");
		Cupomida.AnalyticsSend("/#Login","Login");
		var rep,content="";
		if(Cupomida.user){
			rep={
				_Email:Cupomida.user.Email,
				_GoogleID:Cupomida.Google.ID
			};
		}else rep={
			_Email:"",
			_GoogleID:Cupomida.Google.ID
		};
		content=Cupomida.tpl.login.replace(
			/_Email|_GoogleID/g,
			function(m){return rep[m];
		});
		$(Cupomida.select.content).html(content);
		//Cupomida.Google.init();
		$(".section-breadcrumbs")
			.html(Cupomida.tpl.cabeceraLogin)
			.removeClass("hidden");
		if(typeof(callback)=="function") callback();
	},
	pageRegistro:function(callback){
		Cupomida.reset.changePage();
		Cupomida.history.push(Cupomida.titulo+" - Registro","#Registro");
		Cupomida.AnalyticsSend("/#Registro","Registro");
		var rep,content="";
		if(Cupomida.user){
			rep={
				_Email:Cupomida.user.Email,
				_GoogleID:Cupomida.Google.ID
			};
		}else{
			rep={
				_Email:"",
				_GoogleID:Cupomida.Google.ID
			};
		}
		content=Cupomida.tpl.registro.replace(
			/_Email|_GoogleID/g,
			function(m){return rep[m];
		});
		$(Cupomida.select.content).html(content);
		$(".btn-twitter-login").addClass("hidden");
		$(".section-breadcrumbs").html(Cupomida.tpl.cabeceraRegistro).removeClass("hidden");
		if(typeof(callback)=="function") callback();
	},
	pagePerfil:function(callback){
		Cupomida.reset.changePage();
		Cupomida.history.push(Cupomida.titulo+" - Perfil","#Perfil");
		Cupomida.AnalyticsSend("/#Perfil","Pefil");
		if(Cupomida.user){
			Cupomida.poblarPerfil.Datos(function(){
				Cupomida.poblarPerfil.Genero();
				Cupomida.poblarPerfil.Imagen();
				Cupomida.poblarPerfil.Categorias(function(){
					if(typeof(callback)=="function") callback();
				});
			});
			Cupomida.poblarPerfil.Cupones({});
		}else{
			Cupomida.pageInicio(function(){
				Cupomida.setClicks.Galeria();
				Cupomida.preguntarLogin();
			});
		}
	},
	pageValidar:function(ID,callback){
		Cupomida.reset.changePage();
		Cupomida.AnalyticsSend("/#Validar","Validar Cuenta");
		$.post(Cupomida.JSONroot,{
			Accion:"Usuario por Key",
			ID:ID
		},function(data){
			if(data.Success){
				if(data.Result){
					Cupomida.user=data.Result;
					var rep={
						_Nombre:data.Result.first_name,
						_Apellido:data.Result.last_name,
						_Correo:data.Result.email
					};
					var content=Cupomida.tpl.perfil.replace(
						/_Nombre|_Apellido|_Correo/g,
						function(m){return rep[m];
					});
					$(Cupomida.select.content).html(content);
					if(typeof(callback)=="function") callback();
				}else{
					Cupomida.mensajeError("Error","Hubo un error con la clave.");
				}
			}else{
				Cupomida.errorCode(data.errorCode,function(){
					Cupomida.mensajeError("Error","Hubo un error con el login.");
				});
			}
		},"json").fail(function(jqXHR, textStatus){
			Cupomida.serverResponse(jqXHR, textStatus);
		});
	},
	pageCupon:function(ID,callback){
		Cupomida.reset.changePage();
		Cupomida.history.push(Cupomida.titulo+" - Cupón","#Cupon/"+ID);
		$.post(Cupomida.JSONroot,{
			Accion:"Cupon por ID",
			ID:ID
		},function(data){
			if(data.Success){
				if(data.Result.length>0 && data.Result[0].id_cupon!==null){
					var c=data.Result[0],
						precio=Cupomida.formatoNumero(c.precio*((c.descuento/100)+1)),
						impresos=(parseInt(c.cantidad,10)-parseInt(c.restantes,10)),
						mark=false;
					Cupomida.cupon=c;
					Cupomida.AnalyticsSend("/#Cupon"+ID,"Cupon - "+c.titulo);
					var CondicionesContent="";
					if(Cupomida.cupon.condiciones!==""){
						var Condiciones=Cupomida.unserialize(Cupomida.cupon.condiciones);
						for(var C in Condiciones){
							CondicionesContent+=Cupomida.tpl.cuponCondiciones.replace("_Condicion",Condiciones[C]);
						}
					}
					var Categorias=Cupomida.tpl.cuponComida.replace("_Titulo",c.titulo_categoria);
					var rep={
						_ID:c.id_cupon,
						_Ancho:"100%",
						_Alto:"auto",
						_Titulo:c.titulo,
						_Class:""
					};
					var ImagenCupon=Cupomida.tpl.imagenEspera.replace(
						/_ID|_Ancho|_Alto|_Titulo|_Class/g,
						function(m){return rep[m];}
					);
					rep={
						_ID:c.id_cliente,
						_Ancho:"100%",
						_Alto:"auto",
						_Titulo:c.nombre_cliente,
						_Class:" center-block"
					};
					var ImagenCliente=Cupomida.tpl.imagenEspera.replace(
						/_ID|_Ancho|_Alto|_Titulo|_Class/g,
						function(m){return rep[m];}
					);
					var distancia="";
					if(typeof(Cupomida.user.latitude)!=="undefined"){
						distancia=" - A ";
							distancia+=Cupomida.calcularDistancia(
								Cupomida.user.latitude,
								Cupomida.user.longitude,
								c.latitud,
								c.longitud
							).toFixed(2);
						distancia+="Km de Usted.";
					}
					rep={
						_IdCupon:c.id_cupon,
						_Titulo:c.titulo,
						_Descripcion:c.descripcion,
						_Detalle:c.detalle,
						_Categoria:"",
						_ImagenCupon:ImagenCupon,
						_Impresos:'Impresos: '+impresos+' cupones.',
						_Termino:'Finaliza en: '+Cupomida.calcularDias(c.valido_hasta)+' dias',
						_Precio:Cupomida.formatoNumero(c.precio),
						_Descuento:"$"+precio,
						_btnDenunciar:'<a href="javascript:void(0);" class="glyphicon glyphicon-flag denuncia" title="Denunciar"></a>',
						_Hora_Canjeo:'Cupón canjeable desde las '+c.hora_inicio+' hasta las '+c.hora_fin,
						_btnImprimir:Cupomida.tpl.btnImprimir,
						_Horario:"",
						_Semana:Cupomida.tpl.cuponSemana,
						_FBLikes:Cupomida.tpl.likes.replace("_FBLink",Cupomida.root+'Cupon/'+c.id_cupon),
						_FBComments:Cupomida.tpl.comentarios.replace("_FBLink",Cupomida.root+'Cupon/'+c.id_cupon),
						_TWShare:Cupomida.tpl.twitterShare,
						_ValoracionesCupon:"<span> ("+c.valoraciones_cupon+" votos)</span>",
						_IdCliente:c.id_cliente,
						_NombreCliente:c.nombre_cliente,
						_NombreIntCliente:c.nombre_cliente,
						_Condiciones:CondicionesContent,
						_Informacion:c.informacion,
						_ImagenCliente:ImagenCliente,
						_ValoracionesCliente:"<span> ("+c.valoraciones_cliente+" votos)</span>",
						_Telefono:c.telefono,
						_Email:'<a href="mailto:'+c.email+'">'+c.email+'</a>',
						_Atencion:c.horario,
						_Comidas:Categorias,
						_Comuna:c.direccion,
						_Distancia:distancia,
						_Mapa:Cupomida.tpl.cuponMapa
					};
					var content=Cupomida.tpl.cuponDetalle.replace(
						Cupomida.regularExpressions.Cupon,
						function(m){return rep[m];}
					);
					$(Cupomida.select.content).html(content);
					Cupomida.setDiasHabiles(c.dias_habiles);
					Cupomida.reset.stars();
					if(typeof(FB)!='undefined') FB.XFBML.parse();
					var center=new google.maps.LatLng(
							parseFloat(Cupomida.cupon.latitud),
							parseFloat(Cupomida.cupon.longitud)
						);
					Cupomida.showMap({
						divId:"googleMap",
						Titulo:c.titulo,
						mapProp:{
							center:center,
							zoom:15,
							mapTypeId:google.maps.MapTypeId.ROADMAP
						}
					},function(map){
						mark=new google.maps.Marker({
							position:center,
							map:map,
							title:c.titulo
						});
						Cupomida.setImages(function(type,img){
							if(type=="Imagen Cupon"){
								Cupomida.setMarkIcon(mark,img);
							}
						});
						if(typeof(callback)=="function") callback();
					});
				}else Cupomida.emptyResult();
			}else{
				Cupomida.errorCode(data.errorCode,function(){
					Cupomida.mensajeError("Error","No se pudieron cargar los cupones de esta sección.");
				});
			}
		},"json").fail(function(jqXHR, textStatus){
			Cupomida.serverResponse(jqXHR, textStatus);
		});
	},
	pageNuevoCupon:function(callback){
		Cupomida.reset.changePage();
		Cupomida.history.push(Cupomida.titulo+" - Nuevo Cupón","#Cupon/Nuevo");
		Cupomida.AnalyticsSend("/#Cupon/Nuevo","Nuevo Cupón");
		var Condiciones=Cupomida.tpl.cuponCondiciones.replace(
				"_Condicion",
				Cupomida.tpl.cuponNuevaCondicion
			)+
			Cupomida.tpl.cuponCondiciones.replace(
				"_Condicion",
				'<button class="btn btn-red" type="button" id="btnNuevaCondicion">Nueva Condicion</button>'
			);
		var Comidas=Cupomida.tpl.cuponComida.replace("_Titulo",'<input type="text" id="nuevaCategoria"');
		var content="";
		var rep={
			_IdCupon:0,
			_Titulo:'<input type="text" id="nuevoTitulo" placeholder="Titulo del cupón.">',
			_Descripcion:'<textarea id="nuevaDescripcion" placeholder="Descripción del cupón."></textarea>',
			_Detalle:'<textarea id="nuevoDetalle" placeholder="Detalle"></textarea>',
			_Categoria:'<input type="text" id="nuevaCategoria" placeholder="Categoria">'+
				'<textarea id="nuevaDescripcionCategoria" placeholder="Descripción de la categoría del cupón."></textarea>'+
				'<input type="hidden" id="nuevoIdCategoria" value="0">',
			_ImagenCupon:'<input type="file" id="nuevaFileImagenCupon">'+
				'<canvas id="nuevaImagenCupon"></canvas>',
			_Impresos:'Cantidad de cupones: <input type="number" id="nuevaCantidad" value="1">',
			_Termino:'Válido desde: <input type="text" id="nuevoInicio"><br/>'+
				'Válido hasta: <input type="text" id="nuevoTermino">',
			_Precio:'Precio: <input type="number" id="nuevoValor">',
			_Descuento:'Descuento (%): <input type="number" id="nuevoDescuento">',
			_btnDenunciar:"",
			_Hora_Canjeo:"",
			_btnImprimir:Cupomida.tpl.btnGuardar,
			_Informacion:'<textarea id="nuevaInformacion" placeholder="Información del producto."></textarea>',
			_Horario:'<input type="text" id="nuevoHorario" placeholder="Desayuno, Almuerzo, Cena, etc...">'+
				'<input type="hidden" id="nuevoIdHorario" value="0">'+
				'<input type="text" id="nuevaHoraInicio" placeholder="Canjeable desde las">'+
				'<input type="text" id="nuevaHoraFin" placeholder="Canjeable hasta las">',
			_Semana:Cupomida.tpl.cuponSemana,
			_FBLikes:"",
			_FBComments:"",
			_TWShare:"",
			_ValoracionesCupon:"",
			_IdCliente:0,
			_NombreCliente:'<span id="nuevoNombreIntCliente"></span>',
			_NombreIntCliente:'<input type="text" id="nuevoNombreCliente" placeholder="Nombre de la empresa.">'+
				'<input type="hidden" id="nuevoIdCliente" value="0">',
			_Condiciones:Condiciones,
			_ImagenCliente:'<input type="file" id="nuevaFileImagenCliente">'+
				'<canvas id="nuevaImagenCliente"></canvas>',
			_ValoracionesCliente:"",
			_Telefono:'<input type="telephone" id="nuevoTelefono" placeholder="Teléfono">',
			_Email:'<input type="email" id="nuevoCorreo" placeholder="Correo electrónico">',
			_Atencion:'<input type="text" id="nuevaAtencion" placeholder="Horario de atención de la empresa.">',
			_Comidas:Comidas,
			_Comuna:'<input type="text" id="nuevaComuna" placeholder="Comuna">',
			_Distancia:"",
			_Mapa:Cupomida.tpl.cuponMapa+
				'<input type="hidden" id="nuevaIdUbicacion" value="0">'+
				'<input type="hidden" id="nuevaLatitud" value="0">'+
				'<input type="hidden" id="nuevaLongitud" value="0">'
		};
		content+=Cupomida.tpl.cuponDetalle.replace(
			Cupomida.regularExpressions.Cupon,
			function(m){return rep[m];}
		);
		$(Cupomida.select.content).html(content);
		Cupomida.reset.priceWas();
		if(typeof(FB)!='undefined') FB.XFBML.parse();
		var center=new google.maps.LatLng(
				Cupomida.centro.lat,
				Cupomida.centro.lon
			);
		Cupomida.showMap({
			divId:"googleMap",
			mapProp:{
				center:center,
				zoom:15,
				mapTypeId:google.maps.MapTypeId.ROADMAP
			}
		},function(map){
			Cupomida.markers.center = new google.maps.Marker({
				position:center,
				map:map,
				title:"Santiago de Chile"
			});
			Cupomida.setClicks.NuevoCupon(map,function(){
				if(typeof(callback)!="undefined") callback();
			});
		});
	},
	pageEditarCupon:function(ID,callback){
		Cupomida.reset.changePage();
		window.location.hash="#Editar/"+ID;
		Cupomida.history.push(Cupomida.titulo+" - Editar Cupón","#Editar/"+ID);
		Cupomida.AnalyticsSend("/#Editar/"+ID,"Editar Cupón");
		$.post(Cupomida.JSONroot,{
			Accion:"Cupon por ID",
			ID:ID
		},function(data){
			if(data.Success){
				if(data.Result.length>0){
					var c=data.Result[0],
					CondicionesContent="";
					if(c.condiciones!==""){
						var Condiciones=Cupomida.unserialize(c.condiciones);
						for(var C in Condiciones){
							CondicionesContent+=Cupomida.tpl.cuponCondiciones.replace(
								"_Condicion",
								Cupomida.tpl.cuponNuevaCondicion.replace("_Condicion",Condiciones[C])
							);
						}
					}
					CondicionesContent+=Cupomida.tpl.cuponCondiciones.replace(
						"_Condicion",
						Cupomida.tpl.cuponNuevaCondicion.replace("_Condicion","")
					)+
					Cupomida.tpl.cuponCondiciones.replace(
						"_Condicion",
						'<button class="btn btn-red" type="button" id="btnNuevaCondicion">Nueva Condicion</button>'
					);
					var Comidas=Cupomida.tpl.cuponComida.replace("_Titulo",'<input type="text" id="nuevaCategoria"'),
					content="";
					var rep={
						_IdCupon:c.id_cupon,
						_Titulo:'<input type="text" id="nuevoTitulo" value="'+c.titulo+'" placeholder="Titulo del cupón.">',
						_Descripcion:'<textarea id="nuevaDescripcion" placeholder="Descripción del cupón.">'+c.descripcion+'</textarea>',
						_Detalle:'<textarea id="nuevoDetalle" placeholder="Detalle">'+c.detalle+'</textarea>',
						_Categoria:'<input type="text" id="nuevaCategoria" value="'+c.titulo_categoria+'" placeholder="Categoria">'+
							'<textarea id="nuevaDescripcionCategoria" placeholder="Descripción de la categoría del cupón.">'+c.descripcion_categoria+'</textarea>'+
							'<input type="hidden" value="'+c.id_categoria+'" id="nuevoIdCategoria" value="0">',
						_ImagenCupon:'<input type="file" id="nuevaFileImagenCupon">'+
							'<canvas id="nuevaImagenCupon"></canvas>',
						_Impresos:'Cantidad de cupones: <input type="number" id="nuevaCantidad" value="'+c.cantidad+'">',
						_Termino:'Válido desde: <input type="text" id="nuevoInicio" value="'+c.valido_desde+'"><br/>'+
							'Válido hasta: <input type="text" id="nuevoTermino" value="'+c.valido_hasta+'">',
						_Precio:'Precio: <input type="number" id="nuevoValor" value="'+c.precio+'">',
						_Descuento:'Descuento (%): <input type="number" id="nuevoDescuento" value="'+c.descuento+'">',
						_btnDenunciar:"",
						_Hora_Canjeo:"",
						_btnImprimir:Cupomida.tpl.btnGuardar,
						_Informacion:'<textarea id="nuevaInformacion" placeholder="Información del producto.">'+c.informacion+'</textarea>',
						_Horario:'<input type="text" id="nuevoHorario" value="'+c.titulo_horario+'" placeholder="Desayuno, Almuerzo, Cena, etc..." >'+
							'<input type="hidden" id="nuevoIdHorario" value="'+c.id_horario+'">'+
							'<input type="text" id="nuevaHoraInicio" value="'+c.hora_inicio+'" placeholder="Canjeable desde las">'+
							'<input type="text" id="nuevaHoraFin" value="'+c.hora_fin+'" placeholder="Canjeable hasta las">',
						_Semana:Cupomida.tpl.cuponSemana,
						_FBLikes:"",
						_FBComments:"",
						_TWShare:"",
						_ValoracionesCupon:"",
						_IdCliente:c.id_cliente,
						_NombreCliente:'<span id="nuevoNombreIntCliente">'+c.nombre_cliente+'</span>',
						_NombreIntCliente:'<input type="text" id="nuevoNombreCliente" value="'+c.nombre_cliente+'" placeholder="Nombre de la empresa.">'+
							'<input type="hidden" id="nuevoIdCliente" value="'+c.id_cliente+'">',
						_Condiciones:CondicionesContent,
						_ImagenCliente:'<input type="file" id="nuevaFileImagenCliente">'+
							'<canvas id="nuevaImagenCliente"></canvas>',
						_ValoracionesCliente:"",
						_Telefono:'<input type="telephone" id="nuevoTelefono" value="'+c.nombre_cliente+'" placeholder="Teléfono">',
						_Email:'<input type="email" id="nuevoCorreo" value="'+c.email+'" placeholder="Correo electrónico">',
						_Atencion:'<input type="text" id="nuevaAtencion" value="'+c.horario+'" placeholder="Horario de atención de la empresa.">',
						_Comidas:Comidas,
						_Comuna:'<input type="text" id="nuevaComuna" value="'+c.direccion+'" placeholder="Comuna">',
						_Distancia:"",
						_Mapa:Cupomida.tpl.cuponMapa+
							'<input type="hidden" id="nuevaIdUbicacion" value="'+c.id_ubicacion+'">'+
							'<input type="hidden" id="nuevaLatitud" value="'+c.latitud+'">'+
							'<input type="hidden" id="nuevaLongitud" value="'+c.longitud+'">'
					};
					content+=Cupomida.tpl.cuponDetalle.replace(
						Cupomida.regularExpressions.Cupon,
						function(m){return rep[m];}
					);
					$(Cupomida.select.content).html(content);
					c.dias_habiles=c.dias_habiles.split(",");
					for(var i in c.dias_habiles){
						if(c.dias_habiles[i]!=="N"){
							$($(Cupomida.select.semanaHabil)[i]).addClass("btn-red");
						}
					}
					Cupomida.reset.priceWas();
					Cupomida.idImagenACanvas("Imagen Cupon",c.id_cupon,$("#nuevaImagenCupon")[0],function(base64){
						Cupomida.newCupon.image=base64;
					});
					Cupomida.idImagenACanvas("Imagen Cliente",c.id_cliente,$("#nuevaImagenCliente")[0],function(base64){
						Cupomida.newCupon.imageCliente=base64;
					});
					if(typeof(FB)!='undefined') FB.XFBML.parse();
					var center=new google.maps.LatLng(
							parseFloat(c.latitud),
							parseFloat(c.longitud)
						);
					Cupomida.showMap({
						divId:"googleMap",
						Titulo:c.direccion,
						mapProp:{
							center:center,
							zoom:15,
							mapTypeId:google.maps.MapTypeId.ROADMAP
						}
					},function(map){
						Cupomida.markers.center=new google.maps.Marker({
							position:center,
							map:map,
							title:c.titulo
						});
						Cupomida.setClicks.NuevoCupon(map,function(){
							if(typeof(callback)=="function") callback();
						});
					});
				}
			}
		});
	},
	pageTimeLine:function(){
		Cupomida.reset.changePage();
		Cupomida.history.push(Cupomida.titulo+" - TimeLine","#TimeLine");
		Cupomida.AnalyticsSend("/#TimeLine","TimeLine");
		var content=Cupomida.tpl.timeLine;
		$(Cupomida.select.content).html(content);
		Cupomida.poblarTimeLine(false,function(){
			Cupomida.setClicks.TimeLine();
		});
	},
	pageTutorial:{
		iniciado:false,
		uno:function(){
			Cupomida.pageTutorial.iniciado=true;
			$(Cupomida.select.body).prepend('<div class="glyphicon glyphicon-arrow-up arrow uno floating"></div>');
			Cupomida.mensajeTutorial("Uno:","Inicie una sessión.");
		},
		dos:function(){
			$(".glyphicon.glyphicon-arrow-up.arrow.uno").remove();
			Cupomida.mensajeTutorial("Uno:","Cree una cuenta con sus datos de Facebook, Twitter o Correo electrónico.");
			//$(Cupomida.select.body).prepend('<div class="glyphicon glyphicon-arrow-right arrow dos floating"></div>');
			$(Cupomida.select.body).prepend('<div class="glyphicon glyphicon-arrow-right arrow tres floating"></div>');
			$(Cupomida.select.body).prepend('<div class="glyphicon glyphicon-arrow-left arrow cuatro floating"></div>');
			$(Cupomida.select.body).prepend('<div class="glyphicon glyphicon-arrow-left arrow cinco floating"></div>');
		}
	},
	pageCliente:function(ID,callback){
		Cupomida.reset.changePage();
		Cupomida.history.push(Cupomida.titulo+" - Cliente","#Cliente/"+ID);
		Cupomida.AnalyticsSend("/#Cliente/"+ID,"Cliente "+ID);
		Cupomida.poblarCupones({
			Accion:"Cupones por Cliente",
			ID:ID
		},function(){
			if(typeof(callback)=="function") callback();
		});
	},
	pageCategoria:function(ID,callback){
		Cupomida.reset.changePage();
		Cupomida.history.push(Cupomida.titulo+" - Categoría","#Categoría/"+ID);
		Cupomida.AnalyticsSend("/#Categoria/"+ID,"Categoria "+ID);
		Cupomida.poblarCupones({
			Accion:"Cupones por Categoria",
			ID:ID
		},function(){
			if(typeof(callback)=="function") callback();
		});
	},
	pageUbicacion:function(ID,callback){
		Cupomida.reset.changePage();
		Cupomida.history.push(Cupomida.titulo+" - Ubicación","#Ubicación/"+ID);
		Cupomida.AnalyticsSend("/#Ubicación/"+ID,"Ubicación "+ID);
		Cupomida.poblarCupones({
			Accion:"Cupones por Ubicacion",
			ID:ID
		},function(){
			if(typeof(callback)=="function") callback();
		});
	},
	pageEstadisticas:function(callback){
		Cupomida.reset.changePage();
		Cupomida.history.push(Cupomida.titulo+" - Estadísticas","#Estadísticas");
		Cupomida.AnalyticsSend("/#Estadísticas","Estadísticas");
		$(Cupomida.select.content).html(Cupomida.tpl.estadisticas);
		Cupomida.setClicks.Estadisticas.Pagina();
		Cupomida.poblarEstadisticas.Impresos({Rango:"Semana"});
		Cupomida.poblarEstadisticas.Vistos({Rango:"Semana"});
		Cupomida.poblarEstadisticas.Likes({Rango:"Semana"});
		Cupomida.poblarEstadisticas.Visitas(function(){
			if(typeof(callback)=="function") callback();
		});
	},
	pagePanelControl:function(callback){
		Cupomida.reset.changePage();
		Cupomida.history.push(Cupomida.titulo+" - Panel de Control","#Panel_Control");
		Cupomida.AnalyticsSend("/#Panel_Control","Panel de Control");
		if(Cupomida.user){
			$(Cupomida.select.content).html(Cupomida.tpl.panelControl.replace("_Contenido",""));
			switch(parseInt(Cupomida.user.administrador,10)){
				case 1:
					Cupomida.poblarPanelControl.Usuarios(function(){
						Cupomida.poblarPanelControl.Clientes(function(){
							Cupomida.poblarPanelControl.Pendientes(function(){
								Cupomida.setImages();
							});
						});
					});
				case 2:
					Cupomida.poblarPanelControl.Cupones();
					Cupomida.poblarPanelControl.Denuncias();
					Cupomida.setImages();
				break;
			}
			if(typeof(callback)=="function") callback();
		}else Cupomida.preguntarLogin();
	},
	pageBuscar:function(termino,callback){
		Cupomida.reset.changePage();
		Cupomida.history.push(Cupomida.titulo+" - Búsqueda","#Buscar/"+termino);
		Cupomida.AnalyticsSend("/#Buscar",termino);
		Cupomida.poblarCupones({
			Accion:"Cupon Buscar",
			Termino:termino
		},function(){
			Cupomida.setClicks.Galeria();
			if(typeof(callback)=="function") callback();
		});
	},
	pageMapa:function(){
		Cupomida.reset.changePage();
		Cupomida.history.push(Cupomida.titulo+" - Mapa de Cupones","#Mapa");
		Cupomida.AnalyticsSend("/#Mapa","Mapa");
		Cupomida.poblarMapa.Mapa(function(){
		});
	},
	pageCodigos:function(){
		Cupomida.reset.changePage();
		Cupomida.history.push(Cupomida.titulo+" - Códigos","#Códigos");
		Cupomida.AnalyticsSend("/#Codigos","Códigos");
		$(Cupomida.select.content).html(Cupomida.tpl.codigos);
		Cupomida.poblarCodigos({});
	},
	pageEspera:function(){
		document.title=Cupomida.titulo+" - Cargando";
		$(Cupomida.select.content).html(Cupomida.tpl.espera);
	},
	menuLogin:function(){
		$(Cupomida.select.menu).html("");
		$(Cupomida.select.menuLogin).html(Cupomida.tpl.menuTime);
		if(Cupomida.user){
			$(Cupomida.select.menuLogin).append(Cupomida.tpl.menuPerfil);
			if(parseInt(Cupomida.user.administrador,10)>0){
				$(Cupomida.select.menu).append(Cupomida.tpl.menuAdmin);
				$(Cupomida.select.menuAdmin).append(Cupomida.tpl.menuNuevoCupon);
				$(Cupomida.select.menuAdmin).append(Cupomida.tpl.menuEstadisticas);
				$(Cupomida.select.menuAdmin).append(Cupomida.tpl.menuPanelControl);
				$(Cupomida.select.menuAdmin).append(Cupomida.tpl.menuCodigos);
			}
		}else{
			$(Cupomida.select.menuLogin).append(Cupomida.tpl.menuLogin);
		}
		$(Cupomida.select.menu).append(Cupomida.tpl.menuMapa);
		$(Cupomida.select.menu).append(Cupomida.tpl.menuTimeLine);
		$(Cupomida.select.menu).append(Cupomida.tpl.menuBuscar);
		Cupomida.setClicks.Menu();
		Cupomida.initTime();
	},
	guardarCupon:function(callback){
		var Id_Cupon=$("#nuevoIdCupon").val().trim(),
			Titulo=$("#nuevoTitulo").val().trim(),
			Detalle=$("#nuevoDetalle").val().trim(),
			Descripcion=$("#nuevaDescripcion").val().trim(),
			Imagen_Cupon=Cupomida.newCupon.image,
			Cantidad=$("#nuevaCantidad").val().trim(),
			Dias_Habiles="",
			Dias_Habiles_Val=true,
			Valor=$("#nuevoValor").val().trim(),
			Descuento=$("#nuevoDescuento").val().trim(),
			Informacion=$("#nuevaInformacion").val().trim(),
			Condiciones=[],
			ID_Horario=$("#nuevoIdHorario").val().trim(),
			Nombre_Horario=$("#nuevoHorario").val().trim(),
			Hora_Inicio=$("#nuevaHoraInicio").val().trim(),
			Hora_Fin=$("#nuevaHoraFin").val().trim(),
			ID_Cliente=$("#nuevoIdCliente").val(),
			Nombre_Cliente=$("#nuevoNombreCliente").val().trim(),
			Imagen_Cliente=Cupomida.newCupon.imageCliente,
			Telefono=$("#nuevoTelefono").val().trim(),
			Email=$("#nuevoCorreo").val().trim(),
			ID_Categoria=$("#nuevoIdCategoria").val(),
			Nombre_Categoria=$("#nuevaCategoria").val().trim(),
			Descripcion_Categoria=$("#nuevaDescripcionCategoria").val().trim(),
			Atencion=$("#nuevaAtencion").val().trim(),
			Valido_Desde=$("#nuevoInicio").val().trim(),
			Valido_Hasta=$("#nuevoTermino").val().trim(),
			Id_Ubicacion=$("#nuevaIdUbicacion").val().trim(),
			Direccion=$("#nuevaComuna").val().trim(),
			Latitud=$("#nuevaLatitud").val().trim(),
			Longitud=$("#nuevaLongitud").val().trim();

		$(".nuevaCondicion").each(function(i,e){
			Condiciones.push($(e).val());
		});
			
		$(Cupomida.select.semanaHabil).each(function(i,e){
			if($(e).hasClass("btn-red")){
				Dias_Habiles+=$(e).html()+",";
				Dias_Habiles_Val=false;
			}else Dias_Habiles+="N,";
		});
		Dias_Habiles=Dias_Habiles.replace(/,\s*$/,"");
		var falta='<ul>';
			if(Titulo==="") falta+='<li>Título del cupón.</li>';
			if(Detalle==="") falta+='<li>Detalle del cupón.</li>';
			if(Descripcion==="") falta+='<li>Descripción del cupón.</li>';
			if(Valido_Desde==="") falta+='<li>Fecha de inicio de la promoción.</li>';
			if(Valido_Hasta==="") falta+='<li>Fecha de finalización de la promoción.</li>';
			if(typeof(Imagen_Cupon)=="undefined") falta+='<li>Imagen del cupón.</li>';
			if(Cantidad==="") falta+='<li>Cantidad de cupones.</li>';
			if(parseInt(Cantidad,10)<0){
				falta+='<li>La cantidad de cupones debe ser mayor a 0.</li>';
			}
			if(Valor==="") falta+='<li>Precio del cupón.</li>';
			if(Informacion==="") falta+='<li>Información del cupón.</li>';
			if(Descuento==="") falta+='<li>Descuento del cupón.</li>';
			if(Nombre_Categoria==="") falta+='<li>Categoria del cupón.</li>';
			if(Descripcion_Categoria==="") falta+='<li>Descripción de la categoria del cupón.</li>';
			if(Nombre_Cliente==="") falta+='<li>Empresa.</li>';
			if(Telefono==="") falta+='<li>Teléfono de la empresa.</li>';
			if(Email==="") falta+='<li>Correo de la empresa.</li>';
			if(!Cupomida.regularExpressions.Email.test(Email)){
				falta+='<li>El correo ingresado parece no ser válido.</li>';
			}
			if(Atencion==="") falta+='<li>Horario de atención de la empresa.</li>';
			if(Nombre_Horario==="") falta+='<li>Nombre del horario de atención.</li>';
			if(typeof(Imagen_Cliente)=="undefined") falta+='<li>Imagen de la empresa.</li>';
			if(Direccion==="") falta+='<li>Dirección de la empresa.</li>';
			if(Dias_Habiles_Val) falta+='<li>Debe seleccionar al menos un día hábil.</li>';
		falta+='</ul>';
		if(falta != '<ul></ul>'){
			Cupomida.mensajeError("Faltan datos:",falta);
		}else{
			Cupomida.nuevoCupon({
				Accion:"Nuevo Cupon",
				Id_Cupon:Id_Cupon,
				Titulo:Titulo,
				Descripcion:Descripcion,
				Detalle:Detalle,
				ID_Cliente:ID_Cliente,
				Informacion:Informacion,
				Valido_Desde:Valido_Desde,
				Valido_Hasta:Valido_Hasta,
				Dias_Habiles:Dias_Habiles,
				Cantidad:Cantidad,
				Precio:Valor,
				Descuento:Descuento,
				Imagen:Imagen_Cupon,
				Condiciones:Condiciones,
				ID_Horario:ID_Horario,
				Nombre_Horario:Nombre_Horario,
				Hora_Inicio:Hora_Inicio,
				Hora_Fin:Hora_Fin,
				Nombre_Cliente:Nombre_Cliente,
				Imagen_Cliente:Imagen_Cliente,
				Email:Email,
				Telefono:Telefono,
				Atencion:Atencion,
				ID_Categoria:ID_Categoria,
				Nombre_Categoria:Nombre_Categoria,
				Descripcion_Categoria:Descripcion_Categoria,
				ID_Ubicacion:Id_Ubicacion,
				Direccion_Ubicacion:Direccion,
				Latitud:Latitud,
				Longitud:Longitud
			},function(Response){
				switch(Response.Stock){
					case "0":
						Cupomida.modal.show({
							title:"Sin saldo",
							body:'<p class="lead">Su local no tiene saldo para crear este cupón, '+
								'por favor, contáctenos para adquirir un nuevo plan.<br/>'+
								'Su cupón fue guardado pero no se mostrará hasta regularizar la situación.</p>',
							buttons:[
								{
									id:"guardadoOK",
									text:"Aceptar",
									class:"btn-blue"
								}
							]
						},function(id){
							switch(id){
								case "ready":break;
								default:
									Cupomida.modal.hide();
									Cupomida.pageInicio();
								break;
							}
						});
					break;
					case "1":
						if(typeof(callback)=="function") callback();
					break;
				}
			});
		}
	},
	guardarDenuncia:function(ID){
		$.post(Cupomida.JSONroot,{
			Accion:"Denunciar",
			Cupon:Cupomida.cupon.id_cupon,
			ID:ID
		},function(data){
			if(data.Success){
				Cupomida.mensajeInfo("Denunciado","Su denuncia fue guardada correctamente.");
			}else{
				Cupomida.errorCode(data.errorCode,function(){
					Cupomida.mensajeError("Error","Ocurrió un error al guardar su denuncia.");
				});
			}
		},"json").fail(function(jqXHR, textStatus){
			Cupomida.serverResponse(jqXHR, textStatus);
		});
	},
	borrarCupon:function(id_cupon,callback){
		Cupomida.modal.show({
			title:"Confirme",
			body:'<p class="lead">Por favor confirme que desea eliminar este cupón.</p>',
			buttons:[
				{
					id:"confirmarBorrarModal",
					text:"Eliminar",
					class:"btn-red"
				},{
					id:"cancelarBorrarModal",
					text:"Cancelar",
					class:"btn-blue"
				}
			]
		},function(id){
			switch(id){
				case "ready":
				break;
				case "confirmarBorrarModal":
					Cupomida.modal.hide();
					$.post(Cupomida.JSONroot,{
						Accion:"Eliminar Cupon",
						ID:id_cupon
					},function(data){
						if(data.Success) if(typeof(callback)=="function") callback(true);
						else{
							Cupomida.errorCode(data.errorCode,function(){
								Cupomida.mensajeError("Error","Ocurrió un error al eliminar el cupón.");
							});
						}
					},"json").fail(function(jqXHR, textStatus){
						Cupomida.serverResponse(jqXHR, textStatus);
					});
				break;
				default:
					Cupomida.modal.hide();
					if(typeof(callback)=="function") callback(false);
				break;
			}
		});
	},
	habilitarCupon:function(id_cupon,callback){
		$.post(Cupomida.JSONroot,{
			Accion:"Habilitar Cupon",
			ID:id_cupon
		},function(data){
			if(data.Success) if(typeof(callback)=="function") callback(true);
			else{
				Cupomida.errorCode(data.errorCode,function(){
					Cupomida.mensajeError("Error","Ocurrió un error al habilitar el cupón.");
				});
			}
		},"json").fail(function(jqXHR, textStatus){
			Cupomida.serverResponse(jqXHR, textStatus);
		});
	},
	borrarCliente:function(id_cliente,callback){
		Cupomida.modal.show({
			title:"Confirme",
			body:'<p class="lead">Por favor confirme que desea desvincular a este cliente.</p>',
			buttons:[
				{
					id:"confirmarBorrarModal",
					text:"Eliminar",
					class:"btn-red"
				},{
					id:"cancelarBorrarModal",
					text:"Cancelar",
					class:"btn-blue"
				}
			]
		},function(id){
			switch(id){
				case "ready":
				break;
				case "confirmarBorrarModal":
					Cupomida.modal.hide();
					$.post(Cupomida.JSONroot,{
						Accion:"Eliminar Cliente",
						ID:id_cliente
					},function(data){
						if(data.Success) if(typeof(callback)=="function") callback(true);
						else{
							Cupomida.errorCode(data.errorCode,function(){
								Cupomida.mensajeError("Error","Ocurrió un error al desvincular al cliente.");
							});
						}
					},"json").fail(function(jqXHR, textStatus){
						Cupomida.serverResponse(jqXHR, textStatus);
					});
				break;
				default:
					Cupomida.modal.hide();
					if(typeof(callback)=="function") callback(false);
				break;
			}
		});
	},
	actualizarDatosUsuario:function(callback){
		var first_name=$("#perfilNombre").val().trim(),
			last_name=$("#perfilApellido").val().trim(),
			email=$("#perfilCorreo").val().trim(),
			gender=$("input[name='perfilSexo']:checked").val(),
			falta='<ul>';
		if(first_name==="") falta+='<li>Nombre.</li>';
		if(last_name==="") falta+='<li>Apellido.</li>';
		if(email==="") falta+='<li>Correo.</li>';
		if(gender===undefined) falta+='<li>Género.</li>';
		falta+='</ul>';
		if(falta != '<ul></ul>'){
			Cupomida.mensajeError("Faltan datos:",falta);
		}else{
			var key;
			if(/#Validar\/\w{5}/.test(window.location.hash)){
				key=window.location.hash.replace("#Validar/","");
			}else{
				key=undefined;
			}
			$.post(Cupomida.JSONroot,{
				Accion:"Actualizar Datos Usuario",
				id_usuario:Cupomida.user.id_usuario,
				first_name:first_name,
				last_name:last_name,
				email:email,
				gender:gender,
				key:key
			},function(data){
				if(data.Success){
					Cupomida.user.first_name=first_name;
					Cupomida.user.last_name=last_name;
					Cupomida.user.email=email;
					Cupomida.user.gender=gender;
					Cupomida.mensajeInfo(
						"Actualizado",
						"Los datos fueron actualizados correctamente."
					);
					Cupomida.menuLogin();
					if(typeof(callback)=="function") callback();
				}else{
					Cupomida.errorCode(data.errorCode,function(){
						Cupomida.mensajeError(
							"Error",
							"Ocurrió un error al actualizar sus datos."
						);
					});
				}
			},"json").fail(function(jqXHR, textStatus){
				Cupomida.serverResponse(jqXHR, textStatus);
			});
		}
	},
	actualizarContraUsuario:function(){
		var contraOld=$("#perfilContraOld").val().trim(),
			contra=$("#perfilContra").val().trim(),
			contraVal=$("#perfilContraVal").val().trim(),
			falta="<ul>";
		if(contra!=contraVal) falta+='<li>Las contraseñas no concuerdan.</li>';
		if(contraOld==contraVal) falta+='<li>Las contraseña nueva debe ser distinta a la actual.</li>';
		falta+="</ul>";
		if(falta != '<ul></ul>'){
			Cupomida.mensajeError("Error:",falta);
		}else{
			Cupomida.whirlpool(function(){
				var key;
				if(/#Validar\/\w{5}/.test(window.location.hash)){
					key=window.location.hash.replace("#Validar/","");
				}else{
					key=undefined;
				}
				$.post(Cupomida.JSONroot,{
					Accion:"Actualizar Contra Usuario",
					passOld:Whirlpool(contraOld),
					pass:Whirlpool(contra),
					key:key
				},function(data){
					if(data.Success){
						if(contra!=="" && data.Password.Contra===0){
							Cupomida.mensajeError(
								"Contraseña",
								"No se pudo cambiar la contraseña porque "+
								"la actual no coincide con nuestros registros."
							);
						}else{
							Cupomida.mensajeInfo(
								"Actualizado",
								"Los datos fueron actualizados correctamente."
							);
						}
						if(typeof(callback)=="function") callback();
					}else{
						Cupomida.errorCode(data.errorCode,function(){
							Cupomida.mensajeError(
								"Error",
								"Ocurrió un error al cambiar la contraseña."
							);
						});
					}
				},"json").fail(function(jqXHR, textStatus){
					Cupomida.serverResponse(jqXHR, textStatus);
				});
			});
		}
	},
	actualizarRol:function(ID,Rol,callback){
		$.post(Cupomida.JSONroot,{
			Accion:"Actualizar Rol",
			ID:ID,
			Rol:Rol
		},function(data){
			if(data.Success) if(typeof(callback)=="function") callback(true);
			else{
				Cupomida.errorCode(data.errorCode,function(){
					if(typeof(callback)=="function") callback(false);
				});
			}
		},"json").fail(function(jqXHR, textStatus){
			Cupomida.serverResponse(jqXHR, textStatus);
		});
	},
	actualizarEmpresas:function(Empresas,ID,callback){
		$.post(Cupomida.JSONroot,{
			Accion:"Actualizar Empresas",
			ID:ID,
			Empresas:Empresas
		},function(data){
			if(data.Success) if(typeof(callback)=="function") callback(true);
			else{
				Cupomida.errorCode(data.errorCode,function(){
					if(typeof(callback)=="function") callback(false);
				});
			}
		},"json").fail(function(jqXHR, textStatus){
			if(typeof(callback)=="function") callback(false);
			Cupomida.serverResponse(jqXHR, textStatus);
		});
	},
	actualizarPreferencias:function(Preferencias,callback){
		$.post(Cupomida.JSONroot,{
			Accion:"Actualizar Preferencias",
			Preferencias:Preferencias
		},function(data){
			if(data.Success) if(typeof(callback)=="function") callback(true);
			else{
				Cupomida.errorCode(data.errorCode,function(){
					if(typeof(callback)=="function") callback(false);
				});
			}
		},"json").fail(function(jqXHR, textStatus){
			if(typeof(callback)=="function") callback(false);
			Cupomida.serverResponse(jqXHR, textStatus);
		});
	},
	actualizarCliente:function(postData,callback){
		postData.Accion="Actualizar Cliente";
		$.post(Cupomida.JSONroot,postData,function(data){
			if(data.Success) if(typeof(callback)=="function") callback(true);
			else{
				Cupomida.errorCode(data.errorCode,function(){
					if(typeof(callback)=="function") callback(false);
				});
			}
		},"json").fail(function(jqXHR, textStatus){
			if(typeof(callback)=="function") callback(false);
			Cupomida.serverResponse(jqXHR, textStatus);
		});
	},
	validarCodigo:function(postData,callback){
		postData.Accion="Validar Codigo";
		$.post(Cupomida.JSONroot,postData,function(data){
			if(data.Success){
				if(typeof(callback)=="function"){
					callback(data.Result);
				}
			}else{
				Cupomida.errorCode(data.errorCode,function(){
					if(typeof(callback)=="function") callback(false);
				});
			}
		},"json").fail(function(jqXHR, textStatus){
			if(typeof(callback)=="function") callback(false);
			Cupomida.serverResponse(jqXHR, textStatus);
		});
	},
	mensajeTutorial:function(Titulo,Descripcion,Duracion){
		var rep,content="";
		Duracion=Duracion || 15000;
		if($(".alert-info").length){
			$(".alert-info").append("<br/><strong>"+Titulo+"</strong> "+Descripcion);
			clearTimeout(Cupomida.messagesTimeOut);
		}else{
			rep={
				_Titulo:Titulo,
				_Descripcion:Descripcion
			};
			content=Cupomida.tpl.tutorial.replace(
				/_Titulo|_Descripcion/g,
				function(m){return rep[m];
			});
			if($(document).scrollTop()>Cupomida.sizes.header.height){
				$(Cupomida.select.content).append(content);
			}else $(Cupomida.select.content).prepend(content);
			$(".alert-success").fadeIn(250).removeClass("hidden");
		}
		Cupomida.messagesTimeOut=setTimeout(function(){
			$(".alert-success").fadeOut(250,function(){
				$(this).remove();
			});
		},Duracion);
	},
	mensajeInfo:function(Titulo,Descripcion,Duracion){
		var rep,content="";
		Duracion=Duracion || 5000;
		if($(".alert-info").length){
			$(".alert-info").append("<br/><strong>"+Titulo+"</strong> "+Descripcion);
			clearTimeout(Cupomida.messagesTimeOut);
		}else{
			rep={
				_Titulo:Titulo,
				_Descripcion:Descripcion
			};
			content=Cupomida.tpl.mensaje.replace(
				/_Titulo|_Descripcion/g,
				function(m){return rep[m];
			});
			if($(document).scrollTop()>Cupomida.sizes.header.height){
				$(Cupomida.select.content).append(content);
			}else $(Cupomida.select.content).prepend(content);
			$(".alert-info").fadeIn(250).removeClass("hidden");
		}
		Cupomida.messagesTimeOut=setTimeout(function(){
			$(".alert-info").fadeOut(250,function(){
				$(this).remove();
			});
		},Duracion);
	},
	mensajeError:function(Titulo,Descripcion,Duracion){
		var rep,content="";
		Duracion=Duracion || 5000;
		if($(".alert-danger").length){
			$(".alert-danger").append("<br/><strong>"+Titulo+"</strong> "+Descripcion);
			clearTimeout(Cupomida.messagesTimeOut);
		}else{
			rep={
				_Titulo:Titulo,
				_Descripcion:Descripcion
			};
			content=Cupomida.tpl.error.replace(
				/_Titulo|_Descripcion/g,
				function(m){return rep[m];
			});
			if($(document).scrollTop()>Cupomida.sizes.header.height){
				$(Cupomida.select.content).append(content);
			}else $(Cupomida.select.content).prepend(content);
			$(".alert-danger").fadeIn(250).removeClass("hidden");
		}
		Cupomida.messagesTimeOut=setTimeout(function(){
			$(".alert-danger").fadeOut(250,function(){
				$(this).remove();
			});
		},Duracion);
	},
	modal:{
		show:function(data,callback){
			if(!$(Cupomida.select.modal).is(":visible")){
				$(".modal-title").html(data.title);
				$(".modal-body").html(data.body);
				if(typeof(data.buttons)=="object"){
					for(var id in data.buttons){
						$(".modal-footer").append('<button id="'+data.buttons[id].id+'" '+
							'type="button" '+
							'class="btn '+data.buttons[id].class+'">'+
								data.buttons[id].text+
							'</button>');
						$("#"+data.buttons[id].id).off("click").on("click",function(e){
							if(typeof(callback)=="function"){
								callback($(e.currentTarget).attr("id"));
							}
						});
					}
				}
				$(Cupomida.select.modal)
					.off("shown.bs.modal")
					.on("shown.bs.modal",function(e){
						if(typeof(callback)=="function") callback("ready");
					})
					.modal("show")
					.off("hidden.bs.modal")
					.on("hidden.bs.modal",function(e){
						if(typeof(callback)=="function") callback("");
					});
			}
		},
		hide:function(){
			$(Cupomida.select.modal).modal("hide").find(".btn").each(function(i,e){
				$(e).off("click");
			});
			$(".modal-title").html("");
			$(".modal-body").html("");
			$(".modal-footer").html("");
		}
	},
	tpl:{
		cuponCondiciones:'<li>_Condicion</li>',
		cuponNuevaCondicion:'<input type="text" class="nuevaCondicion" placeholder="Condición" value="_Condicion">',
		cuponComida:'<span class="label label-info tags">_Titulo</span>',
		cuponMapa:'<div id="googleMap"></div>',
		cuponSemana:'<div id="semanaHabil" class="btn-group pull-right">'+
				'<button type="button" class="btn btn-sm">L</button>'+
				'<button type="button" class="btn btn-sm">M</button>'+
				'<button type="button" class="btn btn-sm">M</button>'+
				'<button type="button" class="btn btn-sm">J</button>'+
				'<button type="button" class="btn btn-sm">V</button>'+
				'<button type="button" class="btn btn-sm">S</button>'+
				'<button type="button" class="btn btn-sm">D</button>'+
			'</div>',
		cuponDetalle:'<div class="row">'+
				'<div class="col-sm-6">'+
					'<div class="product-image-large">_ImagenCupon</div>'+
				'</div>'+
				'<div class="col-sm-6 product-details">'+
					'<h4>_Titulo</h4>'+
					'<input type="hidden" id="nuevoIdCupon" value="_IdCupon">'+
					'<h5></h5>'+
					'<p>_Descripcion</p>'+
					'<table class="shop-item-selections" border="0px">'+
						'<tr>'+
							'<td>'+
								'<div class="price">'+
									'<span class="price-was">_Descuento</span> $_Precio'+
								'</div>'+
							'</td>'+
							'<td class="right">_btnImprimir</td>'+
						'</tr>'+
						'<tr>'+
							'<td>_btnDenunciar</td>'+
							'<td class="right">_Hora_Canjeo</td>'+
						'</tr>'+
						'<tr>'+
							'<td colspan="2">_Semana</td>'+
						'</tr>'+
						'<tr>'+
							'<td>&nbsp</td>'+
						'</tr>'+
						'<tr>'+
							'<td>_Impresos</td>'+
							'<td class="right">_Termino</td>'+
						'</tr>'+
						'<tr>'+
							'<td>_FBLikes</td>'+
							'<td class="right">_TWShare</td>'+
						'</tr>'+
					'</table>'+
				'</div>'+
				'<div class="col-sm-12">'+
					'<div class="tabbable">'+
						'<ul class="nav nav-tabs product-details-nav">'+
							'<li class="active">'+
								'<a href="#tab1" data-toggle="tab">Acerca del cupón</a>'+
							'</li>'+
							'<li>'+
								'<a href="#tab2" data-toggle="tab">Acerca de _NombreCliente</a>'+
							'</li>'+
						'</ul>'+
						'<div class="tab-content product-detail-info">'+
							'<div class="tab-pane active" id="tab1">'+
								'<div class="container">'+
									'<div class="row">'+
										'<div class="col-md-11 col-xs-11">'+
											'<h4>Descripción del producto  '+
												'<i class="glyphicon glyphicon-star rating-star-cupon"></i>'+
												'<i class="glyphicon glyphicon-star rating-star-cupon"></i>'+
												'<i class="glyphicon glyphicon-star rating-star-cupon"></i>'+
												'<i class="glyphicon glyphicon-star rating-star-cupon"></i>'+
												'<i class="glyphicon glyphicon-star rating-star-cupon"></i>'+
												'<span>_ValoracionesCupon</span>'+
											'</h4>'+
											'<p>_Detalle</p>'+
											'<p>_Categoria</p>'+
											'<h4>Condiciones del cupón</h4>'+
											'<ul>_Condiciones</ul>'+
											'<h4>Información adicional</h4>'+
											'_Horario'+
											'<p>_Informacion</p>'+
										'</div>'+
										'<div class="col-md-11 col-xs-11">'+
											'<p>'+
												'<strong>'+
													'<i class="glyphicon glyphicon-home"></i> '+
													'Ubicación: '+
												'</strong> _Comuna'+
												'_Distancia'+
											'</p>'+
											'<div>_Mapa</div>'+
										'</div>'+
									'</div>'+
								'</div>'+
							'</div>'+
							'<div class="tab-pane" id="tab2">'+
								'<div class="container">'+
									'<div class="row">'+
										'<div class="col-md-8 col-xs-8">'+
											'<div class="panel panel-default">'+
												'<div class="panel-body">'+
													'<div class="row">'+
														'<div class="col-xs-12 col-sm-4 text-center">'+
															'_ImagenCliente'+
															'<ul class="list-inline ratings text-center">'+
																'<li class="glyphicon glyphicon-star rating-star-cliente"></li>'+
																'<li class="glyphicon glyphicon-star rating-star-cliente"></li>'+
																'<li class="glyphicon glyphicon-star rating-star-cliente"></li>'+
																'<li class="glyphicon glyphicon-star rating-star-cliente"></li>'+
																'<li class="glyphicon glyphicon-star rating-star-cliente"></li>'+
															'</ul>'+
															'<span>_ValoracionesCliente</span>'+
														'</div>'+
														'<div class="col-xs-12 col-sm-8">'+
															'<h2>_NombreIntCliente</h2>'+
															'<p><strong>Contacto: </strong> _Telefono _Email</p>'+
															'<p><strong>Horario: </strong> _Atencion</p>'+
															'<p><strong>Comidas: </strong>_Comidas</p>'+
															'<p>'+
																'<button type="button" class="btn btn-red ver-cliente" data-id="_IdCliente">'+
																	'Ver más cupones<br/>de este local'+
																'</button>'+
															'</p>'+
														'</div>'+
													'</div>'+
												'</div>'+
											'</div>'+
										'</div>'+
										'<div class="col-md-5 col-xs-8">_FBComments</div>'+
									'</div>'+
								'</div>'+
							'</div>'+
						'</div>'+
					'</div>'+
				'</div>'+
			'</div>',
		twitterShare:"",
		likes:'<div class="fb-like" data-href="_FBLink" data-layout="button_count" data-action="like" data-show-faces="true" data-share="false"></div>',
		comentarios:'comentarios:<div class="fb-comments" data-href="_FBLink" data-numposts="100" data-colorscheme="light"></div>',
		cupones:'<div class="col-md-11" align="center">_Cupones</div>',
		cupon:'<div class="col-md-4 col-sm-6" id="Cupon-_ID">'+
				'<div class="portfolio-item">'+
					'<div class="portfolio-image">'+
						'<a href="javascript:void(0);" class="ver-cupon" data-id="_ID">'+
							'<img src="_Imagen"  width="300" height="220"  alt="_Titulo">'+
						'</a>'+
					'</div>'+
					'<div class="portfolio-info">'+
						'<ul>'+
							'<li class="portfolio-project-name">_Titulo</li>'+
							'<li>_Descripcion</li>'+
							'<li>$_Precio_Distancia</li>'+
							'<li class="read-more">'+
								'<a href="javascript:void(0);" class="btn btn-red ver-cupon" data-id="_ID">Ver cupón</a> '+
								'_Admin'+
							'</li>'+
						'</ul>'+
					'</div>'+
				'</div>'+
			'</div>',
		btnImprimir:'<a href="javascript:void(0)" class="btn btn-red btnImprimir">'+
				'<i class="icon-shopping-cart icon-white"></i> Imprimir cupón'+
			'</a>',
		iframeCupon:'<iframe src="PDF.php?ID=_ID" id="iframeCupon" width="100%"></iframe>',
		btnGuardar:'<a href="javascript:void(0)" class="btn btn-blue btnGuardar">'+
				'<i class="glyphicon glyphicon-floppy-disk"></i> Guardar cupón'+
			'</a>',
		btnPaginacion:'<button type="button" class="btn btn-red btn-paginacion">_Numero</button>',
		categoria:'<li>'+
				'<a href="javascript:void(0);" class="ver-categoria" data-id="_ID" title="_Descripcion">_Titulo</a>'+
			'</li>',
		cabeceraLogin:'<div class="container">'+
				'<div class="row">'+
					'<div class="col-md-12">'+
						'<h1>Iniciar sesión</h1>'+
					'</div>'+
				'</div>'+
			'</div>',
		login:'<div class="row">'+
				'<div class="col-sm-5">'+
					'<div class="basic-login">'+
						'<form role="form" role="form">'+
							'<div class="form-group">'+
								'<label for="login-username">'+
									'<i class="glyphicon glyphicon-user"></i> '+
									'<b>Dirección de Correo:</b>'+
								'</label>'+
								'<input class="form-control" id="login-username" type="text" placeholder="">'+
							'</div>'+
							'<div class="form-group">'+
								'<label for="login-password">'+
									'<i class="glyphicon glyphicon-lock"></i> '+
									'<b>Contraseña:</b>'+
								'</label>'+
								'<input class="form-control" id="login-password" type="password" placeholder="">'+
							'</div>'+
							'<div class="form-group">'+
								'<a href="page-password-reset.html" class="forgot-password">¿Olvidaste la contraseña?</a>'+
								'<button type="submit" class="btn btn-blue pull-right" id="login-entrar">Login</button>'+
								'<div class="clearfix"></div>'+
							'</div>'+
						'</form>'+
					'</div>'+
				'</div>'+
				'<div class="col-sm-7 social-login">'+
					'<p>O inicia sesión con tu cuenta de Facebook.</p>'+//, Twitter o Google
					'<div class="social-login-buttons">'+
						'<a href="javascript:void(0);" class="btn-facebook-login">Iniciar con Facebook</a>'+
						//'<a href="javascript:void(0);" class="btn-twitter-login">Iniciar con Twitter</a>'+
					'</div>'+
					'<div class="col-sm-7 social-extras">'+
						'<span id="signinButton">'+
							'<span '+
								'class="g-signin" '+
								'data-callback="Cupomida.Google.signinCallback" '+
								'data-clientid="_GoogleID" '+
								'data-cookiepolicy="single_host_origin" '+
								'data-origin="http://cupomida.cl/" '+
								'data-requestvisibleactions="http://schema.org/AddAction" '+
								'data-scope="https://www.googleapis.com/auth/plus.login">'+
							'</span>'+
						'</span>'+
					'</div>'+
					'<div class="clearfix"></div>'+
					'<div class="not-member">'+
						'<p>No eres usuario? <a href="javascript:void(0);" class="page-register">Regístrate aquí</a></p>'+
					'</div>'+
				'</div>'+
			'</div>',
		cabeceraRegistro:'<div class="container">'+
				'<div class="row">'+
					'<div class="col-md-12">'+
						'<h1>Registro</h1>'+
					'</div>'+
				'</div>'+
			'</div>',
		registro:'<div class="row">'+
				'<div class="col-sm-5">'+
					'<div class="basic-login">'+
						'<form role="form">'+
							'<div class="form-group">'+
								'<label for="formCorreo">'+
									'<i class="glyphicon glyphicon-envelope"></i> '+
									'<b>Email</b>'+
								'</label>'+
								'<input class="form-control" id="formCorreo" value="_Email" type="text" placeholder="">'+
							'</div>'+
							'<div class="form-group">'+
								'<label for="formContra">'+
									'<i class="glyphicon glyphicon-lock"></i> '+
									'<b>Password</b>'+
								'</label>'+
								'<input class="form-control" id="formContra" type="password" placeholder="">'+
							'</div>'+
							'<div class="form-group">'+
								'<label for="formContraConf">'+
									'<i class="glyphicon glyphicon-lock"></i> '+
									'<b>Re-ingresar Password</b>'+
								'</label>'+
								'<input class="form-control" id="formContraConf" type="password" placeholder="">'+
							'</div>'+
							'<div class="form-group">'+
								'<button type="submit" class="btn btn-blue pull-right" id="formRegistrar">Registrar</button>'+
								'<div class="clearfix"></div>'+
							'</div>'+
						'</form>'+
					'</div>'+
				'</div>'+
				'<div class="col-sm-6 col-sm-offset-1 social-login">'+
					'<p>También te puedes registrar con tu Facebook.</p>'+//, Twitter o Google
					'<div class="social-login-buttons">'+
						'<a href="javascript:void(0);" class="btn-facebook-login">Usa Facebook</a>'+
						//'<a href="javascript:void(0);" class="btn-twitter-login">Usa Twitter</a>'+
					'</div>'+
				'</div>'+
				'<div class="col-sm-6" social-extras>'+
					'<span id="signinButton">'+
						'<span '+
							'class="g-signin" '+
							'data-callback="signinCallback" '+
							'data-clientid="_GoogleID" '+
							'data-cookiepolicy="single_host_origin" '+
							'data-origin="http://cupomida.cl/" '+
							'data-requestvisibleactions="http://schema.org/AddAction" '+
							'data-scope="https://www.googleapis.com/auth/plus.login">'+
						'</span>'+
					'</span>'+
				'</div>'+
			'</div>',
		perfil:'<div class="row">'+
				'<div class="tabbable">'+
					'<ul class="nav nav-tabs product-details-nav">'+
						'<li class="active">'+
							'<a href="#tabPerfilDatos" data-toggle="tab">Datos</a>'+
						'</li>'+
						'<li>'+
							'<a href="#tabPerfilImpresos" data-toggle="tab">Cupones Impresos</a>'+
						'</li>'+
						'<li>'+
							'<a href="#tabPerfilContra" data-toggle="tab">Contraseña</a>'+
						'</li>'+
						'<li>'+
							'<a href="#tabPerfilPref" data-toggle="tab">Preferencias</a>'+
						'</li>'+
					'</ul>'+
					'<div class="tab-content product-detail-info">'+
						'<div class="tab-pane active" id="tabPerfilDatos">'+
							'<div class="container">'+
								'<div class="row">'+
									'<div class="col-md-11 col-xs-11">'+
										'<div class="panel panel-default">'+
											'<div class="panel-body">'+
												'<div class="row">'+
													'<div class="col-sm-5">'+
														'<div class="img-thumbnail">'+
															'<input type="file" id="nuevaFileImagenPerfil">'+
															'<canvas id="nuevaImagenPerfil"></canvas>'+
														'</div>'+
													'</div>'+
													'<div class="col-sm-7">'+
														'<div class="form-group">'+
															'<h3>Cambiar Datos:</h3>'+
															'<label for="perfilNombre">'+
																'<i class="glyphicon glyphicon-user"></i> '+
																'<b>Nombre</b>'+
															'</label>'+
															'<input class="form-control" id="perfilNombre" type="text" value="_Nombre">'+
														'</div>'+
														'<div class="form-group">'+
															'<label for="perfilApellido">'+
																'<i class="glyphicon glyphicon-user"></i> '+
																'<b>Apellido</b>'+
															'</label>'+
															'<input class="form-control" id="perfilApellido" type="text" value="_Apellido">'+
														'</div>'+
														'<div class="form-group">'+
															'<label for="perfilCorreo">'+
																'<i class="glyphicon glyphicon-envelope"></i> '+
																'<b>Correo</b>'+
															'</label>'+
															'<input class="form-control" id="perfilCorreo" type="email" value="_Correo">'+
														'</div>'+
														'<div class="form-group">'+
															'<label for="">'+
																'<i class="glyphicon glyphicon-user"></i> '+
																'<b>Sexo</b>'+
															'</label><br>'+
															'<b>Mujer</b> <input type="radio" name="perfilSexo" value="female"> '+
															'<b>Hombre</b> <input type="radio" name="perfilSexo" value="male">'+
														'</div>'+
														'<div class="form-group">'+
															'<button type="button" class="btn btn-blue" id="btnDatosPerfil">Guardar</button> '+
															'<button type="button" class="btn btn-red btnCancelarPerfil">Cancelar</button>'+
														'</div>'+
													'</div>'+
												'</div>'+
											'</div>'+
										'</div>'+
									'</div>'+
								'</div>'+
							'</div>'+
						'</div>'+
						'<div class="tab-pane" id="tabPerfilImpresos">'+
							'<div class="container">'+
								'<div class="row">'+
									'<div class="col-md-11 col-xs-11">'+
										'<div class="panel panel-default">'+
											'<div class="panel-body">'+
												'<div class="search col-sm-12">'+
													'<label class="filtroMisCupones">'+
														'<input type="checkbox" id="vencidosMisCupones" value="">'+
														'Vencidos'+
													'</label>'+
													'<input type="text" class="pull-right" id="Filtro_Mis_Cupones" placeholder="Buscar">'+
												'</div>'+
												'<div class="col-md-12" id="MisCupones"></div>'+
											'</div>'+
										'</div>'+
									'</div>'+
								'</div>'+
							'</div>'+
						'</div>'+
						'<div class="tab-pane" id="tabPerfilContra">'+
							'<div class="container">'+
								'<div class="row">'+
									'<div class="col-md-11 col-xs-11">'+
										'<div class="panel panel-default">'+
											'<div class="panel-body">'+
												'<div class="col-md-12">'+
													'<div class="form-group">'+
														'<h3>Cambiar Contraseña:</h3>'+
														'<i class="glyphicon glyphicon-lock"></i> '+
														'<b>Contraseña Actual:</b> '+
															'<input class="form-control" type="password" id="perfilContraOld"> '+
														'<i class="glyphicon glyphicon-lock"></i> '+
														'<b>Contraseña Nueva:</b> '+
															'<input class="form-control" type="password" id="perfilContra">'+
														'<i class="glyphicon glyphicon-lock"></i> '+
														'<b>Confirme su contraseña nueva:</b> '+
															'<input class="form-control" type="password" id="perfilContraVal">'+
													'</div>'+
													'<div class="form-group">'+
														'<button class="btn btn-blue" id="btnContraPerfil">Guardar</button> '+
														'<button class="btn btn-red btnCancelarPerfil">Cancelar</button>'+
													'</div>'+
												'</div>'+
											'</div>'+
										'</div>'+
									'</div>'+
								'</div>'+
							'</div>'+
						'</div>'+
						'<div class="tab-pane" id="tabPerfilPref">'+
							'<div class="container">'+
								'<div class="row">'+
									'<div class="col-md-11 col-xs-11">'+
										'<div class="panel panel-default">'+
											'<div class="panel-body">'+
												'<div class="col-md-12">'+
													'<div class="col-sm-5">'+
														'<div class="form-group">'+
															'<label for="perfilCategoria">'+
																'<i class="glyphicon glyphicon-user"></i> '+
																'<b>Preferencia:</b>'+
															'</label>'+
															'<div class="form-group"><div id="perfilCategoria"></div></div>'+
														'</div>'+
														'<div class="form-group">'+
															'<button class="btn btn-red btnCancelarPerfil">Cancelar</button>'+
														'</div>'+
													'</div>'+
												'</div>'+
											'</div>'+
										'</div>'+
									'</div>'+
								'</div>'+
							'</div>'+
						'</div>'+
					'</div>'+
				'</div>'+
			'</div>',
		estadisticas:'<div class="row">'+
				'<div class="col-sm-6 pull-right">'+
					'<label class="filtroEstadistica">'+
						'<input type="checkbox" id="eliminadosEstadisticas" value="">'+
						'Eliminados'+
					'</label>'+
					'<label class="filtroEstadistica">'+
						'<input type="checkbox" id="vencidosEstadisticas" value="">'+
						'Vencidos'+
					'</label>'+
					'<label class="filtroEstadistica">'+
						'<input type="checkbox" id="vendidosEstadisticas" value="">'+
						'Completamente vendidos'+
					'</label>'+
					'<select id="intervaloEstadisticas">'+
						'<option value="Semana">Esta semana</option>'+
						'<option value="Mes">Este mes</option>'+
						'<option value="Año">Este año</option>'+
						'<option value="Todo">Todo</option>'+
					'</select>'+
				'</div>'+
				'<div class="col-sm-12">'+
					'<h2>Cupones impresos:</h2>'+
					'<div class="search">'+
						'<input type="text" id="FiltroImpresosEstadistica" placeholder="Buscar">'+
					'</div>'+
					'<div id="CuponesImpresos"></div>'+
				'</div>'+
				'<div class="col-sm-12">'+
					'<h2>Cupones vistos en detalle:</h2>'+
					'<div class="search">'+
						'<input type="text" id="FiltroVistosDetalleEstadistica" placeholder="Buscar">'+
					'</div>'+
					'<div id="CuponesVistosDetalle"></div>'+
				'</div>'+
				'<div class="col-sm-12">'+
					'<h2>Cupones vistos:</h2>'+
					'<div class="search">'+
						'<input type="text" id="FiltroVistosEstadistica" placeholder="Buscar">'+
					'</div>'+
					'<div id="CuponesVistosTabla"></div>'+
				'</div>'+
				'<div class="col-sm-12">'+
					'<h2>Likes a cupones:</h2>'+
					'<div class="search">'+
						'<input type="text" id="FiltroLikesEstadistica" placeholder="Buscar">'+
					'</div>'+
					'<div id="CuponesLikes"></div>'+
				'</div>'+
				'<div class="col-sm-12">'+
					'<h2>Cupones vistos por hora:</h2>'+
					'<div id="CuponesVistosHora"></div>'+
				'</div>'+
				'<div class="col-sm-12">'+
					'<h2>Cupones vistos por día de la semana:</h2>'+
					'<div id="CuponesVistosDia"></div>'+
				'</div>'+
				'<div class="col-sm-12">'+
					'<h2>Cupones vistos por categoría:</h2>'+
					'<div id="CuponesVistosCategoria"></div>'+
				'</div>'+
				'<div class="col-sm-12">'+
					'<h2>Visitas por cupón:</h2>'+
					'<select id="EstadisticaCupon"><option value="0">Seleccione un Cupón</option></select>'+
					'<select id="RangoFechaCupon"></select>'+
					'<div id="CuponesVistos"></div>'+
				'</div>'+
			'</div>',
		impresosEstadisticas:'<tr><td>_Numero</td><td>_Cupon</td><td>_Restantes</td><td>_Ver</td></tr>',
		vistosEstadisticas:'<tr><td>_Numero</td><td>_Cupon</td><td>_Ver</td></tr>',
		likesEstadisticas:'<tr><td>_Numero</td><td>_Cupon</td><td>_Ver</td></tr>',
		timeLine:'<div class="row">'+
				'<div class="searchTimeLine">'+
					'<input type="date" id="fechaTimeLine" />'+
				'</div>'+
				'<table class="table tableTimeLine table-bordered">'+
					'<thead>'+
						'<tr>'+
							'<td width="100px">Hora:</td>'+
							'<td>Cupones:</td>'+
						'</tr>'+
					'</thead>'+
					'<tbody></tbody>'+
				'</table>'+
				'<div class="OverTable col-sm-10">'+
					'<div id="CuponesTimeLine" class="col-sm-12"></div>'+
				'</div>'+
			'</div>',
		timeLineHora:'<tr>'+
				'<td>_Hora</td>'+
				'<td></td>'+
			'</tr>',
		cuponTimeLine:'<div class="CuponTimeLine ver-cupon" data-id="_ID" '+
			'style="height:_Altopx; top:_Posicionpx;">'+
				'<img src="_Imagen" alt="_Titulo"><br/>'+
				'<span>_Titulo</span><br/>'+
				'<span>_Hora</span><br/>'+
				'<span>_Distancia</span>'+
			'</div>',
		panelControl:'<div class="row" id="panelControl">_Contenido</div>',
		contentPanel:'<div class="col-sm-6">'+
				'<h2>_Titulo</h2>'+
				'<div class="search">'+
					'<input type="text" id="Filtro_ID" placeholder="Buscar">'+
				'</div>'+
				'<div id="_ID">'+
					'<img src="img/loader.gif" class="img-responsive" alt="Por favor espere">'+
				'</div>'+
			'</div>',
		denunciaPanel:'<tr><td>_Numero</td><td>_Cupon</td><td>_Tipo</td><td>_Ver</td></tr>',
		clientePanel:'<tr id="Cliente-_ID"><td>_Cliente</td><td>_Cupones</td><td>_Ver</td><td>_Eliminar</td></tr>',
		pendientePanel:'<tr id="Pendiente-_ID"><td>_Cliente</td><td>_Ver</td><td>_Habilitar</td><td>_Eliminar</td></tr>',
		cuponPanel:'<tr id="Cupon-_ID">'+
				'<td>_Imagen</td>'+
				'<td>_Titulo</td>'+
				'<td>_Descripcion</td>'+
				'<td>_Categoria</td>'+
				'<td>_Ver</td>'+
				'<td>_Editar</td>'+
				'<td>_Borrar</td>'+
			'</tr>',
		usuarioPanel:'<tr id="Usuario-_ID">'+
				'<td>_Nombre _Apellido</td>'+
				'<td>'+
					'<select class="rol">'+
						'<option value="1">Administrador</option>'+
						'<option value="2">Cliente</option>'+
						'<option value="0" selected="selected">Usuario</option>'+
					'</select>'+
				'</td>'+
				'<td class="empresa"></td>'+
			'</tr>',
		mapa:'<div class="row pageMapa">'+
				'<div class="col-sm-12" id="pageMapa"></div>'+
			'</div>',
		infoMapa:'<div class="col-sm-12">'+
				'<div class="col-sm-4">'+
					'_Imagen'+
					'<br/><br/>'+
					'<button class="btn btn-red ver-cupon" data-id="_ID">Ver Cupón</button>'+
				'</div>'+
				'<div class="col-sm-8">'+
					'<h2>_Titulo</h2>'+
					'<p>_Descripcion <br/> _Detalle</p>'+
					'<p>$_Precio - Evaluación:_Evaluacion</p>'+
					'<p>_Horario</p>'+
					'_Dias'+
				'</div>'+
			'</div>',
		codigos:'<div class="row">'+
				'<div class="col-sm-12">'+
					'<h2>Códigos:</h2>'+
					'<div class="search col-sm-12">'+
						'<label class="filtroCodigos">'+
							'<input type="checkbox" id="eliminadosCodigos" value="">'+
							'Eliminados'+
						'</label>'+
						'<label class="filtroCodigos">'+
							'<input type="checkbox" id="vencidosCodigos" value="">'+
							'Vencidos'+
						'</label>'+
						'<label class="filtroCodigos">'+
							'<input type="checkbox" id="validadosCodigos" value="">'+
							'Validados'+
						'</label>'+
						'<a class="pull-right" href="javascript:void(0);" id="Descargar_Codigos" >'+
							'<button class="btn btn-green">Descargar CSV</button>'+
						'</a>'+
						'<input type="text" class="pull-right" id="Filtro_Codigos" placeholder="Buscar por Título">'+
						'<input type="text" class="pull-right" id="Filtro_Series" placeholder="Buscar por Serie">'+
					'</div>'+
					'<div id="Codigos"></div>'+
				'</div>'+
			'</div>',
		codigoCodigos:'<tr id="Codigo-_ID">'+
				'<td>_Cupon</td>'+
				'<td>_Serie</td>'+
				'<td>_Validez</td>'+
				'<td>_Impreso</td>'+
				'<td>_Fecha</td>'+
				'<td>_Cobrado</td>'+
				'<td>_Ver</td>'+
			'</tr>',
		codigosUsuario:'<tr id="Codigo-_ID">'+
				'<td>_Cupon</td>'+
				'<td>_Serie</td>'+
				'<td>_Validez</td>'+
				'<td>_Fecha</td>'+
				'<td>_Cobrado</td>'+
				'<td>_Ver</td>'+
			'</tr>',
		espera:'<div class="row">'+
				'<div class="col-sm-12" align="center">'+
					'<img src="img/loader.gif" class="img-responsive" alt="Por favor espere">'+
				'</div>'+
			'</div>',
		imagenEspera:'<img '+
			'width="_Ancho" '+
			'height="_Alto" '+
			'src="img/loader.gif" '+
			'class="loading_Class" '+
			'data-id="_ID" '+
			'alt="_Titulo">',
		vacio:'<div class="row">'+
				'<div class="col-sm-12" align="center">'+
					'<h1>404</h1>'+
					'<h2>Contenido no encontrado.</h2>'+
				'</div>'+
			'</div>',
		menu:'<nav class="navbar navbar-default" role="navigation">'+
				'<div class="container">'+
					'<div class="menuextras">'+
						'<div class="extras">'+
							'<ul id="menu-login"></ul>'+
						'</div>'+
					'</div>'+
					'<div class="navbar-header">'+
						'<button type="button" '+
							'class="navbar-toggle" '+
							'data-toggle="collapse" '+
							'data-target="#mainmenu" '+
							'aria-expanded="true" '+
							'aria-controls="navbar">'+
							'<span class="sr-only">Toggle navigation</span>'+
							'<span class="icon-bar"></span>'+
							'<span class="icon-bar"></span>'+
							'<span class="icon-bar"></span>'+
						'</button>'+
						'<a href="javascript:void(0);" class="page-inicio">'+
							'<img src="img/logos/cupomidalogo.png" width="200" height="50" alt="Logo">'+
						'</a>'+
					'</div>'+
					'<nav id="mainmenu" class="mainmenu navbar-collapse collapse">'+
						'<ul></ul>'+
					'</nav>'+
				'</div>'+
			'</nav>',
		menuMapa:'<li>'+
				'<a href="javascript:void(0);" class="page-mapa">Mapa</a>'+
			'</li>',
		menuTimeLine:'<li>'+
				'<a href="javascript:void(0);" class="page-timeline">Time Line</a>'+
			'</li>',
		menuAdmin:'<li class="has-submenu">'+
				'<a href="javascript:void(0);">Administración</a>'+
				'<div class="mainmenu-submenu">'+
					'<div>'+
						'<h4>Administración</h4>'+
						'<ul id="menuAdmin"></ul>'+
					'</div>'+
				'</div>'+
			'</li>',
		menuBuscar:'<li class="col-sm-3 col-md-3 pull-right">'+
				'<form class="navbar-form" role="search">'+
					'<div class="input-group">'+
						'<input type="text" class="form-control" placeholder="Buscar" id="txt-buscar">'+
						'<div class="input-group-btn">'+
							'<button class="btn btn-red btn-buscar" type="submit">'+
								'<i class="glyphicon glyphicon-search"></i>'+
							'</button>'+
						'</div>'+
					'</div>'+
				'</form>'+
			'</li>',
		menuPerfil:'<li>'+
				'<i class="glyphicon glyphicon-user icon-white"></i>'+
				'<a href="javascript:void(0);" class="page-perfil"> Mi perfil</a>'+
			'</li>'+
			'<li>'+
				'<i class="glyphicon glyphicon-off icon-white"></i>'+
				'<a href="javascript:void(0);" class="page-cerrar"> Salir</a>'+
			'</li>',
		menuLogin:'<li>'+
				'<i class="glyphicon glyphicon-user icon-white"></i>'+
				'<a href="javascript:void(0);" class="page-login"> Iniciar</a>'+
			'</li>',
		menuTime:'<li>'+
				'<i class="glyphicon glyphicon-time icon-white"></i>&nbsp'+
				'<span id="clock"></span>'+
			'</li>',
		menuEstadisticas:'<li>'+
				'<a href="javascript:void(0);" class="page-estadisticas">Estadísticas</a>'+
			'</li>',
		menuPanelControl:'<li>'+
				'<a href="javascript:void(0);" class="page-panel">Panel de Control</a>'+
			'</li>',
		menuNuevoCupon:'<li>'+
				'<a href="javascript:void(0);" class="page-nuevo">Nuevo Cupón</a>'+
			'</li>',
		menuCodigos:'<li>'+
				'<a href="javascript:void(0);" class="page-codigos">Validar Códigos</a>'+
			'</li>',
		menuRecomendacion:'<a href="javascript:void(0);" data-id="_ID">'+
				'_Imagen'+
			'</a>',
		tutorial:'<div class="col-md-11 col-sm-12 alert alert-success hidden"><strong>_Titulo</strong> _Descripcion</div>',
		mensaje:'<div class="col-md-11 col-sm-12 alert alert-info hidden"><strong>_Titulo</strong> _Descripcion</div>',
		error:'<div class="col-md-11 col-sm-12 alert alert-danger hidden"><strong>_Titulo</strong> _Descripcion</div>',
		footer:'<div class="footer">'+
				'<div class="container">'+
					'<div class="row">'+
						'<div class="col-footer col-md-3 col-xs-6">'+
							'<h3>Recomendaciones</h3>'+
							'<div class="portfolio-item">'+
								'<div class="portfolio-image" id="recomendation"></div>'+
							'</div>'+
						'</div>'+
						'<div class="col-footer col-md-3 col-xs-6">'+
							'<h3>Categorias</h3>'+
							'<ul class="no-list-style footer-navigate-section" id="categories"></ul>'+
						'</div>'+
						'<div class="col-footer col-md-4 col-xs-6">'+
							'<h3>Contacto</h3>'+
							'<p class="contact-us-details">'+
								'<b>Direccion:</b> ana cruchaga 394 llay llay<br/>'+
								'<b>Telefono:</b> +569 93940056<br/>'+
								'<b>Email:</b> <a href="mailto:cupomida.cl@gmail.com">cupomida.cl@gmail.com</a>'+
							'</p>'+
						'</div>'+
						'<div class="col-footer col-md-2 col-xs-6">'+
							'<h3>Buscanos en</h3>'+
							'<ul class="footer-stay-connected no-list-style">'+
								'<li>'+
									'<a href="http://www.facebook.com/profile.php?id=100008000925617" class="facebook" target="_blank"></a>'+
								'</li>'+
								'<li>'+
									'<a href="http://twitter.com/Cupomida" class="twitter" target="_blank"></a>'+
								'</li>'+
								'<li>'+
									'<a href="http://plus.google.com/u/0/108097162197125738985/about" class="googleplus" target="_blank"></a>'+
								'</li>'+
							'</ul>'+
						'</div>'+
					'</div>'+
					'<div class="row">'+
						'<div class="col-md-12">'+
							'<div class="footer-copyright">&copy; 2014 mmeneses. Todos los derechos reservados.</div>'+
						'</div>'+
					'</div>'+
				'</div>'+
			'</div>'
	},
	fileToBase64:function(e,canvas,width,max_height,callback){
		var ctx = canvas.getContext('2d'),
			reader = new FileReader(),
			img = new Image();
		reader.onload = function(event){
			img.onload = function(){
				var height=(width/(img.width/img.height));
				canvas.width = width;
				if(height>max_height) canvas.height = max_height;
				else canvas.height = height;
				ctx.fillStyle = "rgba(255,255,255,0)";
				ctx.drawImage(img,0,0,img.width,img.height,0,0,width,height);
				var dataURL = canvas.toDataURL(Cupomida.imageFormat.type,Cupomida.imageFormat.compression);
				if(typeof(callback)=="function") callback.call(this, dataURL);
			};
			switch(event.target.result.substring(0,15)){
				case "data:image/jpeg":
				case "data:image/png;":
				case "data:image/gif;":
					img.src = event.target.result;
				break;
				default:
					Cupomida.mensajeError(
						"Formato no válido",
						"Seleccione un archivo con un formato de imagen válido."
					);
				break;
			}
		};
		if(e.target.error){
			switch(e.target.error.code) {
				case e.target.error.NOT_FOUND_ERR:
					Cupomida.mensajeError(
						"Imagen no encontrada",
						"No se pudo encontrar la imagen seleccionada."
					);
				break;
				case e.target.error.NOT_READABLE_ERR:
					Cupomida.mensajeError(
						"Imagen no accesible",
						"No se pudo leer la imagen seleccionada."
					);
				break;
				default:
					Cupomida.mensajeError(
						"Error desconocido",
						"No se pudo leer la imagen seleccionada."
					);
			}
		}
		if(typeof(e.target.files[0])=="object"){
			try{
				reader.readAsDataURL(e.target.files[0]);
			}catch(err){
				Cupomida.mensajeError("Error",err);
			}
		}
	},
	resizeBase64Image:function(base64,size){
		var canvas = document.createElement('canvas'),
			ctx = canvas.getContext('2d'),
			img = new Image();
		img.src = base64;
		var width = size.width,
			height = (size.width/(img.width/img.height)),
			radius = size.radius;
		canvas.width = width;
		canvas.height = height;
		if(radius>0){
			ctx.beginPath();
			ctx.moveTo(radius, 0);
			ctx.lineTo(width - radius, 0);
			ctx.quadraticCurveTo(width, 0, width, radius);
			ctx.lineTo(width, height - radius);
			ctx.quadraticCurveTo(width, height, width - radius, height);
			ctx.lineTo(radius, height);
			ctx.quadraticCurveTo(0, height, 0, height - radius);
			ctx.lineTo(0, radius);
			ctx.quadraticCurveTo(0, 0, radius, 0);
			ctx.closePath();
			ctx.clip();
		}
		ctx.fillStyle = "rgba(255,255,255,0)";
		ctx.drawImage(img, 0, 0, width, height);
		return canvas.toDataURL(Cupomida.imageFormat.type,Cupomida.imageFormat.compression);
	},
	calcularDias:function(Fecha){
		Fecha=Cupomida.formatoDias(Fecha);
		var Hoy=Cupomida.time.year+"/"+Cupomida.time.month+"/"+Cupomida.time.day+" ";
		Hoy+=Cupomida.time.hour+":"+Cupomida.time.minute;
		return Math.round(Math.abs((
			new Date(Fecha).getTime() - new Date(Hoy).getTime()
		)/(24*60*60*1000)));
	},
	formatoDias:function(date){
		var timestamp,
			struct;
		if((struct=/(\d{4})-(\d{2}|\d{1})-(\d{2}|\d{1})\s(\d{2}|\d{1}):(\d{2}|\d{1})/.exec(date))) {
			timestamp = new Date(struct[1],struct[2]-1,struct[3],struct[4],struct[5]);
		}else {
			timestamp = Date.parse ? Date.parse(date) : NaN;
		}
		return timestamp;
	},
	formatoNumero:function(num){
		num=num.toString().split("").reverse().join("").replace(/(?=\d*\.?)(\d{3})/g,"$1.");
		return num.split("").reverse().join("").replace(/^[\.]/,"");
	},
	cargarNuevoExtensiones:function(callback){
		if(!$("link[href='css/main-new.css']").length){
			$("head").append('<link rel="stylesheet" type="text/css" href="css/main-new.css">');
		}
		if($.ui){
			if(typeof(callback)=="function") callback();
		}else{
			$.ajaxSetup({cache:true});
			$("head").append('<link rel="stylesheet" type="text/css" href="//ajax.googleapis.com/ajax/libs/jqueryui/1.11.1/themes/smoothness/jquery-ui.css">');
			$.getScript('//ajax.googleapis.com/ajax/libs/jqueryui/1.11.1/jquery-ui.min.js',function(){
				if($.datetimepicker){
					if(typeof(callback)=="function") callback();
				}else{
					$("head").append('<link rel="stylesheet" type="text/css" href="css/jquery.datetimepicker.css">');
					$.getScript('js/jquery.datetimepicker.js',function(){
						if(typeof(callback)=="function") callback();
					}).fail(function(jqXHR, textStatus){
						Cupomida.serverResponse(jqXHR, textStatus);
					});
				}
			}).fail(function(jqXHR, textStatus){
				Cupomida.serverResponse(jqXHR, textStatus);
			});
		}
	},
	showMap:function(mapa,callback){
		var map=new google.maps.Map(
			document.getElementById(mapa.divId),
			mapa.mapProp
		);
		if(typeof(callback)=="function") callback(map);
	},
	obtenerNombrePosicion:function(lat,lon,callback){
		$.getJSON("http://maps.google.com/maps/api/geocode/json",{
			latlng:lat+","+lon,
			sensor:false
		},function(data){
			Cupomida.reset.address();
			if(data.status=="OK"){
				for(var r in data.results){
					Cupomida.addresses.push(
						data.results[r].formatted_address
					);
				}
				if(typeof(callback)=="function") callback();
			}
		}).fail(function(jqXHR, textStatus){
			Cupomida.serverResponse(jqXHR, textStatus);
		});
	},
	actualizarPosicion:function(){
		Cupomida.obtenerPosicion(function(position){
			if(position.vieja){
				if((
					position.nueva.coords.latitude.toFixed(5) == parseFloat(position.vieja.latitud)
				) && (
					position.nueva.coords.longitude.toFixed(5) == parseFloat(position.vieja.longitud)
				)){
					Cupomida.user.latitude=position.vieja.latitud;
					Cupomida.user.longitude=position.vieja.longitud;
					Cupomida.mensajeInfo(
						"Ubicación actualizada",
						"Su ubicación se encuentra actualizada."
					);
				}else{
					Cupomida.modal.show({
						title:"Actualizar ubicación.",
						body:'<p class="lead">Se encuentra en una nueva ubicación, '+
							'¿desea guardarla?</p>'+
							'<div id="ubicacionMap"></div>',
						buttons:[
							{
								id:"savePos",
								text:"Aceptar",
								class:"btn-blue"
							},{
								id:"cancelPos",
								text:"Cancelar",
								class:"btn-red"
							}
						]
					},function(id){
						switch(id){
							case "ready":
								var center=new google.maps.LatLng(
									position.nueva.coords.latitude,
									position.nueva.coords.longitude
								);
								Cupomida.showMap({
									divId:"ubicacionMap",
									Titulo:Cupomida.user.first_name,
									mapProp:{
										center:center,
										zoom:15,
										mapTypeId:google.maps.MapTypeId.ROADMAP
									}
								},function(map){
								});
							break;
							case "savePos":
								Cupomida.user.latitude=position.nueva.coords.latitude.toFixed(6);
								Cupomida.user.longitude=position.nueva.coords.longitude.toFixed(6);
								Cupomida.obtenerNombrePosicion(
									position.nueva.coords.latitude,
									position.nueva.coords.longitude,
								function(){
									Cupomida.nuevaPosicion({
										direccion:Cupomida.addresses[0],
										latitud:position.nueva.coords.latitude,
										longitud:position.nueva.coords.longitude
									},function(){
										Cupomida.mensajeInfo(
											"Ubicación guardada",
											"Su nueva ubicación ha sido guardada."
										);
									});
								});
							default:
								Cupomida.user.latitude=position.vieja.latitud;
								Cupomida.user.longitude=position.vieja.longitud;
								Cupomida.modal.hide();
							break;
						}
					});
				}
			}else{				
				Cupomida.obtenerNombrePosicion(
					position.nueva.coords.latitude,
					position.nueva.coords.longitude,
				function(){
					Cupomida.nuevaPosicion({
						direccion:Cupomida.addresses[0],
						latitud:position.nueva.coords.latitude,
						longitud:position.nueva.coords.longitude
					},function(){
						Cupomida.mensajeInfo(
							"Ubicación guardada",
							"Su nueva ubicación ha sido guardada."
						);
					});
				});
			}
		});
	},
	obtenerPosicion:function(callback){
		if(Cupomida.user){
			$.post(Cupomida.JSONroot,{
				Accion:"Ubicacion por ID",
				ID:Cupomida.user.id_ubicacion
			},function(data){
				if(data.Success){
					navigator.geolocation.getCurrentPosition(function(position){
						if(typeof(callback)=="function"){
							callback({
								nueva:position,
								vieja:data.Result
							});
						}
					},function(error){
						switch(error.code){
							case error.PERMISSION_DENIED:
								Cupomida.mensajeError(
									"Error",
									"Debe aceptar los permisos de la aplicación "+
									"para obtener su ubicación y encontrar los "+
									"cupones más cercanos."
								);
							break;
							case error.POSITION_UNAVAILABLE:
							case error.TIMEOUT:
							default:
								Cupomida.mensajeError(
									"Error",
									"No se pudo obtener su ubicación."
								);
							break;
						}
					});
				}else{
					Cupomida.errorCode(data.errorCode,function(){
						Cupomida.mensajeError(
							"Error",
							"No se pudo obtener su ubicación."
						);
					});
				}
			},"json").fail(function(jqXHR, textStatus){
				Cupomida.serverResponse(jqXHR, textStatus);
			});
		}else{
			Cupomida.mensajeError(
				"Error",
				"Debes iniciar sesión antes de guardar tu ubicación."
			);
		}
	},
	obtenerResolucion:function(){
		return {
			width:$(document).width(),
			heigth:$(document).height()
		};
	},
	obtenerCategorias:function(callback){
		if(Cupomida.categories){
			if(typeof(callback)=="function") callback();
		}else{
			$.post(Cupomida.JSONroot,{Accion:"Categorias"},function(data){
				if(data.Success){
					Cupomida.categories=data.Result;
					if(typeof(callback)=="function") callback();
				}else Cupomida.mensajeError("Error","No se pudo cargar la lista de categorias.");
			},"json").fail(function(jqXHR, textStatus){
				Cupomida.serverResponse(jqXHR, textStatus);
			});
		}
	},
	obtenerCuponesHora:function(hora,callback){
		var postData=Cupomida.poblarEstadisticas.Limites();
		postData.Accion="Cupones Visitados";
		postData.Hora=hora;
		postData.Limite=1000;
		$.post(Cupomida.JSONroot,postData,function(data){
			if(data.Success){
				var body='<div class="auto-scroll">';
				if(data.Result.length > 0){
					for(var d in data.Result){
						body+="<p>"+
							data.Result[d].titulo+
							" <button "+
								'type="button" '+
								'class="btn btn-blue btn-sm ver-cupon pull-right" '+
								'data-id="'+data.Result[d].id_cupon+'">'+
								"Ver"+
							"</button>"+
						"</p>";
					}
				}else{
					body+="<p>No existen registros de visitas a cupones en este horario.</p>";
				}
				body+="</div>";
				Cupomida.modal.show({
					title:"Cupones visitados a las "+hora+" horas:",
					body:body,
					buttons:[
						{
							id:"cuponHoraCerrar",
							text:"Cerrar",
							class:"btn-red"
						}
					]
				},function(id){
					switch(id){
						case "ready":
							Cupomida.setClicks.CuponLink.ver();
						break;
						case "cuponHoraCerrar":
						default:
							Cupomida.modal.hide();
						break;
					}
				});
			}
		});
	},
	obtenerCuponesDia:function(dia,callback){
		var postData=Cupomida.poblarEstadisticas.Limites();
		postData.Accion="Cupones Visitados";
		postData.Dia=dia;
		postData.Limite=1000;
		$.post(Cupomida.JSONroot,postData,function(data){
			if(data.Success){
				var body='<div class="auto-scroll">';
				if(data.Result.length > 0){
					for(var d in data.Result){
						body+="<p>"+
							data.Result[d].titulo+
							" <button "+
								'type="button" '+
								'class="btn btn-blue btn-sm ver-cupon pull-right" '+
								'data-id="'+data.Result[d].id_cupon+'">'+
								"Ver"+
							"</button>"+
						"</p>";
					}
				}else{
					body+="<p>No existen registros de visitas a cupones "+
						"en este día de la semana.</p>";
				}
				body+="</div>";
				Cupomida.modal.show({
					title:"Cupones visitados el día "+Cupomida.numeroDia(dia),
					body:body,
					buttons:[
						{
							id:"cuponDiaCerrar",
							text:"Cerrar",
							class:"btn-red"
						}
					]
				},function(id){
					switch(id){
						case "ready":
							Cupomida.setClicks.CuponLink.ver();
						break;
						case "cuponDiaCerrar":
						default:
							Cupomida.modal.hide();
						break;
					}
				});
			}
		});
	},
	obtenerCuponesCategoria:function(categoria,callback){
		var postData=Cupomida.poblarEstadisticas.Limites();
		postData.Accion="Cupones Visitados";
		postData.Categoria=categoria;
		postData.Limite=1000;
		$.post(Cupomida.JSONroot,postData,function(data){
			if(data.Success){
				var body='<div class="auto-scroll">';
				if(data.Result.length > 0){
					for(var d in data.Result){
						body+="<p>"+
							data.Result[d].titulo+
							" <button "+
								'type="button" '+
								'class="btn btn-blue btn-sm ver-cupon pull-right" '+
								'data-id="'+data.Result[d].id_cupon+'">'+
								"Ver"+
							"</button>"+
						"</p>";
					}
				}else{
					body+="<p>No existen registros de visitas a cupones en esta categoría.</p>";
				}
				body+="</div>";
				Cupomida.modal.show({
					title:"Cupones visitados en la categoría "+categoria+":",
					body:body,
					buttons:[
						{
							id:"cuponHoraCerrar",
							text:"Cerrar",
							class:"btn-red"
						}
					]
				},function(id){
					switch(id){
						case "ready":
							Cupomida.setClicks.CuponLink.ver();
						break;
						case "cuponHoraCerrar":
						default:
							Cupomida.modal.hide();
						break;
					}
				});
			}
		});
	},
	numeroDia:function(dia){		
		switch(parseInt(dia,10)){
			case 0:return "Lunes";
			case 1:return "Martes";
			case 2:return "Miércoles";
			case 3:return "Jueves";
			case 4:return "Viernes";
			case 5:return "Sábado";
			case 6:return "Domingo";
			default: return "Inválido";
		}
	},
	nuevaPosicion:function(postData,callback){
		postData.Accion="Nueva Posicion";
		$.post(Cupomida.JSONroot,postData,
		function(data){
			if(data.Success){
				if(typeof(callback)=="function") callback();
			}else{
				Cupomida.errorCode(data.errorCode,function(){
					Cupomida.mensajeError("Error","Ocurrió un error al guardar su ubicación.");
				});
			}
		},"json").fail(function(jqXHR, textStatus){
			Cupomida.serverResponse(jqXHR, textStatus);
		});
	},
	calcularDistancia:function(lat,lon,lat2,lon2){
		var pi=3.1415926535;
		return 111.045*((
			Math.acos(
				Math.cos(lat2*(pi/180))*
				Math.cos(lat*(pi/180))*
				Math.cos((lon2 - lon)*(pi/180))+
				Math.sin(lat2*(pi/180))*
				Math.sin(lat*(pi/180))
			)
		)*(180/pi));
	},
	initGrafico:function(callback){
		google.load('visualization','1',{
			'callback':function(){
				if(typeof(callback)=="function") callback();
			},
			'packages' : ['corechart']
		});
	},
	generarGrafico:function(Data,callback){
		Cupomida.initGrafico(function(){
			var chart=false,
				data=new google.visualization.DataTable(Data.Puntos),
				options={
					"title":Data.Titulo,
					"legend":"bottom",
					"isStacked":true,
					"showTip":true,
					"curveType":"function",
					"pointSize":3,
					"lineWidth":2,
					"vAxis":{
						"title":Data.TituloVertical
					},
					"hAxis":{
						"title":Data.TituloHorizontal,
						"showTextEvery":1,
						textStyle:{
							fontSize:12
						}
					},
					"colors":[
						"#e74c3c"
					],
					"is3D":false,
					"backgroundColor":"#f0f0f0",
					"width":"100%",
					"height":350,
					"chartArea":{
						"left":30,
						"top":30,
						"width":"85%",
						"height":250,
						"backgroundColor":"#f0f0f0"
					}
				};
			switch(Data.Type){
				case "Line":
					chart=new google.visualization.LineChart(document.getElementById(Data.Div));
				break;
				case "Bars":
					chart=new google.visualization.BarChart(document.getElementById(Data.Div));
				break;
				case "Column":
					chart=new google.visualization.ColumnChart(document.getElementById(Data.Div));
				break;
				default:
				break;
			}
			if(chart){
				chart.draw(data,options);
				if(typeof(callback)=="function") callback(data,chart);
			}
		});
	},
	history:{
		push:function(title,url){
			document.title=title;
			if(Cupomida.NoHistory){
				Cupomida.NoHistory=false;
			}else{
				history.pushState({
					number:history.length,
					hash:url
				},title,url);
			}
		}
	},
	reset:{
		cupon:function(){
			Cupomida.cupon=false;
			Cupomida.newCupon={};
		},
		priceWas:function(){
			$(".price-was").css({"text-decoration":"none"});
		},
		markers:function(){
			if(typeof(Cupomida.markers.center)!="undefined"){
				Cupomida.markers.center.setMap(null);
			}
			Cupomida.markers=false;
		},
		address:function(){
			Cupomida.addresses=[];
		},
		session:function(callback){
			$(".page-nuevo").parent().remove();
			$(".page-estadisticas").parent().remove();
			$(".page-panel").parent().remove();
			$.post(Cupomida.JSONroot,{Accion:"Cerrar"},function(){
				Cupomida.user=false;
				if(typeof(callback)=="function") callback();
			},"json").fail(function(jqXHR, textStatus){
				Cupomida.serverResponse(jqXHR, textStatus);
			});
		},
		header:function(){
			$(".section-breadcrumbs").html("").addClass("hidden");
		},
		postsImages:function(){
			for(var p in Cupomida.postImage) Cupomida.postImage[p].abort();
			Cupomida.postImage=[];
		},
		pagination:function(){
			$("#paginacion .btn-group").html("");
			$("#paginacion").addClass("hidden");
		},
		todo:function(callback){
			Cupomida.reset.cupon();
			Cupomida.reset.header();
			Cupomida.reset.address();
			Cupomida.reset.postsImages();
			Cupomida.reset.pagination();
			Cupomida.reset.session(function(){
				if(typeof(callback)=="function") callback();
			});
		},
		changePage:function(){
			$(document).scrollTop(0);
			Cupomida.pageEspera();
			Cupomida.reset.cupon();
			Cupomida.reset.header();
			Cupomida.reset.postsImages();
			Cupomida.reset.pagination();
		},
		stars:function(){
			$(".rating-star-cupon").each(function(i,e){
				if(Cupomida.cupon.valoracion_cupon===null){
					$(e).removeClass("glyphicon-star");
					$(e).addClass("glyphicon-star-empty");
				}else{
					if(Cupomida.cupon.valoracion_cupon>=i){
						$(e).addClass("glyphicon-star");
						$(e).removeClass("glyphicon-star-empty");
					}else{
						$(e).removeClass("glyphicon-star");
						$(e).addClass("glyphicon-star-empty");
					}
				}
			});
			$(".rating-star-cliente").each(function(i,e){
				if(Cupomida.cupon.valoracion_cliente===null){
					$(e).removeClass("glyphicon-star");
					$(e).addClass("glyphicon-star-empty");
				}else{
					if(Cupomida.cupon.valoracion_cliente>=i){
						$(e).addClass("glyphicon-star");
						$(e).removeClass("glyphicon-star-empty");
					}else{
						$(e).removeClass("glyphicon-star");
						$(e).addClass("glyphicon-star-empty");
					}
				}
			});
		}
	},
	serverResponse:function(jqXHR, textStatus){
		switch(textStatus){
			case "success":
			case "abort":
			break;
			default:
				Cupomida.AnalyticsSend("/#404","No encontrado.");
				console.log(jqXHR);
				console.log(textStatus);
				Cupomida.mensajeError(
					"Error de servidor",
					"Lo sentimos, estamos experimentando problemas "+
					"con nuestro servidor."
				);
			break;
		}
	},
	errorCode:function(Code,callback){
		switch(Code){
			case 1: //Sin sesión.
				Cupomida.preguntarLogin();
			break;
			case 2: //Error en la base de datos.
				if(typeof(callback)=="function") callback();
			break;
			case 3: //Faltan datos.
				if(typeof(callback)=="function") callback();
			break;
			case 4: //No es administrador
				if(Cupomida.user){
					Cupomida.mensajeError(
						"No autorizado",
						"Su usuario no se encuentra autorizado para realizar esta acción.",
						10000
					);
				}else Cupomida.preguntarLogin();
			break;
		}
	},
	preguntarLogin:function(callback){
		Cupomida.modal.show({
			title:"Debe iniciar una sesión",
			body:'<p class="lead">Para realizar esta acción debe primero iniciar una sesión</p>',
			buttons:[
				{
					id:"loadLogin",
					text:"Login",
					class:"btn-blue"
				},{
					id:"loadRegister",
					text:"Registro",
					class:"btn-blue"
				},{
					id:"cancelLogin",
					text:"Cancelar",
					class:"btn-red"
				}
			]
		},function(id){
			switch(id){
				case "ready":break;
				case "loadLogin":
					Cupomida.modal.hide();
					Cupomida.after=window.location.hash;
					if(typeof(callback)=="function"){
						callback(function(){
							Cupomida.pageLogin(function(){
								Cupomida.setClicks.Registro();
							});
						});
					}else{
						Cupomida.pageLogin(function(){
							Cupomida.setClicks.Registro();
						});
					}
				break;
				case "loadRegister":
					Cupomida.modal.hide();
					Cupomida.after=window.location.hash;
					if(typeof(callback)=="function"){
						callback(function(){
							Cupomida.pageRegistro(function(){
								Cupomida.setClicks.Registro();
							});
						});
					}else{
						Cupomida.pageRegistro(function(){
							Cupomida.setClicks.Registro();
						});
					}
				break;
				default:
					Cupomida.modal.hide();
				break;
			}
		});
	},
	emptyResult:function(){
		$(Cupomida.select.content).html(Cupomida.tpl.vacio);
		Cupomida.mensajeError(
			"No encontrado",
			"El cupón que buscas no existe o venció hace poco tiempo.",
			15000
		);
	},
	whirlpool:function(callback){
		if(typeof(Whirlpool)!='undefined'){
			if(typeof(callback)=="function") callback();
		}else{
			$.getScript("js/whirlpool.js",function(){
				if(typeof(callback)=="function") callback();
			});
		}
	},
	wordWrap:function(string,limit){
		var words=string.split(" ");
		if(words.length<=limit) return string;
		else{
			var crop="",w=0;
			while(w<=limit){
				crop+=" "+words[w];
				w++;
			}
			return crop+"...";
		}
	},
	unserialize:function unserialize(data) {
		//  discuss at: http://phpjs.org/functions/unserialize/
		// original by: Arpad Ray (mailto:arpad@php.net)
		// improved by: Pedro Tainha (http://www.pedrotainha.com)
		// improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
		// improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
		// improved by: Chris
		// improved by: James
		// improved by: Le Torbi
		// improved by: Eli Skeggs
		// bugfixed by: dptr1988
		// bugfixed by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
		// bugfixed by: Brett Zamir (http://brett-zamir.me)
		//  revised by: d3x
		//    input by: Brett Zamir (http://brett-zamir.me)
		//    input by: Martin (http://www.erlenwiese.de/)
		//    input by: kilops
		//    input by: Jaroslaw Czarniak
		//        note: We feel the main purpose of this function should be to ease the transport of data between php & js
		//        note: Aiming for PHP-compatibility, we have to translate objects to arrays
		//   example 1: unserialize('a:3:{i:0;s:5:"Kevin";i:1;s:3:"van";i:2;s:9:"Zonneveld";}');
		//   returns 1: ['Kevin', 'van', 'Zonneveld']
		//   example 2: unserialize('a:3:{s:9:"firstName";s:5:"Kevin";s:7:"midName";s:3:"van";s:7:"surName";s:9:"Zonneveld";}');
		//   returns 2: {firstName: 'Kevin', midName: 'van', surName: 'Zonneveld'}

		var that=this,
			utf8Overhead=function(chr){
				// http://phpjs.org/functions/unserialize:571#comment_95906
				var code = chr.charCodeAt(0);
				if (code < 0x0080) return 0;
				if (code < 0x0800) return 1;
				return 2;
			},
			error=function(type,msg,filename,line) {
				throw new that.window[type](msg,filename,line);
			},
			read_until=function(data,offset,stopchr){
				var i=2,
					buf=[],
					chr=data.slice(offset,offset+1);
				while(chr!=stopchr){
					if((i+offset)>data.length) error("Error","Invalid");
					buf.push(chr);
					chr=data.slice(offset+(i-1),offset+i);
					i+=1;
				}
				return [buf.length, buf.join("")];
			},
			read_chrs=function(data,offset,length){
				var i,chr,buf;
				buf=[];
				for(i=0;i<length;i++){
					chr=data.slice(offset+(i-1),offset+i);
					buf.push(chr);
					length-=utf8Overhead(chr);
				}
				return [buf.length,buf.join("")];
			},
			_unserialize=function(data,offset){
				var dtype, dataoffset, keyandchrs, keys, contig,
				length, array, readdata, readData, ccount,
				stringlength, i, key, kprops, kchrs, vprops,
				vchrs, value, chrs=0,
				typeconvert = function(x) { return x; };
				if (!offset) offset=0;
				dtype=(data.slice(offset,offset+1)).toLowerCase();
				dataoffset=offset+2;
				switch(dtype){
					case 'i':
						typeconvert=function(x){
							return parseInt(x,10);
						};
						readData=read_until(data,dataoffset,';');
						chrs=readData[0];
						readdata=readData[1];
						dataoffset+=chrs+1;
					break;
					case 'b':
						typeconvert = function(x) {
							return parseInt(x,10)!==0;
						};
						readData = read_until(data, dataoffset, ';');
						chrs = readData[0];
						readdata = readData[1];
						dataoffset += chrs + 1;
					break;
					case 'd':
						typeconvert = function(x) {
							return parseFloat(x);
						};
						readData = read_until(data, dataoffset, ';');
						chrs = readData[0];
						readdata = readData[1];
						dataoffset += chrs + 1;
					break;
					case 'n':
					readdata = null;
					break;
					case 's':
						ccount = read_until(data, dataoffset, ':');
						chrs = ccount[0];
						stringlength = ccount[1];
						dataoffset += chrs + 2;
						readData = read_chrs(data,dataoffset + 1,parseInt(stringlength,10));
						chrs = readData[0];
						readdata = readData[1];
						dataoffset += chrs + 2;
						if (chrs != parseInt(stringlength, 10) && chrs != readdata.length) {
							error('SyntaxError', 'String length mismatch');
						}
					break;
					case 'a':
						readdata = {};
						keyandchrs = read_until(data, dataoffset, ':');
						chrs = keyandchrs[0];
						keys = keyandchrs[1];
						dataoffset += chrs + 2;
						length = parseInt(keys, 10);
						contig = true;
						for(i = 0; i < length; i++){
							kprops = _unserialize(data,dataoffset);
							kchrs = kprops[1];
							key = kprops[2];
							dataoffset += kchrs;
							vprops = _unserialize(data, dataoffset);
							vchrs = vprops[1];
							value = vprops[2];
							dataoffset += vchrs;
							if (key !== i) contig=false;
							readdata[key]=value;
						}
						if(contig){
							array = new Array(length);
							for (i = 0; i < length; i++) array[i] = readdata[i];
							readdata = array;
						}
						dataoffset += 1;
					break;
					default:
						error("SyntaxError","Unknown / Unhandled data type(s): " + dtype);
					break;
				}
				return [dtype, dataoffset - offset, typeconvert(readdata)];
			};
			return _unserialize((data + ""), 0)[2];
	},
	exportToCSV:function($table){
		var $rows = $table.find("tr:has(td)"),
		tmpColDelim = String.fromCharCode(11),
		tmpRowDelim = String.fromCharCode(0),
		colDelim = '","',
		rowDelim = '"\r\n"',
		csv = '"' + $rows.map(function (i,row){
			return $(row).find("td").map(function (j,col){
				return $(col).text().replace('"','""');
			}).get().join(tmpColDelim);
		}).get().join(tmpRowDelim)
			.split(tmpRowDelim).join(rowDelim)
			.split(tmpColDelim).join(colDelim) + '"',
		csvData = "data:application/csv;charset=utf-8," + encodeURIComponent(csv);
		$(this).attr({
			"download": "Códigos Cupomida",
			"href": csvData,
			"target": "_blank"
		});
	}
};
$(document).ready(function(){
	Cupomida.init();
});
