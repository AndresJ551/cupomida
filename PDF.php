<?php
require('cupomida.php');
require('libs/fpdf.php');
require('InstaPush.php');

class PDF extends FPDF {
	public $Info;
	function Header(){
		$this->Image('/home/ubuntu/Cupomida/img/logos/cupomidalogo.png',8,8,null,null,'PNG',"http://cupomida.cl/");
		$this->Cell(55,0,'');
		$this->SetFont('Arial','B',10);
		$this->Cell(20,5,'Nombre: ');
		$this->SetFont('Arial','I',10);
		$this->Cell(0,5,
			utf8_decode($this->Info["Usuario"]["first_name"]).
			" ".
			utf8_decode($this->Info["Usuario"]["last_name"])
		);
		$this->Ln(5);
		$this->Cell(55,0,'');
		$this->SetFont('Arial','B',10);
		$this->Cell(20,5,'Cupon: ');
		$this->SetFont('Arial','I',10);
		$this->Cell(0,5,
			utf8_decode($this->Info["Cupon"]["titulo"])
		);
		$this->Ln(5);
		$this->Cell(55,0,'');
		$this->SetFont('Arial','B',10);
		$this->Cell(20,5,'Vence el: ');
		$this->SetFont('Arial','I',10);
		$this->Cell(0,5,$this->Info["Cupon"]["valido_hasta"]);
		$this->Ln(10);
	}
	function Footer(){
		$this->SetY(-15);
		$this->SetFont('Arial','I',8);
		$this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'C');
	}
}

class Cupon extends Cupomida {
	public function JSON() {
		parent::Cupomida();
	}
	public function Buscar(){
		if(isset($_REQUEST["ID"])){
			$C=$_REQUEST["ID"];
			$query="SELECT ".
				"`Cupones`.`id_cupon`,".
				"`Cupones`.`titulo`,".
				"`Cupones`.`descripcion`,".
				"`Cupones`.`detalle`,".
				"`Cupones`.`id_cliente`,".
				"`Cupones`.`valido_desde`,".
				"`Cupones`.`valido_hasta`,".
				"`Cupones`.`cantidad`,".
				"`Cupones`.`restantes`,".
				"`Cupones`.`dias_habiles`,".
				"`Cupones`.`condiciones`,".
				"`Cupones`.`precio`,".
				"`Cupones`.`descuento`,".
				"`Cupones`.`estado`,".
				"`Clientes`.`nombre`,".
				"`Clientes`.`imagen`,".
				"`Clientes`.`email`,".
				"`Clientes`.`telefono`,".
				"`Clientes`.`horario`,".
				"`Clientes`.`informacion`,".
				"`Ubicaciones`.`direccion` ".
			"FROM `Cupomida`.`Cupones` ".
			"LEFT JOIN `Cupomida`.`Clientes` ".
				"ON `Cupones`.`id_cliente`=`Clientes`.`id_cliente` ".
			"LEFT JOIN `Cupomida`.`Ubicacion_Cupon` ".
				"ON `Ubicacion_Cupon`.`id_cupon`=`Cupones`.`id_cupon` ".
			"LEFT JOIN `Cupomida`.`Ubicaciones` ".
				"ON `Ubicaciones`.`id_ubicacion`=`Ubicacion_Cupon`.`id_ubicacion` ".
			"WHERE `Cupones`.`id_cupon`=$C ".
			" LIMIT 1;";
			if($Cupones=$this->DB->query($query)){
				$Cupon=mysqli_fetch_assoc($Cupones);
				$Cupones->free();
				if($Cupon){
					if($Cupon["estado"]==1){
						if($Cupon["restantes"]>0){
							$query2="SELECT ".
								"`Usuarios`.`first_name`,".
								"`Usuarios`.`last_name`,".
								"`Usuarios`.`gender`,".
								"`Usuarios`.`email`,".
								"`Usuarios`.`administrador` ".
							"FROM `Cupomida`.`Usuarios` ".
							"WHERE `id_usuario`=".$_SESSION["Usuario"].
							" LIMIT 1;";
							if($Usuarios=$this->DB->query($query2)){
								$Usuario=mysqli_fetch_assoc($Usuarios);
								$Usuarios->free();
								if($Usuario){
									if($Canjeados=$this->DB->query("SELECT ".
										"`id_cupon`,`id_usuario`,`serie` ".
									"FROM `Cupomida`.`Codigos` ".
									"WHERE `id_usuario`=".$_SESSION["Usuario"].
										" AND `id_cupon`=".$Cupon["id_cupon"].
									" LIMIT 1;")){
										$Canjeado=mysqli_fetch_assoc($Canjeados);
										if($Canjeado){
											return array(
												"Cupon"=>$Cupon,
												"Usuario"=>$Usuario,
												"Codigo"=>$Canjeado["serie"]
											);
										}else{
											if($this->DB->query("UPDATE `Cupomida`.`Codigos` ".
												"SET `id_usuario`=".$_SESSION["Usuario"].",".
												"`tiempo`=CONVERT_TZ(NOW(),'UTC','America/Santiago') ".
												"WHERE `id_usuario`=0 ".
													"AND `id_cupon`=".$Cupon["id_cupon"].
												" LIMIT 1;"
											)){
												if($Canjeado=$this->DB->query("SELECT `serie` ".
													"FROM `Cupomida`.`Codigos` ".
													"WHERE `id_usuario`=".$_SESSION["Usuario"].
														" AND `id_cupon`=".$Cupon["id_cupon"].
													" LIMIT 1;")){
													$Serie=mysqli_fetch_assoc($Canjeado);
													if($this->DB->query("UPDATE `Cupomida`.`Cupones` ".
														"SET `restantes`=`restantes`-1 ".
														"WHERE `id_cupon`=".$Cupon["id_cupon"].";")){
														return array(
															"Cupon"=>$Cupon,
															"Usuario"=>$Usuario,
															"Codigo"=>$Serie["serie"]
														);
													}else{
														echo "No se pudo actualizar la cantidad de cupones restantes.";
													}
												}else{
													echo "No se pudo obtener el código del cupón.";
												}
											}else{
												echo "No se pudo generar el código del cupón.";
											}
										}
									}else{
										echo "No se pudo validar la duplicidad.";
									}
								}else{
									echo "No se encuentra el usuario.";
								}
							}else{
								echo "Ocurrió un error al seleccionar el usuario.";
							}
						}else{
							echo "No quedan cupones.";
						}
					}else{
						echo "El cupón fue eliminado.";
					}
				}else{
					echo "No se encuentra el cupón.";
				}
			}else{
				echo "Ocurrió un error al seleccionar el cupón.";
			}
		}else{
			echo "No se especificó el cupón.";
		}
	}
	public function SendMail($Info){
		$query="SELECT ".
			"`Cola_Correos`.`id_correo`".
		"FROM `Cupomida`.`Cola_Correos` ".
		"WHERE `id_usuario`=".$_SESSION["Usuario"].
		" AND `id_cupon`=".$Info["Cupon"]["id_cupon"].
		" LIMIT 1;";
		if($Cupones=$this->DB->query($query)){
			$Cupon=mysqli_fetch_assoc($Cupones);
			$Cupones->free();
			if(!$Cupon){
				$query="CALL `Cupomida`.`nuevo_correo`(".
					$_SESSION["Usuario"].",".
					$Info["Cupon"]["id_cupon"].",".
					"'Cupón desde Cupomida.',".
					"'¡Gracias por ser parte de Cupomida!<br/> ".
					"Le adjuntamos el cupón imprimible en formato PDF. ".
					"Puede mostrarlo tal cual en el local en la pantalla de su celular.',".
					"'".$Info["Cupon"]["PDF"]."');";
				if(!$this->DB->query($query)) error_log($this->DB->error,0);
			}
		}else error_log($this->DB->error,0);
	}
}

$Cupon=new Cupon();
if(isset($_SESSION["Usuario"])){
	$Info=$Cupon->Buscar();
	if($Info){
		$FullWidth=0;
		$HalfWidth=95;
		$LineHeight=5;
		$DescHeight=25;
		$TextHeight=40;
		$Border=1;
		$Align="L";
		$Ln=0;
		$Fill=false;

		$Family="Times";
		$SizeBig=18;
		$SizeTitle=13;
		$SizeText=11;
		$pdf=new PDF();
		$pdf->Info=$Info;
		$pdf->AliasNbPages();
		$pdf->AddPage();
		$pdf->SetFont($Family,'',$SizeTitle);
		$pdf->MultiCell($FullWidth,$LineHeight,
			utf8_decode($Info["Cupon"]["descripcion"])."\n".
			utf8_decode($Info["Cupon"]["detalle"]),$Border);
		$pdf->SetFont($Family,'',$SizeBig);
		$pdf->Cell($FullWidth,$DescHeight,utf8_decode("Código: ").$Info["Codigo"],$Border,$Ln,$Align,$Fill);
		$url="http://cupomida.cl/%23Cupon/".$Info["Cupon"]["id_cupon"];
		$pdf->Image("http://chart.apis.google.com/chart?cht=qr&chs=200x200&chl=".$url,179,null,20,20,"PNG",$url);
		$pdf->Ln($LineHeight);
		$pdf->SetFont($Family,'',$SizeText);
		$pdf->SetTextColor(190,0,15);
		$pdf->SetFont($Family,'',$SizeTitle);
		$pdf->Cell($FullWidth,$LineHeight,utf8_decode("Dirección:"),$Border,$Ln,$Align,$Fill);
		$pdf->Ln($LineHeight);
		$pdf->SetFont($Family,'',$SizeText);
		$pdf->SetTextColor(0,0,0);
		$pdf->Cell($FullWidth,$LineHeight,utf8_decode($Info["Cupon"]["direccion"]),$Border,$Ln,$Align,$Fill);
		$pdf->Ln($LineHeight);
		$pdf->SetTextColor(190,0,15);
		$pdf->SetFont($Family,'',$SizeTitle);
		$pdf->Cell($HalfWidth,$LineHeight,"Condiciones:",$Border,$Ln,$Align,$Fill);
		$pdf->Cell($HalfWidth,$LineHeight,utf8_decode("Información de la empresa:"),$Border,$Ln,$Align,$Fill);
		$pdf->Ln($LineHeight);
		$pdf->SetTextColor(0,0,0);
		$pdf->SetFont($Family,'',$SizeText);
		$Informaciones[0]=utf8_decode("Nombre: ".$Info["Cupon"]["nombre"]);
		$Informaciones[1]=utf8_decode("Teléfono: ".$Info["Cupon"]["telefono"]);
		$Informaciones[2]=utf8_decode("Correo: ".$Info["Cupon"]["email"]);
		$Informaciones[3]=utf8_decode("Horario de atención: ".$Info["Cupon"]["horario"]);
		$Condiciones=unserialize($Info["Cupon"]["condiciones"]);
		$max=count($Condiciones);
		if($max<5) $max=5;
		for($i=0;$i<$max;$i++){
			if(isset($Condiciones[$i])) $Condicion=$Condiciones[$i];
			else $Condicion="";
			$pdf->Cell($HalfWidth,$LineHeight,$Condicion,$Border,$Ln,$Align,$Fill);
			if(isset($Informaciones[$i])) $Informacion=$Informaciones[$i];
			else $Informacion="";
			$pdf->Cell($HalfWidth,$LineHeight,$Informacion,$Border,$Ln,$Align,$Fill);
			$pdf->Ln($LineHeight);
		}
		$pdf->SetTextColor(190,0,15);
		$pdf->SetFont($Family,'',$SizeTitle);
		$pdf->Cell($FullWidth,$LineHeight,utf8_decode("Días de atención:"),$Border,$Ln,$Align,$Fill);
		$pdf->Ln($LineHeight);
		$Semana="";
		$Semanas=explode(",",$Info["Cupon"]["dias_habiles"]);
		if($Semanas[0]!="N") $Semana.="Lunes, ";
		if($Semanas[1]!="N") $Semana.="Martes, ";
		if($Semanas[2]!="N") $Semana.=utf8_decode("Miércoles, ");
		if($Semanas[3]!="N") $Semana.="Jueves, ";
		if($Semanas[4]!="N") $Semana.="Viernes, ";
		if($Semanas[5]!="N") $Semana.=utf8_decode("Sábado, ");
		if($Semanas[6]!="N") $Semana.="Domingo";
		$Semana=rtrim($Semana,", ");
		$pdf->SetTextColor(0,0,0);
		$pdf->SetFont($Family,'',$SizeText);
		$pdf->Cell($FullWidth,$LineHeight,$Semana,$Border,$Ln,$Align,$Fill);
		$pdf->Output();

		if($Info["Usuario"]["email"]!=""){
			$Info["Cupon"]["PDF"]=base64_encode($pdf->Output('', 'S'));
			$Cupon->SendMail($Info);
		}
		$InstaPush=new InstaPush();
		$InstaPush->Push("CuponImpreso",array(
			"Usuario"=>$Info["Usuario"]["first_name"]." ".$Info["Usuario"]["last_name"],
			"Local"=>$Info["Cupon"]["nombre"],
			"Cupon"=>$Info["Cupon"]["titulo"],
			"Tiempo"=>date("H:i:s d/m/Y")
		));
	}
}else{
	echo "No existe una sesión abierta";
}
