<?php
	require("cupomida.php");
	class Imagen extends Cupomida {
		public function Cupon(){
			parent::Cupomida();
		}
		public function Buscar(){
			if(isset($_REQUEST["i"])){
				$id_cupon=(double) $_REQUEST["i"];
				$Query="SELECT `imagen` FROM `Cupones` WHERE `id_cupon`=".$id_cupon;
				$Cupon=$this->DB->query($Query);
				$row=mysqli_fetch_assoc($Cupon);
				$Cupon->free();
				$formato=substr($row["imagen"],0,strpos($row["imagen"],";base64,"));
				$code_base64 = str_replace($formato.';base64,','',$row["imagen"]);
				$code_binary = base64_decode($code_base64);
				if(function_exists("imagecreatefromstring")){
					$image = imagecreatefromstring($code_binary);
					header('Content-Type: image/jpeg');
					imagejpeg($image);
					imagedestroy($image);
				}else{
					echo "Error";
				}
			}
		}
	}
	$Imagen=new Imagen();
	$Imagen->Buscar();
